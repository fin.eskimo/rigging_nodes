import traceback


class BBN_NodeException(Exception):
    def __init__(self, node, path, info):
        self.node = node
        self.info = info
        self.path = path

        self.traceback = traceback.format_exc()

    def __str__(self):
        return f'{self.node.id_data} : {self.node.name} -> {self.info}'
