import bpy
import time
from bpy.types import NodeSocket
from bpy.utils import escape_identifier
from ..runtime import cache_loop_inputs, cache_socket_conections, cache_socket_variables
from ..node_tree import BBN_tree

# never use is_linked, always check links instead,
# sometimes is_linked says your socket is not connected but links actually returns a list of links
# nevermind, accessing links is very slow for drawing, only use when it's really necessary
# use is_linked for real time drawing

draw_times = {}
get_value_times = {}


class BBN_socketmixin():
    '''Used by nodesocket and nodesocketinterface'''

    dependent_classes = []

    def init_from_socket(self, node, socket):
        self.display_shape = socket.shape

        if hasattr(self, 'default_value') and hasattr(socket, 'default_value'):
            if hasattr(self, 'set_default_value'):
                self.set_default_value(socket.default_value)
            else:
                self.default_value = socket.default_value


class BBN_socket_interface():
    '''Stores the necessary properties for a new socket to be correctly created'''

    shape = 'CIRCLE'
    dependent_classes = []

    def from_socket(self, node, socket):
        # https://docs.blender.org/api/current/bpy.types.NodeSocketInterface.html#bpy.types.NodeSocketInterface.from_socket
        self.init_from_socket(node, socket)

    def init_socket(self, node, socket, data_path):
        # https://docs.blender.org/api/current/bpy.types.NodeSocketInterface.html#bpy.types.NodeSocketInterface.init_socket
        socket.init_from_socket(node, self)

    def draw_color(self, context):
        '''Color of the socket icon'''
        return self.color

    def draw(self, context, layout):
        if hasattr(self, 'default_value'):
            layout.prop(self, 'default_value', text='Default')


class BBN_socket_array_interface(BBN_socket_interface):
    shape = 'SQUARE'
    pass


class BBN_socket():

    socket_version = 0
    current_socket_version: bpy.props.IntProperty(default=0)

    draw_time: bpy.props.FloatProperty(default=0)

    value: bpy.props.StringProperty()

    error: bpy.props.BoolProperty()
    # for initializing the default value without updating the node tree
    do_not_update: bpy.props.BoolProperty(default=False)
    shape = 'CIRCLE'

    description: bpy.props.StringProperty()
    display_name: bpy.props.StringProperty()
    editable_name: bpy.props.BoolProperty(default=False)
    is_moveable: bpy.props.BoolProperty(default=False)
    is_deletable: bpy.props.BoolProperty(default=False)

    # instead of storing the default value prop in the socket, use a specific property from the node
    default_value_property_name: bpy.props.StringProperty(default='')

    dependent_classes = []
    compatible_sockets = []

    other_socket = bpy.props.PointerProperty(type=bpy.types.NodeSocket)

    icon = 'NONE'
    show_extra_info = False

    def draw_color(self, context, node):
        '''Color of the socket icon'''
        return self.color

    def upgrade_socket(self):
        '''TODO: implement a way to upgrade sockets just like nodes'''
        pass

    @classmethod
    def register_dependants(cls):
        '''Registers the classes in dependent_classes'''
        from bpy.utils import register_class

        for cls in cls.dependent_classes:
            register_class(cls)

    @classmethod
    def unregister_dependants(cls):
        '''Unregisters the classes in dependent_classes'''
        from bpy.utils import unregister_class

        for cls in cls.dependent_classes:
            unregister_class(cls)

    @property
    def connected_socket(self):
        '''
        Returns connected socket

        It takes O(len(nodetree.links)) time to iterate thought the links to check the connected socket
        To avoid doing the look up every time, the connections are cached in a dictionary
        The dictionary is emptied whenever a socket/connection/node changes in the nodetree
        '''
        # accessing links Takes O(len(nodetree.links)) time.
        _nodetree_socket_connections = cache_socket_conections.setdefault(self.id_data, {})
        _connected_socket = _nodetree_socket_connections.get(self, None)

        if _connected_socket:
            return _connected_socket

        socket = self
        if socket.is_output:
            while socket.links and socket.links[0].to_node.bl_rna.name == 'Reroute':
                socket = socket.links[0].to_node.outputs[0]
            if socket.links:
                _connected_socket = socket.links[0].to_socket
        else:
            while socket.links and socket.links[0].from_node.bl_rna.name == 'Reroute':
                socket = socket.links[0].from_node.inputs[0]
            if socket.links:
                _connected_socket = socket.links[0].from_socket

        cache_socket_conections[self.id_data][self] = _connected_socket
        return _connected_socket

    def unlink(self):
        '''Unlinks the socket'''
        if self.links:
            self.id_data.links.remove(self.links[0])

    def _init(self, **kwargs):
        '''Initialize the socket data from the given dictionary'''
        self.display_shape = self.shape
        self.display_name = kwargs.pop('display_name', '')
        self.description = kwargs.pop('description', '')
        self.hide = not kwargs.pop('visible', True)

        def_value = kwargs.pop('default_value', None)
        if def_value:
            self.set_default_value(def_value)

        self.default_value_property_name = kwargs.pop('default_value_property_name', '')

        self.is_deletable = kwargs.pop('is_deletable', False)
        self.is_moveable = kwargs.pop('is_moveable', False)
        self.editable_name = kwargs.pop('editable_name', False)

        return self

    def set_default_value(self, value):
        '''
        Changes the default value of the socket without re-executing the tree
        '''
        self.do_not_update = True
        self.default_value = value
        self.do_not_update = False

    # TODO: instaed of removing, make them invalid, and change the get_value function to treat them as not connected but apparently it is not possible in python?
    # https://blender.stackexchange.com/questions/153489/custom-nodes-how-to-validate-a-link
    # only being used in input sockets, so only one link is necessary
    def remove_incorrect_links(self):
        '''
        Removes the invalid links from the socket when the tree in updated
        There is no visual indication for incorrect custom sockets other than removing the invalid links 
        '''
        if self.node.id_data in cache_socket_conections:
            del cache_socket_conections[self.node.id_data]
        connected_socket = self.connected_socket

        if connected_socket:
            if not self.is_socket_compatible(connected_socket):
                self.unlink()

    def is_socket_compatible(self, other_socket):
        '''
        Checks if the provided socket is compatible with this one
        '''
        # to avoid removing links before the group input socket can create a socket
        if other_socket.bl_idname == 'NodeSocketVirtual':
            return True
        return other_socket.bl_idname == self.bl_idname or other_socket.bl_idname in self.compatible_sockets

    # TODO: rework all these 6 get/set functions ??

    def convert_value(self, value):
        return value

    def set_value(self, value):
        '''Sets the value of an output socket'''
        cache_socket_variables.setdefault(self.id_data, {})[self] = self.convert_value(value)

    def get_self_value(self):
        '''returns the stored value of an output socket'''
        val = cache_socket_variables.setdefault(self.id_data, {}).get(self, None)
        return val

    def get_self_default_value(self):
        '''returns the default value of an input socket'''
        if self.default_value_property_name and hasattr(self.node, self.default_value_property_name):
            return getattr(self.node, self.default_value_property_name)
        elif 'custom_default_value' in self:
            return self['custom_default_value']
        elif hasattr(self, 'default_value'):
            return self.default_value
        return None

    def get_value(self):
        '''
        if the socket is an output it returns the stored value of that socket
        if the socket is an input:
            if it's connected, it returns the value of the connected output socket
            if it's not it returns the default value of the socket
        '''
        start_time = time.perf_counter_ns()
        ans = ''
        if not self.is_output:
            connected_socket = self.connected_socket
            if not connected_socket:
                ans = self.get_self_default_value()
            else:
                ans = connected_socket.get_self_value()
        else:
            ans = self.get_self_value()

        end_time = time.perf_counter_ns()
        get_value_times.setdefault(self.id_data, {})[self] = ((end_time - start_time) / 1000000000)

        ans = self.convert_value(ans)
        return ans

    def get_real_value(self):
        '''Like get value, but converts the stored data to a more manageable type (used in pointers for example)'''
        return self.get_value()

    def value_to_string(self, value):
        '''Converts the value to string for UI purposes'''
        return str(value)

    def value_to_string_extended(self, value):
        '''Converts the value to a larger string (useful for previewing lists, matrix etc.)'''
        return self.value_to_string(value)

    def draw_value(self, layout):
        is_selected = self.node.select

        if self.show_extra_info:
            layout = layout.row(align=True)
            layout.operator('bbn.show_shocket_value', emboss=False, text='', icon='INFO').value = self.value_to_string_extended(self.get_value())
        layout.label(icon=self.icon, text=self.value_to_string(self.get_value()))

    def draw_buttons(self, layout):
        pass

    def draw(self, context, layout, node, text):
        if context.space_data.edit_tree == self.id_data:
            start_time = time.perf_counter_ns()
            self.draw_complex(context, layout)
            end_time = time.perf_counter_ns()
            draw_times.setdefault(self.id_data, {})[self] = ((end_time - start_time) / 1000000000)

    def draw_complex(self, context, layout):
        display_name = self.display_name if self.display_name else self.name
        sockets = self.node.outputs if self.is_output else self.node.inputs

        main_row = layout.row()
        row = main_row.row()

        if self.is_output:
            main_row.alignment = 'EXPAND'

            if not self.is_linked and context.region.type == 'UI':
                icon = 'HIDE_OFF' if not self.hide else 'HIDE_ON'
                row.prop(self, "hide", icon=icon, icon_only=True, invert_checkbox=True, emboss=False)

            label_split = row.split(factor=0.3, align=True)
            label_split.alignment = 'EXPAND'
            label_row = label_split.row()
            label_row.alignment = 'RIGHT'

            if context.region.type == 'UI' and self.editable_name:
                label_row.prop(self, 'name', text='')
            else:
                label_row.label(text=f'{display_name}:')
            self.draw_value(label_split)
        else:
            if self.error:
                layout.label(icon='ERROR')
            if self.is_linked:
                label_split = main_row.split(factor=0.3, align=True)
                label_split.alignment = 'LEFT'

                label_row = label_split.row()
                label_row.alignment = 'RIGHT'

                if self.editable_name and context.region.type == 'UI':
                    label_row.prop(self, 'name', text='')
                else:
                    label_row.label(text=f'{display_name}:')
                self.draw_value(label_split)

            else:
                if context.region.type == 'UI':
                    icon = 'HIDE_OFF' if not self.hide else 'HIDE_ON'
                    row.prop(self, "hide", icon=icon, icon_only=True, invert_checkbox=True, emboss=False, text='')
                row = row.split(factor=0.3, align=True)
                label_row = row.row()
                label_row.alignment = 'RIGHT'
                if self.editable_name and context.region.type == 'UI':
                    label_row.prop(self, 'name', text='')
                else:
                    label_row.label(text=f'{display_name}:')
                row = row.row(align=True)

                if self.has_default_value:
                    self.draw_default_prop(context, row)
                else:
                    self.draw_value(row)

        if context.region.type == 'UI':
            extra_row = main_row.row()
            extra_row.alignment = 'RIGHT'

            if self.is_moveable:
                for i in range(0, len(sockets)):
                    if sockets[i] == self:
                        current_index = i

                        op = extra_row.operator('bbn.move_socket', text='', icon='TRIA_UP')
                        op.node = self.node.name
                        op.is_output = self.is_output
                        op.new_index = current_index - 1
                        op.current_index = current_index

                        op = extra_row.operator('bbn.move_socket', text='', icon='TRIA_DOWN')
                        op.node = self.node.name
                        op.is_output = self.is_output
                        op.new_index = current_index + 1
                        op.current_index = current_index

            if self.is_deletable:
                for i in range(0, len(sockets)):
                    if sockets[i] == self:
                        current_index = i

                        op = extra_row.operator('bbn.delete_socket', text='', icon='X')
                        op.node = self.node.name
                        op.is_output = self.is_output
                        op.current_index = current_index

    @property
    def has_default_value(self):
        return hasattr(self, 'default_value') or 'default_value' in self or self.default_value_property_name and hasattr(self.node, 'default_value_property_name')

    def draw_default_prop(self, context, layout):
        if self.default_value_property_name and hasattr(self.node, 'default_value_property_name'):
            layout.prop(self.node, self.default_value_property_name, text='', icon=self.icon)
        elif 'custom_default_value' in self:
            layout.prop(self, '["%s"]' % escape_identifier('custom_default_value'), text="")
        else:
            layout.prop(self, 'default_value', text='', icon=self.icon)


class BBN_array_base(BBN_socketmixin):
    required_length: bpy.props.IntProperty(default=0)

    shape = 'SQUARE'

    def init_from_socket(self, node, socket):
        super().init_from_socket(node, socket)
        self.required_length = socket.required_length


class BBN_socket_array(BBN_socket):
    socket_class = BBN_socket
    show_extra_info = True

    def get_self_default_value(self):
        '''Returns the serialized stored value'''
        if hasattr(self, 'default_value'):
            return [x.value for x in self.default_value]
        return []

    def draw_default_prop(self, context, layout):
        for x in self.default_value:
            layout.prop(x, 'value', text='')

    def is_socket_compatible(self, other_socket):
        if not (other_socket.bl_idname == self.bl_idname or other_socket.bl_idname in self.compatible_sockets):
            return False
        if self.required_length and len(other_socket.value) != self.required_length:
            return False
        return True

    @property
    def has_default_value(self):
        return self.required_length and hasattr(self, 'default_value')

    def _init(self, **kwargs):
        self.required_length = kwargs.pop('required_length', 0)
        if hasattr(self, 'default_value'):
            for i in range(0, self.required_length):
                val = self.default_value.add()
        return super()._init(**kwargs)

    def set_value(self, value):
        '''sets the value already serialized'''
        if hasattr(value, 'copy'):
            cache_socket_variables.setdefault(self.id_data, {})[self] = value.copy()
        else:
            raise ValueError(f'{self.node} {self.name} {self.identifier} trying to copy {value}, but failed')

    def set_default_value(self, value):
        for i, x in enumerate(value):
            self.default_value[i].value = x

    def value_to_string(self, value):
        if value is not None:
            return f'{self.socket_class.bl_label} Array [{len(value)}]'
        return f'{self.socket_class.bl_label} Array'

    def value_to_string_extended(self, value):
        if value:
            ans = ''
            for item in value:
                if self.socket_class.show_extra_info:
                    ans += self.socket_class.value_to_string_extended(None, item)
                else:
                    ans += self.socket_class.value_to_string(None, item)
                ans += '\n'
            return ans
        return '(EMPTY ARRAY)'
