import bpy
import os
import importlib

from .socket import BBN_socket, BBN_socket_array


tree = [x[:-3] for x in os.listdir(os.path.dirname(__file__)) if x.endswith('.py') and x != '__init__.py' and x != 'socket.py']

for i in tree:
    importlib.import_module('.' + i, package=__package__)

__globals = globals().copy()

sockets = set()

for num_id, x in enumerate([x for x in __globals if x.startswith('socket_')]):
    for y in [item for item in dir(__globals[x]) if item.startswith('BBN_socket_') and item != 'BBN_socket' and item != 'BBN_socket_interface' and item != 'BBN_socket_array' and item != 'BBN_socket_array_interface']:
        sockets.add(getattr(__globals[x], y))

sockets = list(sockets)

array_sockets = {x.bl_idname: x for x in sockets if BBN_socket_array in x.__bases__}


def register():
    from bpy.utils import register_class

    dependent_classes = []

    for cls in sockets:
        for x in cls.dependent_classes:
            if x not in dependent_classes:
                dependent_classes.append(x)

    for cls in dependent_classes:
        register_class(cls)

    for cls in sockets:
        register_class(cls)


def unregister():
    from bpy.utils import unregister_class

    dependent_classes = set()

    for cls in reversed(sockets):
        unregister_class(cls)

    for cls in sockets:
        for x in cls.dependent_classes:
            dependent_classes.add(x)

    for cls in dependent_classes:
        unregister_class(cls)
