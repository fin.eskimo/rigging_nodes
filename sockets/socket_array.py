import logging
import bpy
from bpy.types import NodeSocket, NodeSocketInterface
from ..node_tree import BBN_tree


from .socket import BBN_socket, BBN_socket_interface, BBN_socket_array_interface, BBN_socketmixin

# IMPORT SOCKETS
from .socket_string import BBN_socket_string
from .socket_bone import BBN_socket_bone
from .socket_bool import BBN_socket_bool
from .socket_collection import BBN_socket_collection
from .socket_driver import BBN_socket_driver
from .socket_drivervar import BBN_socket_drivervar
from .socket_enum import BBN_socket_enum
from .socket_float import BBN_socket_float
from .socket_integer import BBN_socket_int
from .socket_matrix import BBN_socket_matrix
from .socket_object_ref import BBN_socket_object_ref
from .socket_object import BBN_socket_object
from .socket_vector import BBN_socket_vector

sockets = {
    BBN_socket_string,
    BBN_socket_bone,
    BBN_socket_bool,
    BBN_socket_collection,
    BBN_socket_driver,
    BBN_socket_drivervar,
    BBN_socket_enum,
    BBN_socket_float,
    BBN_socket_int,
    BBN_socket_matrix,
    BBN_socket_object_ref,
    BBN_socket_object,
    BBN_socket_vector
}

socket_dict = {x.bl_idname: x for x in sockets}


log = logging.getLogger(__name__)

# TODO: remove this
# it doesn't work that way....

socket_type_2_property = {
    'BBN_bool_socket': 'boolean',
    'BBN_vector_socket': 'vector',
    'BBN_float_socket': 'float',
    'BBN_string_socket': 'string',
    'BBN_bone_socket': 'boolean',
    'BBN_socket_enum': 'string',
    'BBN_int_socket': 'integer',
}


class BBN_array_value(bpy.types.PropertyGroup):
    array_type: bpy.props.StringProperty()

    string: bpy.props.StringProperty()
    integer: bpy.props.IntProperty()
    pointer: bpy.props.PointerProperty(type=bpy.types.Object)
    vector: bpy.props.FloatVectorProperty()
    boolean: bpy.props.BoolProperty()
    float: bpy.props.FloatProperty()

    constraint_path: bpy.props.StringProperty()

    def draw_value(self, layout):
        if self.array_type in socket_type_2_property.keys():
            layout.prop(self, socket_type_2_property[self.array_type], text='')
        else:
            layout.label(text='*')

    def get_value(self):
        if self.array_type == 'BBN_bool_socket':
            return self.boolean
        elif self.array_type == 'BBN_object_socket':
            return self.pointer
        elif self.array_type == 'BBN_object_ref_socket':
            return self.pointer
        elif self.array_type == 'BBN_vector_socket':
            return self.vector
        elif self.array_type == 'BBN_float_socket':
            return self.float
        elif self.array_type == 'BBN_string_socket' or self.array_type == 'BBN_bone_socket':
            return self.string
        elif self.array_type == 'BBN_socket_enum':
            return self.string
        elif self.array_type == 'BBN_int_socket':
            return self.integer
        elif self.array_type == 'BBN_collection_socket':
            return (self.pointer, self.constraint_path)
        return None

    def set_value(self, value):

        if self.array_type == 'BBN_bool_socket':
            self.boolean = value
        elif self.array_type == 'BBN_object_socket':
            self.pointer = value
        elif self.array_type == 'BBN_object_ref_socket':
            self.pointer = value
        elif self.array_type == 'BBN_vector_socket':
            self.vector = value
        elif self.array_type == 'BBN_float_socket':
            self.float = value
        elif self.array_type == 'BBN_string_socket' or self.array_type == 'BBN_bone_socket':
            self.string = value
        elif self.array_type == 'BBN_socket_enum':
            self.string = value
        elif self.array_type == 'BBN_int_socket':
            self.integer = value
        elif self.array_type == 'BBN_collection_socket':
            self.pointer, self.constraint_path = value

    def to_format(self):
        if self.array_type == 'BBN_vector_socket':
            return [x for x in self.get_value()]
        return self.get_value()


class BBN_array_base(BBN_socketmixin):
    color = (8.0, 8.0, 8.0, 1.0)

    array_socket_type: bpy.props.StringProperty()
    required_length: bpy.props.IntProperty(default=0)

    shape = 'SQUARE'

    def init_from_socket(self, node, socket):
        super().init_from_socket(node, socket)
        self.required_length = socket.required_length
        self.array_socket_type = socket.array_socket_type


class BBN_socket_array_interface_old(BBN_array_base, BBN_socket_interface, NodeSocketInterface):
    bl_socket_idname = 'BBN_string_array_socket'

    def init_socket(self, node, socket, data_path):
        socket._init(required_length=self.required_length, array_socket_type=self.array_socket_type)

    def draw_color(self, context):
        if self.array_socket_type in socket_dict.keys():
            return socket_dict[self.array_socket_type].color
        return self.color


class BBN_socket_array_old(BBN_array_base, BBN_socket, NodeSocket):
    bl_idname = 'BBN_string_array_socket'
    bl_label = "Array"

    value: bpy.props.CollectionProperty(type=BBN_array_value)
    default_value: bpy.props.CollectionProperty(type=BBN_array_value)

    dependent_classes = [BBN_array_value]
    show_extra_info = True

    array_socket_type = bpy.props.StringProperty()
    required_length: bpy.props.IntProperty(default=0)

    @property
    def has_default_value(self):
        return self.required_length

    def draw_default_prop(self, context, layout):
        for x in self.value:
            x.draw_value(layout)

    def _init(self, **kwargs):
        self.array_socket_type = kwargs.pop('array_socket_type', '')
        self.required_length = kwargs.pop('required_length', 0)
        if self.required_length:
            self.value.clear()
            for i in range(0, self.required_length):
                val = self.value.add()
                val.array_type = self.array_socket_type
        return super()._init(**kwargs)

    def draw_color(self, context, node):
        if self.array_socket_type in socket_dict.keys():
            return socket_dict[self.array_socket_type].color
        return self.color

    def value_to_string(self, value):
        return 'UPDATE ME'

    def value_to_string_extended(self, value):
        return 'UPDATE ME'

    def get_self_value(self):
        return self.array_socket_type, [x.get_value() for x in self.value]

    def get_real_value(self):
        array_type, array = self.get_value()
        return array

    def get_self_default_value(self):
        return self.array_socket_type, [x.get_value() for x in self.value]

    def is_socket_compatible(self, other_socket):
        if not (other_socket.bl_idname == self.bl_idname or other_socket.bl_idname in self.compatible_sockets):
            return False
        if self.array_socket_type and other_socket.array_socket_type != self.array_socket_type:
            return False
        if self.required_length and len(other_socket.value) != self.required_length:
            return False

        return True

    def set_value(self, value):
        log.debug(value)
        if value:
            self.array_socket_type = value[0]
            value = value[1]

            self.value.clear()
            for x in value:
                item = self.value.add()
                item.array_type = self.array_socket_type
                item.set_value(x)
                log.debug(item.get_value())
        else:
            self.value.clear()
