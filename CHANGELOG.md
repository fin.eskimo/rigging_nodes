# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.9.2](https://gitlab.com/AquaticNightmare/rigging_nodes/-/releases/0_9_2)
## Added
- New nodes:
    - 'Expression' - It lets the user evaluate a python expression.
    - 'Collection to Array' - Ii converts a collection to an array of pointers.
- "Custom Script" node has been added back
- Grease pencil objects can now be added to the graph
- New options to hook the handles for the "hook to bones" node
- Options to unlink created objects from the node tree on the side panel
## Fixed
- "Hook to bones" node was not working using the "Single" behaviour
- Empty objects can now be used with the "Input Object" node
- Duplicated "Sub-Target" socket in the "Add Constraint" node renamed to "Sub-Target Space"
- "Make Array" node now shows the array types in order


## [0.9.1](https://gitlab.com/AquaticNightmare/rigging_nodes/-/releases/0_9_1)
## Added
- New socket:
    - Text datablock
- New nodes:
    - Set (Array)

## Changed
- More detailed node execution times
- Set bone roll will now automatically normalize the given vector

## Fixed
- Error with the edit datablock operator on input nodes
- Get collection and pointer property from a bone
- Cache node improvements

## [0.9.0](https://gitlab.com/AquaticNightmare/rigging_nodes/-/releases/0_9_0)
## Added
- New operator for editing the custom shape of a bone
    - Found under the "Item" category in the "VIEW 3D" editor, in the "Rigging Nodes" panel

- New nodes:
    - Edit Bone Groups (Add / Remove bone groups and set their color)
    - Sort (Array)
    - Sort Bones
    - Curve Mapping

- Addon settings for changing the naming convention:
    - "DEF"/"CTRL"/"MCH"/"TRGT" prefixes can be edited
    - the separator can be edited (by default it's set to ".") (the last separator used for the bone number will always be ".")
    - "left" and "right" flags can be edited (by default they are set to "L" and "R")

- "Rotate Bone" node can now rotate in the local space
- "Symmetrize" node:
    - new option to symmetrize all bones, one bone, or an array of bones
    - It can now output the symmetrized bone names
- "Join Armature" now can output the joined bone names

## Changed
- Pointer sockets can now be None when the property they are meant to point to does not exist 
- Pointer sockets can be connected to a bool socket to check if it is valid
- Driver sockets can be connected to a bool socket.
- Renamed "Add prop" node to "Add Custom Prop"
- Node trees in the background will not display their socket data
- Created objects mode will be respected after the node tree is executed
## Fixed
- Changing node options would sometimes make deleteable socket not deleteable
- Pointer sockets can now be connected to object sockets
- Symmetrize node was not working correctly when multiple armatures where being edited in the same tree
- Float to int conversion was not working
## Removed
- Removed "Custom Script" node from the node search menu (it needs a proper re-write)


## [0.8.1](https://gitlab.com/AquaticNightmare/rigging_nodes/-/releases/0_8_1)
## Added
- New node:
    - Filter Bones 

## [0.8.0](https://gitlab.com/AquaticNightmare/rigging_nodes/-/releases/0_8_0)
Old loops must be updated with the "Create Correct Loops" operator in the side panel
## Added
- Descriptions added to "Add Socket" operator
- Create bone name now supports suffixes
- "Flip Bone" node now has the option to keep the head position

## Changed
- Loops now work with the default input and output group nodes
- Loop related nodes have been moved from the "Flow Control" category to the "Loop" category
- "Get Bones" Node has the "Include Parent" socket and support arrays
- "Add constraint" node now can return Pointers as an optional output
- Renamed "PropertyGroup" category to "Pointers"

## Fixed
- Faster User Interface
- Modulo operation in the math node
- Tree being updated when changing a value even when "Auto Update" option was disabled


## [0.7.1](https://gitlab.com/AquaticNightmare/rigging_nodes/-/releases/0_7_1)
## Added
- New Nodes:
    - Reverse Array
    - Select (Control the execution flow with enumerators)
- Default values of sockets are now editable from inside a node group
- Reroute nodes now show the actual socket color and shape they are connected to
- Pointer sockets are now compatible with object and string sockets

## Fixed
- Optional outputs and inputs are no longer removed when changing a node's behaviour

## [0.7.0](https://gitlab.com/AquaticNightmare/rigging_nodes/-/releases/0_7_0)
## Added
- New array sockets. One type of array socket for each socket type, instead of one used for all types.
- Arrays now support matrix, quaterions, objects, actions, pointers and collections

- New sockets:
    - Pointer (Points to an object + datapath, for editing nested properties)
    - Quaterion

- New Nodes:
    - Pop (Array)
    - Insert (Array)
    - Math:
        - 6 operators for integers
        - 6 operators for floats
        - 26 operatos for vectors
        - 20 operators for matrix
        - 23 operators for quaternions
    - Create Matrix
        - identity
        - translation
        - rotation
        - scale
        - diagonal
        - ortho_projection
        - shear
    - Logic
        - And
        - Or
        - Not
    - String Operator:
        - +
        - *
        - [ ]
        - [ : ]
        - in
        - not_in
        - replace
    - Edit Collection:
        - Append
        - Get
        - Remove
        - Append Multiple (Returns an array of pointers)
    - Flip Bone
    - Cache (Experimental)
    

## Fixed
- Crashes when grouping nodes
- Improved execution times in heavy scenes (Having heavy meshes referencing the created armature in the scene was exponentially slowing down the creation of the rigs)
- Get children option in the get bone property node now correctly returns an array of strings

## Changed
- Values are no longer stored in the sockets, and their values are cleared when re-executing the tree.
- Linking groups from other .blend files will no longer link unnecessary armatures.
- Math nodes have been merged into a single node
- Logic nodes have been merged into a single node
- Sockets are no longer unlinked when changing a node's behaviour
- More nodes now support array inputs
- Set/Get property group nodes now require a pointer socket to be linked to them and be executed/valid for inputs or outputs to be selectable
- Matrix socket no longer has a default value, if nothing is linked to it, the default value will be an identity matrix
- Add to collection renamed to Edit Collection and added more opetions to it
- Sockets now keep their default values when crating new inputs in a node group
- Objects created in the node tree are relinked to the scene after the entire tree is executed (to avoid renaming vertex groups when renaming bones inside the tree)
- Boolean sockets are now compatible with other sockets and their values will be automatically converted to True or False
- Join Armature now removes the 2nd armature from the scene

## Removed
- Old Logic nodes
- Old math nodes
- Break vector
- Replace String
- To String node (different sockets can be directly connected to a string sockets now and their values will be automatically converted to string)



## [0.6.0](https://gitlab.com/AquaticNightmare/rigging_nodes/-/releases/0_6_0)
## Added
- New "Action" socket

- New nodes:
    - Rotate bone
    - Join Armature
    - Apply Action
    - [Geometry Functions](https://docs.blender.org/api/current/mathutils.geometry.html) (2D vector functions have been ignored)
- Set property nodes can now recognize actions (useful for action constraint)
- Pressing control while activating the "Edit Datablock" operator on the input nodes won't remove from scene the objects created by the current tree.
- "Bool" sockets are now compatible with strings, bones, enums, integers, floats and objects
- "Bone" sockets now show the possible bones if the node has another socket named "Armature"

## Fixed
- Executing the node tree from inside a node group sometimes failed

## Changed
- Set property nodes now respect the order in which the sockets are ordered (useful when changing the connect and parent properties of a bone at the same time)

## Removed
- "Reference Object" Node has been removed as it is now possible to pick the object directly from an object reference socket

## [0.5.0](https://gitlab.com/AquaticNightmare/rigging_nodes/-/releases/0_5_0)

Old node groups must be upgraded to this version with the "Create Correct Groups" operator in the side panel (n)

## Added
- It is now possible to pick an object directly from an object ref socket

## Changed
- Node groups now work with the default blender group input and group output nodes
- Multiple input and output nodes are now supported in the same node group
- Improved UI

## Fixed
- Error inspection operator when executed from inside a node group
- Node reconnection between not compatible sockets

## [0.4.0](https://gitlab.com/AquaticNightmare/rigging_nodes/-/releases/0_4_0)

Old node trees must be upgraded to this version with the "Update Nodes" operator in the side panel (n)

## Added
- Added buttons in the bone properties panel and bone constraints panel for selecting the nodes that edited those bones or constraints in the node tree
- When a node fails to execute, the entire chain of nodes will show an error
- Clicking on the error message of a node will send you to the node that made it fail
- Some nodes now accept some inputs to be passed as arrays
- Input sockets now have the option to output objects as references or as editable objects
- Some nodes now have optional inputs that can be added through a dropdown menu

- New Nodes:
    - Get bones
    - Get bone chain
    - Duplicate chain
    - Remove bone
    - Set bone roll
    - Extend array
    - Split array
    - Symmetrize
    - Create Bone Name

- Boolean and Float array sockets can have their default values changed directly in the node
- Nodes will fail to execute if their version is old. To upgrade them the "Upgrade Nodes" operator can be executed
- Option in Input nodes to add the output object to a collection

## Changed
- Nodes that required sockets to be hidden to be ignored now have those sockets as optional, will always be taken into account and can be added with a dropdown menu
- String and Enum sockets are now compatible with each other
- Update Nodes operator now updates every node in every node group in the file
- Merged get/set curve and armature nodes into the object get/set properties nodes 

## Fixed
- Issues with input nodes
- Updating sockets in node group
- Crashes related to enum properties
- Execution speed improvement by caching socket connections in dictionaries
- Slow UI due to a large ammount of links (solved by caching connections aswell)

## Removed
- Nodes:
    - "Is Subtree"
    - "Get Armature Property"
    - "Set Armature Property"
    - "Get Curve Property"
    - "set Curve Property"

## [0.3.0](https://gitlab.com/AquaticNightmare/rigging_nodes/-/releases/0_3_0)

## Update requirements
- To update files created with previous versions of this addon:
    1. Make a backup of the original file.
    2. Make sure all the "Custom reroute" nodes removed in the previous version (0.2.0) have already been deleted or replaced with actual reroute nodes.
    3. Update the addon
    4. Execute the operator "Create correct tree types" found in the node editor's side panel, under "Rigging Nodes Tree Settings". 

### Added
- Ability to correctly execute nodes inside groups, making working with groups much easier
- "Only update selected nodes" option for speeding up the interface redraw in trees with too many nodes (ideally for splitting it into more manageable sub-groups)
- New Nodes:
    - Armature:
        - Get armature property
        - Set armature property
        - Set armature layers
- Set variable node has a button to select all the "Get" nodes using that variable

## Fixed
- Node group creation errors (links getting disconnected, missplaced nodes and sockets changing their types)

## Changed
- Only main node trees will be able to be selected in the node editor
- Only node trees marked as groups will be selectable in the group node
- Only node trees marked as loops will be selectable in the loop node
- The order of the input sockets when creating a group is now based on the y position of the input nodes


## [0.2.0](https://gitlab.com/AquaticNightmare/rigging_nodes/-/releases/0_2_0)
### Added
- Lateral panel for showing node tree options (in the "Item" tab)
- New Node Tree Options:
    - Show Execution Times
    - Auto Update
    - Show "Hide" toggle in nodes

- New Nodes:
    - Drivers:
        - Add Driver Complex
        - Setup Driver
        - Setup Driver Variable
    - Vector:
        - Break
    - Bones:
        - Bone to datapath
        - Does Bone Exist
    - Constants:
        - Enum
    - String:
        - To String
    - Utilities:
        - Is Subtree

## Changed
- Update sockets button moved from the nodes to the side panel

### Removed
- Custom reroute node


## [0.1.0](https://gitlab.com/AquaticNightmare/rigging_nodes/-/releases/0_1_0)
### Added
- New nodes:
    - Utilities
        - **Custom Script** lets you assign a custom python script, any input conected to the node can be accessed as a global variable inside the script. It also lets you add outputs to the node by executing and inspecting the created global variables within the script.
    - Math:
        - Divide
    - Curve:
        - "Get curve property"
        - "Set curve property"
    - Vector:
        - Length
- Group input and output now let you delete sockets
- Compatibility with reroute nodes
    
### Changed
- Float and Integer sockets can now be connected
- moved some nodes from the Math section to the String and Vector sections 


## [0.0.2](https://gitlab.com/AquaticNightmare/rigging_nodes/-/releases/0_0_2)
### Added
- Now object created with nodes will be deleted on each tree execution. It's cleaner and more intuitive this way.
- Now references to the objects created in the nodetree are not lost when re-executing the nodes (armature modifiers for example will keep the reference to the rig created with the nodes)
- New nodes:
    - Input:
        all "input" nodes create copies of the original object so that their properties can be changed inside the nodetree
        - Input armature
        - Input curve
        - Input mesh
        - Input Object
        "reference" nodes don't allow you to modify their properties, but you can get their values and reference them inside the nodetree
        - Reference Object
    - Object:
        - "Get object property"
        - "Set object property"
    
### Changed
- Nodes appear ordered inside the add node menu

### Removed
- Copy object node replaced with the new input nodes


## [0.0.1](https://gitlab.com/AquaticNightmare/rigging_nodes/-/releases/0_0_1)
### Added
- First bundle of nodes added
- Shortcuts:
    - E: execute preview node
    - ctrl + G: group nodes
    - ctrl + J: frame nodes
    - TAB: edit/close node groups and loops
