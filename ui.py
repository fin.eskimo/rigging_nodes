from .sockets.socket import draw_times, get_value_times
from .runtime import cache_nodetree_times
import functools
import bpy


class BBN_UL_created_objects(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        row = layout.row()
        row.label(text=item.value.name)
        row.operator('bbn.remove_created_object', text='', icon='UNLINKED').index = index

    def invoke(self, context, event):
        pass


class BBN_PT_tree_panel(bpy.types.Panel):
    bl_idname = "BBN_PT_tree_panel"
    bl_label = "Rigging Nodes"
    bl_space_type = 'NODE_EDITOR'
    bl_region_type = 'UI'
    bl_category = "Item"

    @classmethod
    def poll(cls, context):
        # return True
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def draw(self, context):
        tree = context.space_data.node_tree
        if tree:
            layout = self.layout

            layout.label(text=tree.name)

            layout.template_list("BBN_UL_created_objects", "", tree, "objects", tree, "object_index", rows=4)

            layout.prop(tree, 'show_times', text='Show Execution Times')
            layout.prop(tree, 'auto_update', text='Auto Execute')

            #layout.operator("bbn.update_nodes", icon='FILE_REFRESH')
            #layout.operator("bbn.create_correct_tree_types", icon='FILE_REFRESH')
            #layout.operator("bbn.create_correct_groups", icon='FILE_REFRESH')
            #layout.operator("bbn.create_correct_loops", icon='FILE_REFRESH')

            for key, item in cache_nodetree_times.items():
                icon = 'NONE'
                if item < 0.01:
                    icon = 'KEYTYPE_JITTER_VEC'
                elif item < 0.1:
                    icon = 'KEYTYPE_MOVING_HOLD_VEC'
                else:
                    icon = 'KEYTYPE_EXTREME_VEC'
                layout.label(icon=icon, text=f"{key}: {item:.5f}")

            edit_tree = context.space_data.edit_tree
            if edit_tree in draw_times:
                layout.label(text="socket draw: {:.5f}".format(functools.reduce(lambda a, b: a + b, draw_times[edit_tree].values())))
            if edit_tree in get_value_times:
                layout.label(text="socket get value: {:.5f}".format(functools.reduce(lambda a, b: a + b, get_value_times[edit_tree].values())))


class BBN_PT_utils_panel(bpy.types.Panel):
    bl_idname = "BBN_PT_utils_panel"
    bl_label = "Rigging Nodes"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = "Item"

    @classmethod
    def poll(cls, context):
        return context.object and context.object.type == 'ARMATURE'

    def draw(self, context):
        layout = self.layout

        layout.operator('bbn.edit_bone_shape')


classes = [
    BBN_UL_created_objects,
    BBN_PT_tree_panel,
    BBN_PT_utils_panel
]


def register():
    from bpy.utils import register_class

    for cls in classes:
        register_class(cls)


def unregister():
    from bpy.utils import unregister_class

    for cls in reversed(classes):
        unregister_class(cls)
