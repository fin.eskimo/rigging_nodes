import logging
import sys
import bpy

import time
import traceback
import warnings

from ..exceptions import BBN_NodeException
from ..node_tree import BBN_tree
from ..runtime import cache_loop_inputs, cache_node_times, cache_node_dependants, MeasureTime, runtime_info

last_ex_modes = []

log = logging.getLogger(__name__)

# TODO change all imput and output socket dictionaries to OrderedDict
# Optional inputs don't matter


class BBN_node:
    subtype = 'STANDARD'
    log_time = True

    # versioning
    deprecated = False
    node_version = 0  # MUST ALWAYS BE 0 on base class
    current_node_version: bpy.props.IntProperty(default=0)

    # used to know if a node has already been executed
    last_ex_id: bpy.props.StringProperty()

    error_color = [1.0, 0.0, 0.0]
    preview_color = [0.2, 0.7, 0.7]

    _opt_input_sockets = {}
    behaviour_opt_sockets = {}

    @property
    def description(self):
        return ''

    @property
    def opt_input_sockets(self):
        if self.behaviour_enum:
            ans = {**self._opt_input_sockets, **self.behaviour_opt_sockets.get(self.current_behaviour, {}).get('INPUTS', {})}
        else:
            ans = self._opt_input_sockets
        for key in ans.keys():
            ans[key]['is_deletable'] = True
        return ans

    _opt_output_sockets = {}

    @property
    def opt_output_sockets(self):
        if self.behaviour_enum:
            ans = {**self._opt_output_sockets, **self.behaviour_opt_sockets.get(self.current_behaviour, {}).get('OUTPUTS', {})}
        else:
            ans = self._opt_output_sockets

        for key in ans.keys():
            ans[key]['is_deletable'] = True
        return ans

    behaviour_enum = []
    behaviour_sockets = {}

    input_sockets = {}
    output_sockets = {}

    input_to_focus = ''
    focus_mode = {}

    predefined_socket = ''

    # if True, the execute button will appear in the node
    executable = True

    error: bpy.props.BoolProperty()
    error_message: bpy.props.StringProperty()
    error_message_full: bpy.props.StringProperty()
    error_path: bpy.props.StringProperty()

    dependent_classes = []

    initializing: bpy.props.BoolProperty(default=False)

    behaviour_labels = {}

    def updated_behaviour(self, context):
        self.update_behaviour_sockets()
        # BBN_tree.value_updated(self, context)

    def get_execution_modes(self, context):
        global last_ex_modes
        last_ex_modes = self.behaviour_enum
        return last_ex_modes

    current_behaviour: bpy.props.EnumProperty(items=get_execution_modes, update=updated_behaviour)

    def update_behaviour_sockets(self):
        '''Dynamically created sockets are removed and behaviour specific sockets are added
        Other enum properties can call this method from their own update funcions'''

        new_inputs = self.behaviour_sockets.get(self.current_behaviour, {}).get('INPUTS', {})
        new_outputs = self.behaviour_sockets.get(self.current_behaviour, {}).get('OUTPUTS', {})

        for i in reversed(range(len(self.input_sockets), len(self.inputs))):
            if self.inputs[i].name not in new_inputs and self.inputs[i].name not in self.opt_input_sockets:
                self.inputs.remove(self.inputs[i])

        for i in reversed(range(len(self.output_sockets), len(self.outputs))):
            if self.outputs[i].name not in new_outputs and self.outputs[i].name not in self.opt_output_sockets:
                self.outputs.remove(self.outputs[i])

        # first check optional inputs because the obligatory inputs take preference on the socket type
        for socket_name, socket_info in self.opt_input_sockets.items():
            if socket_name in self.inputs and socket_name not in new_inputs.keys():
                self.change_socket(self.inputs, socket_name, socket_info)

        for socket_name, socket_info in self.opt_output_sockets.items():
            if socket_name in self.outputs and socket_name not in new_outputs.keys():
                self.change_socket(self.outputs, socket_name, socket_info)

        for socket_name, socket_info in new_inputs.items():
            self.change_socket(self.inputs, socket_name, socket_info)

        for socket_name, socket_info in new_outputs.items():
            self.change_socket(self.outputs, socket_name, socket_info)

    @ classmethod
    def poll(cls, ntree):
        nodetree_types = {'bbn_tree_group', 'bbn_tree', 'bbn_tree_loop'}
        return ntree.bl_idname in nodetree_types

    @ classmethod
    def register_dependants(cls):
        '''Registers operators or property groups that are required by this class'''
        from bpy.utils import register_class

        for cls in cls.dependent_classes:
            register_class(cls)

    @ classmethod
    def unregister_dependants(cls):
        '''Unregisters operators or property groups that are required by this class'''
        from bpy.utils import unregister_class

        for cls in cls.dependent_classes:
            unregister_class(cls)

    def init(self, context):
        # whenever a socket is created, the update function is called.
        # to avoid updating, check for this variable at the start of the update function
        self.initializing = True
        self.current_node_version = self.node_version
        self.update_colors()
        self.create_sockets()
        if self.behaviour_enum:
            self.update_behaviour_sockets()
        self.initializing = False

    def create_socket(self, sockets, name, info, check_created=True):
        if not check_created or name not in sockets:
            kwargs = info.copy()
            del kwargs['type']
            socket = sockets.new(info['type'], name)._init(**kwargs)
            return socket

    def create_sockets(self):
        for x, info in self.input_sockets.items():
            self.create_socket(self.inputs, x, info)

        for x, info in self.output_sockets.items():
            self.create_socket(self.outputs, x, info)

    def update(self):
        if runtime_info['updating']:
            return
        # return
        for x in self.inputs:
            x.remove_incorrect_links()

    def upgrade_node(self):
        '''this is executed if the current_version and node_version properties do not match
        the node is responsible of making them both match by overriding this function'''
        print(f'UPGRADING {self.name}... {self.current_node_version} > {self.node_version}')

    def change_socket(self, sockets, name, info):
        '''deletes and creates the socket with the new information while keeping the link

        socket : socket iterable
            node.inputs or node.outputs
        name : str/int
            the socket to search for
        info : dict
            the socket creation information
        '''
        connected_sockets = []
        orig_name = name

        index = None
        if type(name) is int:
            orig_socket = sockets[name]
            orig_name = orig_socket.name
        else:
            orig_socket = sockets.get(name)

        if orig_socket:
            if orig_socket.is_output:
                connected_sockets = [x.to_socket for x in orig_socket.links]
            else:
                connected_sockets = [x.from_socket for x in orig_socket.links]

            if type(name) is not int:
                index = next(x for x in range(0, len(sockets)) if sockets[x].name == name)
            else:
                index = name

            sockets.remove(sockets[index])
        new_socket = self.create_socket(sockets, orig_name, info, check_created=False)

        if index is not None:
            sockets.move(len(sockets) - 1, index)

        for x in connected_sockets:
            if new_socket.is_output:
                self.id_data.links.new(new_socket, x)
            else:
                self.id_data.links.new(x, new_socket)

        return new_socket

    def copy(self, node):
        self.update_colors()

    def update_colors(self):
        if self.name == self.id_data.preview_node:
            self.color = self.preview_color
            self.use_custom_color = True
        else:
            self.use_custom_color = False

        if self.error:
            self.color = self.error_color
            self.use_custom_color = True

    # TODO: replace real with raw
    def get_input_value(self, name, default=None, throw=False, required=False, force_list=False):
        with MeasureTime(self, 'Gather Inputs'):
            if type(name) is int:
                ans = self.inputs[name]
            else:
                ans = self.inputs.get(name)

            if ans:

                val = ans.get_value()

                if required and val is None:
                    raise ValueError(f'{name} is not connected or is not valid')

                if force_list and type(val) is not list:
                    val = [val]
                return val

            if throw:
                raise ValueError(f'"{name}" input does not exist')

            return default

    def has_output(self, name):
        return bool(self.outputs.get(name))

    def set_output_value(self, socket, value, required=False):
        with MeasureTime(self, 'Set Outputs'):
            if type(socket) is int:
                ans = self.outputs[socket]
            elif type(socket) is str:
                ans = self.outputs.get(socket)
            else:
                ans = socket

            if ans:
                ans.set_value(value)
            elif required:
                raise ValueError(f'{socket} output does not exist')

    def draw_buttons(self, context, layout):
        self.setup_buttons(context, layout)

    def draw_label(self):
        if self.behaviour_enum:
            return self.behaviour_labels.get(self.current_behaviour, self.bl_label)
        return self.bl_label

    def setup_buttons(self, context, layout):
        '''Creates the buttons and returns multiple layouts for editing'''
        layout = layout.column(align=True)

        options_tree = context.space_data.node_tree

        behaviour_row = layout.row(align=True)
        if self.behaviour_enum:
            behaviour_row.prop(self, 'current_behaviour', text='')

        main_row = layout.row(align=True)

        if self.opt_input_sockets and not all(x in self.inputs for x in self.opt_input_sockets):
            op = main_row.operator('bbn.add_socket', text='Add Input')
            op.socket_var = 'opt_input_sockets'
            op.node = self.name

        if self.opt_output_sockets and not all(x in self.outputs for x in self.opt_output_sockets):
            op = main_row.operator('bbn.add_socket', text='Add Output')
            op.socket_var = 'opt_output_sockets'
            op.node = self.name

        if self.predefined_socket:
            main_row.operator('bbn.add_predefined_socket').node = self.name

        if self.executable:
            if context.space_data.node_tree.bl_idname == 'bbn_tree' and context.space_data.edit_tree.bl_idname in {'bbn_tree', 'bbn_tree_group'}:
                main_row.operator("bbn.generate_rig", text="Set Preview", icon='RESTRICT_VIEW_OFF' if context.space_data.edit_tree.preview_node == self.name else 'RESTRICT_VIEW_ON').node_name = self.name

        if self.description:
            main_row.operator('bbn.show_node_description', text='', icon='INFO').value = self.description

        if self.error:
            txt = 'ERROR' if not self.error_message else self.error_message
            op = layout.operator('bbn.focus_error_node', text=txt, icon='ERROR', emboss=False)
            op.path = self.error_path
            op.error_msg = self.error_message_full

        if context.region.type == 'UI':
            outputs_to_show = self.outputs
            inputs_to_show = [x for x in self.inputs if x.connected_socket]
            if outputs_to_show:
                output_box = layout.box()
                output_box = output_box.column(align=True)
                output_box.prop(
                    options_tree,
                    'show_outputs_in_ui',
                    icon="TRIA_DOWN" if options_tree.show_outputs_in_ui else "TRIA_RIGHT",
                    emboss=False
                )

                if options_tree.show_outputs_in_ui:
                    for x in outputs_to_show:
                        x.draw(context, output_box, self, 'Output')

            if inputs_to_show:
                input_box = layout.box()
                input_box = input_box.column(align=True)

                input_box.prop(
                    options_tree,
                    'show_connected_inputs_in_ui',
                    icon="TRIA_DOWN" if options_tree.show_connected_inputs_in_ui else "TRIA_RIGHT",
                    emboss=False
                )
                if options_tree.show_connected_inputs_in_ui:
                    for x in inputs_to_show:
                        x.draw(context, input_box, self, 'Input')

        if options_tree.show_times and context.region.type != 'UI':
            if self.id_data in cache_node_times:
                if self in cache_node_times[self.id_data]:
                    times = cache_node_times[self.id_data][self]
                    for key, item in times.items():
                        self.draw_time(layout, key, item)

        return layout, behaviour_row, main_row

    def draw_time(self, layout, name, time):
        icon = 'NONE'
        if time < 0.01:
            icon = 'KEYTYPE_JITTER_VEC'
        elif time < 0.1:
            icon = 'KEYTYPE_MOVING_HOLD_VEC'
        else:
            icon = 'KEYTYPE_EXTREME_VEC'
        layout.label(icon=icon, text=f"{name}: {time:.5f}")

    def process(self, context, id, path):
        '''The node's main functionallity is implemented here'''
        pass

    def process_group(self, context, id, path):
        '''The node's main functionallity is implemented here'''
        pass

    def focus_on_object(self, focus, focus_mode, context=None):
        '''Utility function to focus an object and logs the time it took'''

        if bpy.context.active_object != focus:
            if bpy.ops.object.mode_set.poll():
                with MeasureTime(self, 'Unfocus'):
                    bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
            with MeasureTime(self, 'Focus'):
                if bpy.context.selected_objects:
                    for x in bpy.context.selected_objects[:]:
                        x.select_set(False)
                focus.select_set(True)
                if context:
                    context.view_layer.objects.active = focus
                else:
                    bpy.context.view_layer.objects.active = focus
        if focus_mode and focus.mode not in focus_mode:
            with MeasureTime(self, 'Mode Change'):
                try:
                    mode = next(iter(focus_mode))
                    bpy.ops.object.mode_set(mode=mode, toggle=False)
                except TypeError:
                    print(f'{focus} COULD NOT SET MODE "{mode}"')
                    pass

    def focus_input(self, context):
        '''Makes the object that will be modified the active object and its the mode'''
        if self.input_to_focus:
            focus = self.get_input_value(self.input_to_focus)
            if focus:
                self.focus_on_object(focus, self.focus_mode, context=context)

    def get_dependant_nodes(self):
        '''returns the nodes connected to the inputs of this node'''
        with MeasureTime(self, 'Get Dependants'):
            dep_tree = cache_node_dependants.setdefault(self.id_data, {})
            if self in dep_tree:
                return dep_tree[self]
            nodes = []
            for input in self.inputs:
                connected_socket = input.connected_socket

                if connected_socket and connected_socket not in nodes:
                    nodes.append(connected_socket.node)
            dep_tree[self] = nodes
            return nodes

    def path_to_node(self, path):
        node_tree = bpy.data.node_groups.get(path[0])
        for x in path[1:-1]:
            node_tree = node_tree.nodes.get(x).node_tree
        node = node_tree.nodes.get(path[-1])
        return node

    def execute_other(self, context, id, path, other):
        if hasattr(other, 'execute'):
            other.execute(context, id, path)
        else:
            if other.bl_rna.identifier == 'NodeGroupInput':
                if len(path) < 2:
                    raise ValueError(f'trying to setup the values of a nodegroup input on the upper level')
                node = self.path_to_node(path)
                assert node, f'{path} cannot be resolved to a node'
                for i, output in enumerate(other.outputs):
                    if output.bl_rna.identifier != 'NodeSocketVirtual':
                        if self.id_data.bl_idname == 'bbn_tree_group':
                            other_socket = node.inputs[i]
                            output.set_value(other_socket.get_value())
                        elif self.id_data.bl_idname == 'bbn_tree_loop':
                            if output.name in cache_loop_inputs.get(self.id_data, {}):
                                output.set_value(cache_loop_inputs[self.id_data][output.name])
                            else:
                                other_socket = node.inputs[i]
                                output.set_value(other_socket.get_value())

            elif other.bl_rna.identifier == 'NodeGroupOutput':
                nodes = set()
                for input in other.inputs:
                    if input.bl_rna.identifier != 'NodeSocketVirtual':
                        connected_socket = input.connected_socket

                        if connected_socket and connected_socket not in nodes:
                            node = connected_socket.node
                            self.execute_other(context, id, path, node)
                            nodes.add(node)

    def execute_dependants(self, context, id, path):
        '''Responsible of executing the required nodes for the current node to work'''
        for x in self.get_dependant_nodes():
            self.execute_other(context, id, path, x)

    def create_path_to_self(self, parent_path):
        return parent_path + [self.name]

    def execute(self, context, id, path):
        '''Executes the node and starts a reaction chain to execute all of its dependant nodes'''
        try:
            cache_node_times.setdefault(self.id_data, {})[self] = {}

            if self.last_ex_id == id:
                return

            if self.current_node_version != self.node_version:
                raise ValueError(f'Update the nodes with the "Update Nodes" operator in the side panel (N)')

            self.last_ex_id = id

            with MeasureTime(self, 'Dependants'):
                self.execute_dependants(context, id, path)

            self.focus_input(context)

            with MeasureTime(self, 'Group'):
                self.process_group(context, id, path)

            with MeasureTime(self, 'Execution'):
                with warnings.catch_warnings(record=True) as w:
                    warnings.simplefilter("always")

                    self.process(context, id, path)

                    if w:
                        print([a.message for a in w])

        except BBN_NodeException as e:
            # if a custom exception is detected, just raise it
            with MeasureTime(self, 'Error Handling'):
                self.error = True
                self.update_colors()
                self.error_message = e.info
                self.error_message_full = e.traceback
                full_list = e.path + [e.node.name]
                self.error_path = '////'.join(full_list)
                raise

        except Exception as e:
            # if an unknown exception occurred, create a custom exception with all the error information, and raise it
            with MeasureTime(self, 'Error Handling'):
                self.error = True
                self.update_colors()
                self.error_message = str(e)
                self.error_message_full = traceback.format_exc()
                full_list = path + [self.name]
                self.error_path = '////'.join(full_list)

                raise BBN_NodeException(self, path, str(e))

        else:
            # if no exceptions occurred, update the node colors
            if self.error:
                self.error = False
                self.update_colors()
