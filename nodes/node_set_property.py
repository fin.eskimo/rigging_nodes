import bpy
import math
from bpy.types import Node
import time

from .node_base import BBN_node

from mathutils import Vector

import logging
log = logging.getLogger(__name__)

props_dict = {}


class BBN_node_set_property(BBN_node):
    bl_idname = 'bbn_set_property_node'
    bl_label = "Set Bone Property"
    bl_icon = 'BONE_DATA'

    props_to_get = {'name', 'description', 'type', 'subtype', 'enum_items', 'default', 'fixed_type', 'icon', 'is_array', 'array_dimensions', 'array_length', 'default_array'}

    behaviour_enum = [
        ('SINGLE', 'Single', 'Only affect one bone', '', 0),
        ('MULTIPLE', 'Multiple', 'Change property of multiple bones', '', 2),
        ('MULTIPLE_VALUES', 'Multiple Values', 'Change property of multiple bones', '', 4),
    ]

    def draw_label(self):
        if len(self.inputs) == len(self.input_sockets.keys()) + 1:
            return f'Set {self.inputs[(len(self.inputs) - 1)].name}'
        return self.bl_label

    @property
    def props_class(self):
        return bpy.types.PoseBone

    def get_props(self):
        if not self.props_class:
            return {}
        if self.props_class in props_dict:
            return props_dict[self.props_class]
        ans = {x.identifier: {y.identifier: getattr(x, y.identifier) for y in x.bl_rna.properties if y.identifier in self.props_to_get} for x in self.props_class.bl_rna.properties if not x.is_readonly}

        if 'space_subtarget' in ans:
            ans['space_subtarget']['name'] = 'Sub-Target Space'

        props_dict[self.props_class] = ans
        return ans

    @ property
    def opt_input_sockets(self):
        props = self.get_props()
        ans = {}
        if self.current_behaviour != 'MULTIPLE_VALUES':
            for prop, prop_info in props.items():
                if prop_info['type'] == 'STRING':
                    ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_socket', 'default_value': prop_info['default']}
                elif prop_info['type'] == 'ENUM':
                    ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_enum_socket', 'default_value': prop_info['default'], 'items': [(x.identifier, x.name, x.description, x.icon, i) for i, x in enumerate(prop_info['enum_items'])]}
                elif prop_info['type'] == 'BOOLEAN':
                    if prop_info['is_array'] is False:
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_bool_socket', 'default_value': prop_info['default']}
                    elif prop_info['array_length'] < 5:
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_bool_array_socket', 'required_length': prop_info['array_length']}
                    else:
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_int_socket'}
                elif prop_info['type'] == 'POINTER':
                    if prop_info['fixed_type'].bl_rna.identifier == 'Object':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_object_ref_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'EditBone':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_bone_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Bone':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_bone_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'PoseBone':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_bone_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Object':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_object_ref_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Action':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_action_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Text':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_text_socket'}
                elif prop_info['type'] == 'FLOAT':
                    if prop_info['is_array'] is False:
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_float_socket', 'default_value': prop_info['default']}
                    else:
                        if prop_info['array_length'] == 3:
                            ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_vector_socket', 'default_value': prop_info['default_array']}
                        elif prop_info['subtype'] == 'MATRIX':
                            if prop_info['array_length'] == 16:
                                ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_matrix_socket', 'default_value': prop_info['default_array']}
                        else:
                            ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_float_array_socket', 'required_length': prop_info['array_length'], 'default_value': prop_info['default_array']}
                elif prop_info['type'] == 'INT':
                    ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_int_socket', 'default_value': prop_info['default']}
                else:
                    pass
                    # print(prop)
        else:
            for prop, prop_info in props.items():
                if prop_info['type'] == 'STRING':
                    ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_array_v2_socket'}
                elif prop_info['type'] == 'ENUM':
                    ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_array_v2_socket'}
                elif prop_info['type'] == 'BOOLEAN':
                    if prop_info['is_array'] is False:
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_bool_array_socket'}
                    elif prop == 'layers':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_int_array_socket'}
                elif prop_info['type'] == 'POINTER':
                    if prop_info['fixed_type'].bl_rna.identifier == 'Object':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_object_ref_array_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'EditBone':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_array_v2_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Bone':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_array_v2_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'PoseBone':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_string_array_v2_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Object':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_object_ref_array_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Action':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_action_array_socket'}
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Text':
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_text_array_socket'}
                elif prop_info['type'] == 'FLOAT':
                    if prop_info['is_array'] is False:
                        ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_float_array_socket'}
                    else:
                        if prop_info['array_length'] == 3:
                            ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_vector_array_socket'}
                        elif prop_info['subtype'] == 'MATRIX' and prop_info['array_length'] == 16:
                            ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_matrix_array_socket'}
                elif prop_info['type'] == 'INT':
                    ans[prop] = {'display_name': prop_info['name'], 'description': prop_info['description'], 'type': 'BBN_int_array_socket'}
                else:
                    pass
                    # print(prop)
        for key in ans.keys():
            ans[key]['is_deletable'] = True
        return ans

    # TODO: replace id with from_id()

    def get_socket_data(self, id, socket, prop_name):
        val = socket.get_real_value()

        prop_info = self.get_props()[prop_name]

        if prop_info['type'] == 'BOOLEAN' and prop_info['array_length'] >= 5:
            val = tuple(i == val for i in range(prop_info['array_length']))

        if prop_info['type'] == 'POINTER':
            val = socket.get_real_value()
            if self.current_behaviour != 'MULTIPLE_VALUES':
                if val:
                    if prop_info['fixed_type'].bl_rna.identifier == 'EditBone':
                        return id.data.edit_bones[val]
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Bone':
                        return id.data.bones[val]
                    elif prop_info['fixed_type'].bl_rna.identifier == 'PoseBone':
                        return id.pose.bones[val]
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Object':
                        return val
                    else:
                        return val
                else:
                    if prop_info['fixed_type'].bl_rna.identifier == 'EditBone':
                        return None
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Bone':
                        return None
                    elif prop_info['fixed_type'].bl_rna.identifier == 'PoseBone':
                        return None
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Object':
                        return None
                    else:
                        return val
            else:
                if val:
                    if prop_info['fixed_type'].bl_rna.identifier == 'EditBone':
                        return [id.data.edit_bones[x] if x else None for x in val]
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Bone':
                        return [id.data.bones[x] if x else None for x in val]
                    elif prop_info['fixed_type'].bl_rna.identifier == 'PoseBone':
                        return [id.pose.bones[x] if x else None for x in val]
                    elif prop_info['fixed_type'].bl_rna.identifier == 'Object':
                        return val
                    else:
                        return val

        return val

    def set_prop(self, id, socket, obj, prop_name, index=0):
        val = self.get_socket_data(id, socket, prop_name)

        if self.current_behaviour == 'SINGLE' or self.current_behaviour == 'MULTIPLE_SUBTARGETS' or not hasattr(obj, '__iter__'):
            setattr(obj, prop_name, val)
        elif self.current_behaviour == 'MULTIPLE':
            for x in obj:
                setattr(x, prop_name, val)
        elif self.current_behaviour == 'MULTIPLE_VALUES':
            if len(val) != len(obj):
                raise ValueError(f"Array lengths of {prop_name} don't match")
            for i, x in enumerate(obj):
                setattr(x, prop_name, val[i])

    def set_props(self, id, obj, index=0):
        for socket in self.inputs:
            if socket.name in self.opt_input_sockets.keys():
                self.set_prop(id, socket, obj, socket.name)

            else:
                pass
                #print(f'{self} {socket.name} was not set')
