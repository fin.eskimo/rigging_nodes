import bpy
from bpy.types import Node
from ..node_base import BBN_node

from ...node_tree import BBN_tree


class BBN_node_float(Node, BBN_node):
    bl_idname = 'bbn_float_node'
    bl_label = "Float"
    bl_icon = 'IPO_CONSTANT'

    value: bpy.props.FloatProperty(update=BBN_tree.value_updated)

    output_sockets = {
        'Value': {'type': 'BBN_float_socket'},
    }

    def draw_buttons(self, context, layout):
        layout.prop(self, "value")

    def process(self, context, id, path):
        self.outputs['Value'].set_value(self.value)
