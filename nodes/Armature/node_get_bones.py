import bpy
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_get_bones(Node, BBN_node):
    bl_idname = 'bbn_node_get_bones'
    bl_label = "Get Bones"
    bl_icon = 'BONE_DATA'

    input_sockets = {
        'Armature': {'type': 'BBN_object_ref_socket'},
    }

    behaviour_enum = [
        ('ALL', 'All', '', '', 0),
        ('CHILDREN', 'Children', '', '', 1),
        ('MULTIPLE_CHILDREN', 'Multiple Children', '', '', 2),
    ]

    # TODO get bones by tag?
    behaviour_sockets = {
        'ALL': {
        },
        'CHILDREN': {
            'INPUTS': {
                'Parent Bone': {'type': 'BBN_bone_socket'},
                'Include Parent': {'type': 'BBN_bool_socket'},
            }
        },
        'MULTIPLE_CHILDREN': {
            'INPUTS': {
                'Parent Bone': {'type': 'BBN_string_array_v2_socket'},
                'Include Parent': {'type': 'BBN_bool_socket'},
            }
        },
    }

    output_sockets = {
        'Bones': {'type': 'BBN_string_array_v2_socket'},
    }

    input_to_focus = 'Armature'
    focus_mode = {'OBJECT', 'POSE'}

    def process(self, context, id, path):
        armature = self.get_input_value("Armature", required=True)

        ans = []
        if self.current_behaviour == 'ALL':
            for bone in armature.data.bones:
                ans.append(bone.name)
        elif self.current_behaviour == 'CHILDREN' or self.current_behaviour == 'MULTIPLE_CHILDREN':
            bone_names = self.get_input_value("Parent Bone", force_list=True)
            bones = [armature.data.bones.get(x) for x in bone_names]
            if self.get_input_value("Include Parent", default=False):
                ans.extend(bone_names)

            while bones:
                for child in bones[0].children:
                    ans.append(child.name)
                    bones.append(child)
                bones.pop(0)

        self.set_output_value('Bones', ans)
