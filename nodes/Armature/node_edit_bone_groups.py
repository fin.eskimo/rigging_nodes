import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ...node_tree import BBN_tree


class BBN_node_edit_bone_groups(Node, BBN_node):
    bl_idname = 'bbn_node_edit_bone_groups'
    bl_label = "Edit Bone Groups"
    bl_icon = 'OUTLINER_OB_ARMATURE'

    output_sockets = {
        'Armature': {'type': 'BBN_object_socket'}
    }

    input_sockets = {
        'Armature': {'type': 'BBN_object_socket'}
    }

    behaviour_enum = [
        ('ADD', 'Add', 'Add'),
        ('REMOVE', 'Remove', 'Remove')
    ]

    behaviour_opt_sockets = {
        'ADD': {
            'INPUTS': {
                'active color': {'type': 'BBN_vector_socket'},
                'normal color': {'type': 'BBN_vector_socket'},
                'select color': {'type': 'BBN_vector_socket'},
            },
        }
    }

    behaviour_sockets = {
        'ADD': {
            'INPUTS': {
                'Name': {'type': 'BBN_string_socket'},
                'Color Set': {'type': 'BBN_enum_socket',
                              'items': [(x.identifier, x.name, x.description, x.icon, i) for i, x in enumerate(bpy.types.BoneGroup.bl_rna.properties['color_set'].enum_items)]
                              },
            },
            'OUTPUTS': {
                'Index': {'type': 'BBN_int_socket'},
            },
        },
        'REMOVE': {
            'INPUTS': {
                'Index': {'type': 'BBN_int_socket'},
            },
            'OUTPUTS': {

            },
        }
    }

    input_to_focus = 'Armature'
    focus_mode = {'POSE'}

    def process(self, context, id, path):
        armature = self.get_input_value("Armature")
        bgs = armature.pose.bone_groups

        if self.current_behaviour == 'ADD':
            bg = bgs.new(name=self.get_input_value("Name"))
            bg.color_set = self.get_input_value("Color Set")
            self.set_output_value('Index', len(bgs) - 1)
            active = self.get_input_value("active color")
            normal = self.get_input_value("normal color")
            select = self.get_input_value("select color")
            if active:
                bg.colors.active = active
            if normal:
                bg.colors.normal = normal
            if select:
                bg.colors.select = select

        if self.current_behaviour == 'REMOVE':
            bg = bgs[self.get_input_value("Index")]
            bgs.remove(bg)

        self.set_output_value(0, armature)
