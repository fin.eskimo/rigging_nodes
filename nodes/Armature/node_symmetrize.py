import bpy
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_symmetrize(Node, BBN_node):
    bl_idname = 'bbn_node_symmetrize'
    bl_label = "Symmetrize"
    bl_icon = 'BONE_DATA'

    input_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
        'Direction': {'type': 'BBN_enum_socket', 'items': [(x.identifier, x.name, x.description, x.icon, i) for i, x in enumerate(bpy.ops.armature.symmetrize.get_rna_type().bl_rna.properties['direction'].enum_items)]}
    }

    behaviour_enum = [
        ('ALL', 'All', 'All'),
        ('SINGLE', 'Single', 'Single'),
        ('MULTIPLE', 'Multiple', 'Multiple'),
    ]

    behaviour_sockets = {
        'SINGLE': {
            'INPUTS': {
                'Bones': {'type': 'BBN_bone_socket'},
            },
            'OUTPUTS': {
                'Bones': {'type': 'BBN_string_socket'},
            },
        },
        'MULTIPLE': {
            'INPUTS': {
                'Bones': {'type': 'BBN_string_array_v2_socket'},
            },
            'OUTPUTS': {
                'Bones': {'type': 'BBN_string_array_v2_socket'},
            },
        },
    }

    output_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
    }

    input_to_focus = 'Armature'
    focus_mode = {'EDIT'}

    def process(self, context, id, path):
        armature = self.get_input_value("Armature", required=True)
        bones = self.get_input_value('Bones', force_list=True)
        direction = self.get_input_value('Direction', default='NEGATIVE_X')

        orig_vis_layers = list(armature.data.layers)
        armature.data.layers = [True] * 32

        mirror_bones = []
        if bones:
            bpy.ops.armature.select_all(action='DESELECT')
            for bone_name in bones:
                armature.data.edit_bones[bone_name].select_head = True
                armature.data.edit_bones[bone_name].select_tail = True
                armature.data.edit_bones[bone_name].select = True
        elif self.current_behaviour == 'ALL':
            bpy.ops.armature.select_all(action='SELECT')
        else:
            self.set_output_value('Armature', armature)
            if self.has_output('Bones'):
                if self.current_behaviour == 'SINGLE':
                    self.set_output_value('Bones', '')
                elif self.current_behaviour == 'MULTIPLE':
                    self.set_output_value('Bones', [])
            return

        # symetrize operator does not work with context selected bones sadly
        # TODO having a symmetrize node with an invalid action constraint crashes blender when trying to symmetrice right here
        # https://developer.blender.org/T84988
        bpy.ops.armature.symmetrize(direction=direction)

        if self.has_output('Bones'):
            if self.current_behaviour == 'SINGLE':
                self.set_output_value('Bones', next(x.name for x in armature.data.edit_bones if x.select))
            elif self.current_behaviour == 'MULTIPLE':
                self.set_output_value('Bones', [x.name for x in armature.data.edit_bones if x.select])

        armature.data.layers = orig_vis_layers
        self.set_output_value('Armature', armature)
