import bpy
from bpy.types import Node

from ..node_base import BBN_node
from ...runtime import MeasureTime


class BBN_node_join_armature(Node, BBN_node):
    bl_idname = 'bbn_node_join_armature'
    bl_label = "Join Armature"
    bl_icon = 'BONE_DATA'

    input_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
        'Armature 2': {'type': 'BBN_object_ref_socket'},
    }

    output_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
    }

    _opt_input_sockets = {
        'New Parent': {'type': 'BBN_bone_socket', 'description': 'Assigns the defined bone as the new parent of all the joined bones that have no parent'},
    }

    _opt_output_sockets = {
        'Bones': {'type': 'BBN_string_array_v2_socket'},
    }

    input_to_focus = 'Armature'
    focus_mode = {'OBJECT', 'POSE'}

    def process(self, context, id, path):
        armature = self.get_input_value("Armature", required=True)
        armature2 = self.get_input_value("Armature 2", required=True)
        new_parent = self.get_input_value("New Parent")

        with MeasureTime(self, 'Copy Data'):
            copied_armature = armature2.copy()
            copied_data = armature2.data.copy()
            copied_armature.data = copied_data

        collect_bones = self.has_output('Bones')

        if new_parent or self.has_output('Bones'):
            with MeasureTime(self, 'flag bones'):
                for x in copied_data.bones:
                    x['__BBN_joining_flag__'] = 1

        override = context.copy()
        override['selected_objects'] = override['selected_editable_objects'] = [armature, copied_armature]
        override['object'] = override['active_object'] = armature
        with MeasureTime(self, 'Join'):
            bpy.ops.object.join(override)

        joined_bones = []

        if new_parent:
            with MeasureTime(self, 'Set New Parent'):
                self.focus_on_object(armature, {'EDIT'})

                try:
                    new_parent_bone = armature.data.edit_bones[new_parent]
                except KeyError:
                    raise ValueError(f'{new_parent} bone does not exist')

                for x in armature.data.edit_bones:
                    if '__BBN_joining_flag__' in x:
                        if not x.parent:
                            x.parent = new_parent_bone
                        if collect_bones:
                            joined_bones.append(x.name)
                        del x['__BBN_joining_flag__']

        elif collect_bones:
            with MeasureTime(self, 'Get Joined Bones'):
                for x in armature.data.bones:
                    if '__BBN_joining_flag__' in x:
                        joined_bones.append(x.name)
                        del x['__BBN_joining_flag__']

        bpy.data.objects.remove(copied_armature, do_unlink=True)
        bpy.data.armatures.remove(copied_data, do_unlink=True)

        context.space_data.node_tree.unregister_object(context, armature2)

        self.set_output_value('Armature', armature)
        self.set_output_value('Bones', joined_bones)
