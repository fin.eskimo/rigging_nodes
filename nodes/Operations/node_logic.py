import bpy
import operator
from bpy.types import Node

from ..node_base import BBN_node
from ...runtime import runtime_info
from collections import OrderedDict

operation_functions = OrderedDict(
    AND=operator.and_,
    NOT=operator.not_,
    OR=operator.or_,
)


class BBN_node_logic(Node, BBN_node):
    bl_idname = 'bbn_node_logic'
    bl_label = "Logic"
    bl_icon = 'SYNTAX_OFF'

    input_sockets = {
        'Val 1': {'type': 'BBN_bool_socket'},
        # 'Val 2': {'type': 'BBN_bool_socket'},
    }

    behaviour_enum = [
        ('AND', 'And', 'And'),
        ('OR', 'Or', 'Or'),
        ('NOT', 'Not', 'Not'),
    ]

    behaviour_sockets = {
        'AND': {
            'INPUTS': {
                'Val 2': {'type': 'BBN_bool_socket'}
            }
        },
        'OR': {
            'INPUTS': {
                'Val 2': {'type': 'BBN_bool_socket'}
            }
        },

    }

    output_sockets = {
        'Result': {'type': 'BBN_bool_socket'},
    }

    def draw_label(self):
        return f"{self.current_behaviour.title()}"

    def process(self, context, id, path):
        inputs = [self.get_input_value(i) for i in range(0, len(self.inputs))]

        op = operation_functions[self.current_behaviour]

        self.outputs[0].set_value(op(*inputs))
