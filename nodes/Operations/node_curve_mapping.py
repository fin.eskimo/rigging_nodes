import bpy
import operator
from bpy.types import Node

from ..node_base import BBN_node
from ...runtime import runtime_info
from collections import OrderedDict


class BBN_node_curve_mapping(Node, BBN_node):
    bl_idname = 'bbn_node_curve_mapping'
    bl_label = "Curve Mapping"
    bl_icon = 'SYNTAX_OFF'

    input_sockets = {
        'Value': {'type': 'BBN_float_socket', 'default_value': 0.5},
    }

    output_sockets = {
        'Result': {'type': 'BBN_float_socket'},
    }

    curve_tree: bpy.props.PointerProperty(type=bpy.types.NodeTree)

    def init(self, context):
        self.curve_tree = bpy.data.node_groups.new("__curve_data__", "ShaderNodeTree")
        curve_node = self.curve_tree.nodes.new('ShaderNodeRGBCurve')
        super().init(context)

    def draw_buttons(self, context, layout):
        row1, row2, layout2 = self.setup_buttons(context, layout)
        layout.template_curve_mapping(self.curve_tree.nodes[0], "mapping")

    def process(self, context, id, path):
        val = self.get_input_value('Value')
        myCurveMapping = self.curve_tree.nodes[0].mapping  # the CurveMapping
        myCurveMapping.initialize()  # it needs to be initialized
        curve = myCurveMapping.curves[3]  # the Composite-Curve of the RGB Curves
        ans = myCurveMapping.evaluate(curve, val)
        self.outputs[0].set_value(ans)
