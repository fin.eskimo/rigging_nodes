import bpy
import warnings
from bpy.types import Node

from ..node_set_property import BBN_node_set_property

from mathutils import Vector


class BBN_node_set_bone_property(Node, BBN_node_set_property):
    bl_idname = 'bbn_set_bone_property_node'
    bl_label = "Set Bone Property"
    bl_icon = 'BONE_DATA'

    node_version = 2

    input_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
        'Bone': {'type': 'BBN_bone_socket'},
    }

    output_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
        'Bone': {'type': 'BBN_string_socket'},
    }

    behaviour_sockets = {
        'SINGLE': {
            'INPUTS': {
                'Bone': {'type': 'BBN_bone_socket'},
            },
            'OUTPUTS': {
                'Bone': {'type': 'BBN_bone_socket'},
            },
        },
        'MULTIPLE': {
            'INPUTS': {
                'Bone': {'type': 'BBN_string_array_v2_socket'},
            },
            'OUTPUTS': {
                'Bone': {'type': 'BBN_string_array_v2_socket'},
            },
        },
        'MULTIPLE_VALUES': {
            'INPUTS': {
                'Bone': {'type': 'BBN_string_array_v2_socket'},
            },
            'OUTPUTS': {
                'Bone': {'type': 'BBN_string_array_v2_socket'},
            },
        },
    }

    def updated_enum(self, context):
        for i in reversed(range(2, len(self.inputs))):
            self.inputs.remove(self.inputs[i])

    bone_mode: bpy.props.EnumProperty(items=[('EDIT', 'Edit', 'Edit Bone Properties'), ('POSE', 'Pose', 'Pose Bone Properties'), ('BONE', 'Bone', 'Bone Properties')], update=updated_enum)

    input_to_focus = 'Armature'

    @ property
    def props_class(self):
        if self.bone_mode == 'EDIT':
            return bpy.types.EditBone
        elif self.bone_mode == 'POSE':
            return bpy.types.PoseBone
        elif self.bone_mode == 'BONE':
            return bpy.types.Bone

    @ property
    def focus_mode(self):
        if self.bone_mode == 'EDIT':
            return {'EDIT'}
        elif self.bone_mode == 'POSE':
            return {'POSE', 'OBJECT'}
        elif self.bone_mode == 'BONE':
            return {'OBJECT', 'POSE'}

    def draw_buttons(self, context, layout):
        layout, row1, row2 = self.setup_buttons(context, layout)
        row1.prop(self, 'bone_mode', text='')

    def upgrade_node(self):
        super().upgrade_node()

        if self.current_node_version == 0:
            inputs = self.inputs[2:]
            for x in inputs:
                if x.hide:
                    self.inputs.remove(x)

        if 'execute_mode' in self.keys():
            self['current_behaviour'] = self['execute_mode']
            del self['execute_mode']

        self.current_node_version = self.node_version

    def process(self, context, id, path):
        armature = self.get_input_value("Armature", required=True)
        bone_names = self.get_input_value("Bone")

        bones = []
        if self.current_behaviour == 'SINGLE':
            bone_names = [bone_names]

        if self.bone_mode == 'POSE':
            bones = [armature.pose.bones[x] for x in bone_names]
        elif self.bone_mode == 'EDIT':
            bones = [armature.data.edit_bones[x] for x in bone_names]
        elif self.bone_mode == 'BONE':
            bones = [armature.data.bones[x] for x in bone_names]

        if self.current_behaviour == 'SINGLE':
            self.set_props(armature, bones[0])
        else:
            self.set_props(armature, bones)

        for bone in bones:
            if bone.length <= 0:
                raise ValueError(f'The bone "{bone.name}" has 0 length\nuse_connect, matrix, head, tail and length attributes can cause this')
            armature.data.BBN_info.add_bone_tracking(bone.name, self, path)

        self.outputs['Armature'].set_value(armature)

        # instead of returning the original names, re-fetch them because their name could have changed
        if self.current_behaviour == 'SINGLE':
            self.outputs['Bone'].set_value(bones[0].name)
        else:
            self.outputs['Bone'].set_value([x.name for x in bones])
