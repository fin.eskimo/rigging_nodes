import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_add_bone(Node, BBN_node):
    bl_idname = 'bbn_add_bone_node'
    bl_label = "Add Bone"
    bl_icon = 'BONE_DATA'

    node_version = 3

    behaviour_enum = [
        ('MANUAL', 'Manual', 'Using head, tail and roll vectors'),
        ('COPY_BONE', 'Copy from Bone', 'Copy from bone'),
        ('SMART', 'Smart', 'It will create the bone in the given position with the given size, pointing in the given direction'),
        ('DIRECTION', 'Direction', 'It will create the bone in the given position with the given size, pointing in the given direction'),
        ('MATRIX', 'Matrix', 'It will create the bone in the given position with the given size, pointing in the given direction'),
    ]

    behaviour_sockets = {
        'MANUAL': {
            'INPUTS': {
                'Head': {'type': 'BBN_vector_socket', 'default_value': [0, 0, 0]},
                'Tail': {'type': 'BBN_vector_socket', 'default_value': [0, 1, 0]},
                'Roll': {'type': 'BBN_float_socket'},
            }
        },
        'COPY_BONE': {
            'INPUTS': {
                'Copy Bone': {'type': 'BBN_bone_socket'},
                'Scale': {'type': 'BBN_float_socket', 'default_value': 1},
            }
        },
        'SMART': {
            'INPUTS': {
                'Position': {'type': 'BBN_vector_socket'},
                'Length': {'type': 'BBN_float_socket', 'default_value': 1},
                'Direction': {'type': 'BBN_enum_socket', 'items': ['Y', '-Y', 'X', '-X', 'Z', '-Z', ]},
            }
        },
        'DIRECTION': {
            'INPUTS': {
                'Position': {'type': 'BBN_vector_socket'},
                'Length': {'type': 'BBN_float_socket', 'default_value': 1},
                'Direction': {'type': 'BBN_vector_socket', 'default_value': [0, 1, 0]},
            }
        },
        'MATRIX': {
            'INPUTS': {
                'Matrix': {'type': 'BBN_matrix_socket'},
            }
        },
    }

    _opt_input_sockets = {
        'Parent': {'type': 'BBN_bone_socket'},

        'Use Connect': {'type': 'BBN_bool_socket', 'default_value': True},
        'Use Deform': {'type': 'BBN_bool_socket', 'default_value': True},

        'Layer': {'type': 'BBN_int_socket'},

        'Shape': {'type': 'BBN_object_ref_socket'},
        'Shape Scale': {'type': 'BBN_float_socket', 'default_value': 1},
        'Shape Follow': {'type': 'BBN_bone_socket'},

        'Rotation Mode': {'type': 'BBN_enum_socket', 'default_value': 'XYZ', 'items': [
            'QUATERNION',
            'XYZ',
            'XZY',
            'YXZ',
            'YZX',
            'ZXY',
            'XYZ',
            'AXIS_ANGLE']
        },
    }

    input_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
        'Name': {'type': 'BBN_string_socket'},  # it should be string, or the selection popup will appear
    }

    output_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
        'Bone': {'type': 'BBN_string_socket'},
    }

    input_to_focus = 'Armature'
    focus_mode = {'EDIT'}

    def upgrade_node(self):
        super().upgrade_node()

        # remap old properties
        if 'transform_creation_type' in self.keys():
            self['current_behaviour'] = self['transform_creation_type']
            del self['transform_creation_type']

        self.current_node_version = self.node_version

    def draw_label(self):
        name_socket = self.inputs.get('Name')
        if name_socket:
            name = name_socket.get_value()
            if name:
                return 'Add Bone: ' + name_socket.get_value()
        return 'Add Bone'

    def process(self, context, id, path):
        armature = self.get_input_value("Armature", required=True)
        parent_bone_name = self.get_input_value("Parent", default='')
        if parent_bone_name:
            parent_bone = bpy.context.active_object.data.edit_bones.get(parent_bone_name)
            if not parent_bone:
                raise ValueError(f'Parent bone "{parent_bone_name}" is not valid')
        else:
            parent_bone = None

        out_bone = bpy.context.active_object.data.edit_bones.new(self.get_input_value("Name"))

        if self.current_behaviour == 'MANUAL':
            out_bone.head = self.get_input_value("Head")
            out_bone.tail = self.get_input_value("Tail")
            out_bone.roll = self.get_input_value("Roll")
        elif self.current_behaviour == 'COPY_BONE':
            from_bone = armature.data.edit_bones.get(self.get_input_value("Copy Bone"))
            scale = self.get_input_value("Scale")

            from_matrix = from_bone.matrix.copy()

            pos = from_matrix.translation.copy()
            direction = from_matrix.col[1].copy() * from_bone.length
            roll_z = from_matrix.col[2].copy()

            roll_z.resize_3d()
            direction.resize_3d()

            out_bone.head = pos
            out_bone.tail = pos + (direction * scale)
            out_bone.align_roll(roll_z)
        elif self.current_behaviour == 'SMART':
            pos = self.get_input_value("Position")

            direction = self.get_input_value("Direction")
            if direction == 'Y':
                direction = mathutils.Vector((0, 1, 0))
            elif direction == '-Y':
                direction = mathutils.Vector((0, -1, 0))
            elif direction == 'X':
                direction = mathutils.Vector((1, 0, 0))
            elif direction == '-X':
                direction = mathutils.Vector((-1, 0, 0))
            elif direction == 'Z':
                direction = mathutils.Vector((0, 0, 1))
            elif direction == '-Z':
                direction = mathutils.Vector((0, 0, -1))

            length = self.get_input_value("Length")

            out_bone.head = pos
            out_bone.tail = pos + (direction)
            out_bone.length = length

        elif self.current_behaviour == 'DIRECTION':
            pos = self.get_input_value("Position")

            direction = self.get_input_value("Direction")
            direction.normalize()

            length = self.get_input_value("Length")

            out_bone.head = pos
            out_bone.tail = pos + (direction * length)

        elif self.current_behaviour == 'MATRIX':
            matrix = self.get_input_value("Matrix")
            out_bone.tail = mathutils.Vector((0, 1, 0))  # if we don't set the length before the matrix, it stays with length 0???
            out_bone.matrix = matrix.copy()
            # context.view_layer.update()

        use_connect = self.get_input_value('Use Connect', default=False)
        out_bone.use_connect = use_connect

        use_deform = self.get_input_value('Use Deform', default=False)
        out_bone.use_deform = use_deform

        layer = self.get_input_value('Layer')
        if layer is not None:
            out_bone.layers = [x == layer for x in range(0, 32)]

        out_bone.parent = parent_bone
        out_name = out_bone.name

        changed_to_pose_mode = False

        if out_bone.length <= 0:
            raise ValueError(f'The bone "{out_bone.name}" has 0 length, use_connect, matrix, head, tail and length attributes can cause this value to be 0')

        custom_shape = self.get_input_value('Shape')
        if custom_shape:
            self.focus_on_object(armature, {'POSE'})
            changed_to_pose_mode = True
            out_pose_bone = armature.pose.bones[out_name]
            out_pose_bone.custom_shape = custom_shape

            shape_scale = self.get_input_value('Shape Scale')
            if shape_scale is not None:
                out_pose_bone.custom_shape_scale = self.get_input_value('Shape Scale')
                out_pose_bone.use_custom_shape_bone_size = True

            follow_bone = self.get_input_value('Shape Follow')
            if follow_bone:
                out_pose_bone.custom_shape_transform = armature.pose.bones[follow_bone]

        rotation_mode = self.get_input_value('Rotation Mode', default='QUATERNION')
        if rotation_mode != 'QUATERNION':
            if not changed_to_pose_mode:
                self.focus_on_object(armature, {'POSE'})
                changed_to_pose_mode = True

            out_pose_bone = armature.pose.bones[out_name]
            out_pose_bone.rotation_mode = rotation_mode

        self.set_output_value('Bone', out_name)
        self.set_output_value('Armature', armature)

        armature.data.BBN_info.add_bone_tracking(out_name, self, path)
