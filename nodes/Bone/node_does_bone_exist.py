import bpy
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_does_bone_exist(Node, BBN_node):
    bl_idname = 'bbn_does_bone_exist_node'
    bl_label = "Does bone exist"
    bl_icon = 'BONE_DATA'

    input_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
        'Bone': {'type': 'BBN_bone_socket'},
    }

    output_sockets = {
        'Exists': {'type': 'BBN_bool_socket'},
    }

    input_to_focus = 'Armature'
    focus_mode = {'OBJECT', 'POSE'}

    def process(self, context, id, path):
        armature = self.get_input_value("Armature", required=True)
        bone_name = self.get_input_value("Bone")

        self.set_output_value('Exists', bone_name in armature.data.bones)
