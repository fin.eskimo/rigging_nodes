import bpy
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_set_bone_layer(Node, BBN_node):
    bl_idname = 'bbn_set_bone_layer_node'
    bl_label = "Set bone layer"
    bl_icon = 'BONE_DATA'

    input_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
        'Bone': {'type': 'BBN_bone_socket'},
        'Layer': {'type': 'BBN_int_socket'},
    }

    output_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
        'Bone': {'type': 'BBN_bone_socket'},
    }

    behaviour_enum = [
        ('SINGLE', 'Single', 'Only affect one bone', '', 0),
        ('MULTIPLE', 'Multiple', 'Change property of multiple bones', '', 2),
    ]

    behaviour_sockets = {
        'SINGLE': {
            'INPUTS': {
                'Bone': {'type': 'BBN_bone_socket'},
            },
            'OUTPUTS': {
                'Bone': {'type': 'BBN_bone_socket'},
            },
        },
        'MULTIPLE': {
            'INPUTS': {
                'Bone': {'type': 'BBN_string_array_v2_socket'},
            },
            'OUTPUTS': {
                'Bone': {'type': 'BBN_string_array_v2_socket'},
            },
        },
    }

    input_to_focus = 'Armature'
    focus_mode = {'OBJECT', 'POSE'}

    def process(self, context, id, path):
        armature = self.get_input_value("Armature", required=True)
        bone_names = self.get_input_value('Bone')

        index = self.get_input_value('Layer')
        visible_layers = [x == index for x in range(0, 32)]

        if type(bone_names) is not list:
            bone_names = [bone_names]

        for bone_name in bone_names:
            bone = armature.data.bones[bone_name]

            bone.layers = visible_layers

        armature.data.BBN_info.add_bone_tracking(bone_name, self, path)

        if self.current_behaviour == 'SINGLE':
            self.outputs['Bone'].set_value(bone_names[0])
        elif self.current_behaviour == 'MULTIPLE':
            self.outputs['Bone'].set_value(bone_names)
        self.outputs['Armature'].set_value(armature)
