import bpy
from bpy.types import Node

from ..node_get_property import BBN_node_get_property

from mathutils import Vector

import logging

log = logging.getLogger(__name__)

last_output_items = []


class BBN_node_get_bone_property(Node, BBN_node_get_property):
    bl_idname = 'bbn_get_bone_property_node'
    bl_label = "Get Bone Property"
    bl_icon = 'BONE_DATA'

    node_version = 1

    input_sockets = {
        'Armature': {'type': 'BBN_object_ref_socket'},

    }

    behaviour_sockets = {
        'SINGLE': {
            'INPUTS': {
                'Bone': {'type': 'BBN_bone_socket'},
            }
        },
        'MULTIPLE': {
            'INPUTS': {
                'Bone': {'type': 'BBN_string_array_v2_socket'},
            }
        },
    }

    def updated_enum(self, context):
        self.update_behaviour_sockets()

    bone_mode: bpy.props.EnumProperty(
        items=[
            ('EDIT', 'Edit', 'Edit Bone Properties'),
            ('POSE', 'Pose', 'Pose Bone Properties'),
            ('BONE', 'Bone', 'Bone Properties')
        ], update=updated_enum)

    output_type: bpy.props.EnumProperty(
        items=[
            ('VALUE', 'Value', 'Value', 'NONE', 0),
            ('NAME', 'Name', 'Name', 'NONE', 1),
            ('DATAPATH', 'Datapath', 'Datapath', 'NONE', 2)
        ], update=updated_enum)

    input_to_focus = 'Armature'

    @property
    def as_string(self):
        return self.output_type != 'VALUE'

    @ property
    def props_class(self):
        if self.bone_mode == 'EDIT':
            return bpy.types.EditBone
        elif self.bone_mode == 'POSE':
            return bpy.types.PoseBone
        elif self.bone_mode == 'BONE':
            return bpy.types.Bone

    def upgrade_node(self):
        super().upgrade_node()

        if self.current_node_version == 0:
            outputs = self.outputs[:]
            for x in outputs:
                if x.hide:
                    self.outputs.remove(x)

        if 'execute_mode' in self.keys():
            self['current_behaviour'] = self['execute_mode']
            del self['execute_mode']

        if 'as_datapath' in self.keys():
            if self['as_datapath']:
                self['output_type'] = 2
            del self['as_datapath']

        self.current_node_version = self.node_version

    @property
    def focus_mode(self):
        if self.bone_mode == 'EDIT':
            return {'EDIT'}
        elif self.bone_mode == 'POSE':
            return {'POSE'}
        elif self.bone_mode == 'BONE':
            return {'OBJECT'}

    def draw_buttons(self, context, layout):
        layout, row1, row2 = self.setup_buttons(context, layout)
        row1.prop(self, 'bone_mode', text='')
        row1.prop(self, 'output_type', text='')

    def process(self, context, id, path):
        armature = self.get_input_value("Armature", required=True)
        bone_name = self.get_input_value("Bone")

        if self.current_behaviour == 'SINGLE':
            bone = None
            if self.bone_mode == 'POSE':
                bone = armature.pose.bones[bone_name]
            elif self.bone_mode == 'EDIT':
                bone = armature.data.edit_bones[bone_name]
            elif self.bone_mode == 'BONE':
                bone = armature.data.bones[bone_name]

            if not bone:
                raise ValueError('Bone not found')

            if self.output_type == 'VALUE':
                self.set_outputs(armature, bone)
            elif self.output_type == 'NAME':
                for x in self.outputs:
                    x.set_value(x.name)
            elif self.output_type == 'DATAPATH':
                for x in self.outputs:
                    x.set_value(f'{bone.path_from_id()}.{x.name}')
        elif self.current_behaviour == 'MULTIPLE':
            bone_names = bone_name
            bones = []
            if self.bone_mode == 'POSE':
                bones = [armature.pose.bones[x] for x in bone_names]
            elif self.bone_mode == 'EDIT':
                bones = [armature.data.edit_bones[x] for x in bone_names]
            elif self.bone_mode == 'BONE':
                bones = [armature.data.bones[x] for x in bone_names]

            if self.output_type == 'VALUE':
                self.set_outputs([armature] * len(bones), bones)
            elif self.output_type == 'DATAPATH':
                for output in self.outputs:
                    output.set_value([f'{x.path_from_id()}.{output.name}' for x in bones])
            elif self.output_type == 'NAME':
                for output in self.outputs:
                    output.set_value([output.name for x in bones])
