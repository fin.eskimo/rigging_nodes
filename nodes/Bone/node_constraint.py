import bpy
from bpy.types import Node

from ..node_set_property import BBN_node_set_property

import logging

log = logging.getLogger(__name__)

all_constraints = {
    'ACTION': bpy.types.ActionConstraint,
    'ARMATURE': bpy.types.ArmatureConstraint,
    'CAMERA_SOLVER': bpy.types.CameraSolverConstraint,
    'CHILD_OF': bpy.types.ChildOfConstraint,
    'CLAMP_TO': bpy.types.ClampToConstraint,
    'COPY_LOCATION': bpy.types.CopyLocationConstraint,
    'COPY_ROTATION': bpy.types.CopyRotationConstraint,
    'COPY_SCALE': bpy.types.CopyScaleConstraint,
    'COPY_TRANSFORMS': bpy.types.CopyTransformsConstraint,
    'DAMPED_TRACK': bpy.types.DampedTrackConstraint,
    'FLOOR': bpy.types.FloorConstraint,
    'FOLLOW_PATH': bpy.types.FollowPathConstraint,
    'FOLLOW_TRACK': bpy.types.FollowTrackConstraint,
    'IK': bpy.types.KinematicConstraint,
    'LIMIT_DISTANCE': bpy.types.LimitDistanceConstraint,
    'LIMIT_LOCATION': bpy.types.LimitLocationConstraint,
    'LIMIT_ROTATION': bpy.types.LimitRotationConstraint,
    'LIMIT_SCALE': bpy.types.LimitScaleConstraint,
    'LOCKED_TRACK': bpy.types.LockedTrackConstraint,
    'MAINTAIN_VOLUME': bpy.types.MaintainVolumeConstraint,
    'OBJECT_SOLVER': bpy.types.ObjectSolverConstraint,
    'PIVOT': bpy.types.PivotConstraint,
    # '': bpy.types.PythonConstraint,
    'SHRINKWRAP': bpy.types.ShrinkwrapConstraint,
    'SPLINE_IK': bpy.types.SplineIKConstraint,
    'STRETCH_TO': bpy.types.StretchToConstraint,
    'TRACK_TO': bpy.types.TrackToConstraint,
    'TRANSFORM_CACHE': bpy.types.TransformCacheConstraint,
    'TRANSFORM': bpy.types.TransformConstraint,

}

constraint_types = []


class BBN_node_add_constraint(Node, BBN_node_set_property):
    node_version = 4

    bl_idname = 'bbn_add_constraint_node'
    bl_label = "Add Constraint"
    bl_icon = 'CONSTRAINT_BONE'

    def updated_constraint(self, context):
        self.update_behaviour_sockets()

    def get_constraint_types(self, context):
        global constraint_types
        if self.current_behaviour != 'MULTIPLE_SUBTARGETS':
            constraint_types = [(id, x.bl_rna.name, x.bl_rna.description) for id, x in all_constraints.items()]
            return constraint_types
        constraint_types = [(id, x.bl_rna.name, x.bl_rna.description) for id, x in all_constraints.items() if 'subtarget' in x.bl_rna.properties]
        return constraint_types

    constraint_type: bpy.props.EnumProperty(items=get_constraint_types, update=updated_constraint)

    input_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
        'Bone': {'type': 'BBN_bone_socket'},
    }

    output_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
    }

    behaviour_enum = [
        ('SINGLE', 'Single', 'Add a constraint to one bone', '', 0),
        ('MULTIPLE', 'Multiple', 'Add a constraint to multiple bones', '', 1),
        ('MULTIPLE_SUBTARGETS', 'Multiple Subtargets', 'Add constraint to multiple bones with different subtargets', '', 2),
    ]

    behaviour_opt_sockets = {
        'SINGLE': {
            'OUTPUTS': {
                'Bone': {'type': 'BBN_bone_socket'},
                'Datapath': {'type': 'BBN_string_socket'},
                'Pointer': {'type': 'BBN_pointer_socket'},
            },
        },
        'MULTIPLE': {
            'OUTPUTS': {
                'Bone': {'type': 'BBN_string_array_v2_socket'},
                'Datapath': {'type': 'BBN_string_array_v2_socket'},
                'Pointer': {'type': 'BBN_pointer_array_socket'},
            },
        },
        'MULTIPLE_SUBTARGETS': {
            'OUTPUTS': {
                'Bone': {'type': 'BBN_string_array_v2_socket'},
                'Datapath': {'type': 'BBN_string_array_v2_socket'},
                'Pointer': {'type': 'BBN_pointer_array_socket'},
            },
        },
    }

    behaviour_sockets = {
        'SINGLE': {
            'INPUTS': {
                'Bone': {'type': 'BBN_bone_socket'},
            },
        },
        'MULTIPLE': {
            'INPUTS': {
                'Bone': {'type': 'BBN_string_array_v2_socket'},
            },
        },
        'MULTIPLE_SUBTARGETS': {
            'INPUTS': {
                'Bone': {'type': 'BBN_string_array_v2_socket'},
                'target': {'type': 'BBN_object_ref_socket'},
                'subtarget': {'type': 'BBN_string_array_v2_socket'},
            },
        },
    }

    input_to_focus = 'Armature'
    focus_mode = {'POSE', 'OBJECT'}

    def upgrade_node(self):
        super().upgrade_node()

        if self.current_node_version == 0:
            inputs = self.inputs[:]
            for x in inputs:
                if x.hide:
                    self.inputs.remove(x)

        # remap old properties
        if 'execute_mode' in self.keys():
            log.info(self['execute_mode'])
            self['current_behaviour'] = self['execute_mode']
            del self['execute_mode']

        self.current_node_version = self.node_version

    @ property
    def props_class(self):
        return all_constraints[self.constraint_type]

    def draw_label(self):
        try:
            return next(x[1] for x in self.get_constraint_types(None) if x[0] == self.constraint_type)
        except StopIteration:
            return self.bl_label

    def draw_buttons(self, context, layout):
        col = layout.column(align=True)
        layout, row1, row2 = self.setup_buttons(context, col)
        row1.prop(self, 'constraint_type', text='')

    def process(self, context, id, path):
        armature = self.get_input_value('Armature', throw=True, required=True)
        bone_names = self.get_input_value('Bone', throw=True, required=True)

        datapaths = []
        constraints = []

        if self.current_behaviour == 'SINGLE':
            bone_names = [bone_names]

        for i, bone_name in enumerate(bone_names):
            bone = armature.pose.bones.get(bone_name)

            if not bone:
                raise ValueError(f'Bone {bone_name} not found')

            ct = bone.constraints.new(self.constraint_type)
            constraints.append(ct)

            if self.current_behaviour == 'MULTIPLE_SUBTARGETS':
                target_armature = self.get_input_value('target', throw=True, required=True)
                target_bones = self.get_input_value('subtarget', throw=True, required=True)

                ct.target = target_armature
                ct.subtarget = target_bones[i]

                for x in self.inputs[4:]:
                    self.set_prop(armature, x, ct, x.name)
            else:
                for x in self.inputs[2:]:
                    self.set_prop(armature, x, ct, x.name)

            armature.data.BBN_info.add_bone_tracking(bone_name, self, path)
            armature.data.BBN_info.add_constraint_tracking(bone_name, ct.name, self, path)

            datapath = ct.path_from_id()
            datapaths.append(datapath)

        if self.current_behaviour == 'SINGLE':
            self.set_output_value('Armature', armature)
            self.set_output_value('Bone', bone_names[0])
            self.set_output_value('Datapath', datapaths[0])
            if self.has_output('Pointer'):
                self.set_output_value('Pointer', (armature, datapaths[0], constraints[0].bl_rna))
        else:
            self.set_output_value('Armature', armature)
            self.set_output_value('Bone', bone_names)
            self.set_output_value('Datapath', datapaths)
            if self.has_output('Pointer'):
                self.set_output_value('Pointer', list(zip([armature] * len(datapaths), datapaths, [x.bl_rna for x in constraints])))
