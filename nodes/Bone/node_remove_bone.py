import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_remove_bone(Node, BBN_node):
    bl_idname = 'bbn_remove_bone_node'
    bl_label = "Remove Bone"
    bl_icon = 'BONE_DATA'

    input_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
    }

    output_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
    }

    behaviour_enum = [
        ('SINGLE', 'Single', 'Only affect one bone', '', 0),
        ('MULTIPLE', 'Multiple', 'Change property of multiple bones', '', 1),
    ]

    behaviour_sockets = {
        'SINGLE': {
            'INPUTS': {
                'Name': {'type': 'BBN_bone_socket'},
            },
        },
        'MULTIPLE': {
            'INPUTS': {
                'Name': {'type': 'BBN_string_array_v2_socket'},
            },
        },
    }

    input_to_focus = 'Armature'
    focus_mode = {'EDIT'}

    def draw_label(self):
        name_socket = self.inputs.get('Name')
        if self.current_behaviour == 'SINGLE' and name_socket:
            return 'Remove Bone: ' + name_socket.get_value()
        return 'Remove Bone'

    def process(self, context, id, path):
        armature = self.get_input_value("Armature", required=True)
        bone_names = self.get_input_value("Name")

        if self.current_behaviour == 'SINGLE':
            bone = armature.data.edit_bones.get(bone_names)
            if not bone:
                raise ValueError(f'Bone "{bone_names}" not found')
            armature.data.edit_bones.remove(bone)
        elif self.current_behaviour == 'MULTIPLE':
            for bone_name in bone_names:
                bone = armature.data.edit_bones.get(bone_name)
                if not bone:
                    raise ValueError(f'Bone "{bone_name}" not found')
                armature.data.edit_bones.remove(bone)

        self.outputs['Armature'].set_value(armature)
