import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_flip_bone(Node, BBN_node):
    bl_idname = 'bbn_node_flip_bone'
    bl_label = "Flip Bone"
    bl_icon = 'BONE_DATA'

    bone_default: bpy.props.StringProperty(default='Bone')

    input_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
    }

    output_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
    }

    behaviour_enum = [
        ('SINGLE', 'Single', 'Aligns the z axis of the bone to the given vector', '', 0),
        ('MULTIPLE', 'Multiple', 'Aligns the z axis of the bone to the given vector', '', 1),
    ]

    behaviour_sockets = {
        'SINGLE': {
            'INPUTS': {
                'Bone': {'type': 'BBN_bone_socket'},
                'Keep Position': {'type': 'BBN_bool_socket', 'default_value': False},
            },
            'OUTPUTS': {
                'Bone': {'type': 'BBN_string_socket'},
            }
        },
        'MULTIPLE': {
            'INPUTS': {
                'Bone': {'type': 'BBN_string_array_v2_socket'},
                'Keep Position': {'type': 'BBN_bool_socket', 'default_value': False},
            },
            'OUTPUTS': {
                'Bone': {'type': 'BBN_string_array_v2_socket'},
            }
        },
    }

    input_to_focus = 'Armature'
    focus_mode = {'EDIT'}

    def upgrade_node(self):
        super().upgrade_node()

        # remap old properties
        if 'execute_mode' in self.keys():
            self['current_behaviour'] = self['execute_mode']
            del self['execute_mode']

        self.current_node_version = self.node_version

    def process(self, context, id, path):
        armature = self.get_input_value("Armature", required=True)
        bone_names = self.get_input_value('Bone')
        keep_position = self.get_input_value('Keep Position', default=False)

        if self.current_behaviour == 'SINGLE':
            bone_names = [bone_names]

        for bone_name in bone_names:
            bone = armature.data.edit_bones[bone_name]
            orig_z = bone.z_axis.copy()
            bone.head, bone.tail = bone.tail.copy(), bone.head.copy()
            bone.align_roll(orig_z)
            if keep_position:
                loc, rot, scale = bone.matrix.decompose()
                bone.matrix = mathutils.Matrix.Translation(bone.tail) @ rot.to_matrix().to_4x4() @ mathutils.Matrix.Scale(1, 4, scale)
            armature.data.BBN_info.add_bone_tracking(bone_name, self, path)

        if self.current_behaviour == 'SINGLE':
            self.set_output_value('Bone', bone_names[0])
        else:
            self.set_output_value('Bone', bone_names)
        self.set_output_value('Armature', armature)
