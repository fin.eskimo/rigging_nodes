import bpy
import mathutils
import math
from bpy.types import Node

from ..node_base import BBN_node
from ...node_tree import BBN_tree


class BBN_node_set_bone_rotation(Node, BBN_node):
    bl_idname = 'bbn_node_set_bone_rotation'
    bl_label = "Rotate Bone"
    bl_icon = 'BONE_DATA'

    node_version = 2

    input_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
        'Bone': {'type': 'BBN_bone_socket'},
        'Order': {'type': 'BBN_enum_socket', 'items': ['XYZ', 'XZY', 'YXZ', 'YZX', 'ZXY', 'ZYX']}
    }

    output_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
    }

    behaviour_enum = [
        ('SINGLE', 'Single', 'Single'),
        ('MULTIPLE', 'Multiple', 'Multiple'),
    ]

    behaviour_sockets = {
        'SINGLE': {
            'INPUTS': {
                'Bone': {'type': 'BBN_bone_socket'},
            },
            'OUTPUTS': {
                'Bone': {'type': 'BBN_bone_socket'},
            },
        },
        'MULTIPLE': {
            'INPUTS': {
                'Bone': {'type': 'BBN_string_array_v2_socket'},
            },
            'OUTPUTS': {
                'Bone': {'type': 'BBN_string_array_v2_socket'},
            },
        },
    }

    rotation_type: bpy.props.EnumProperty(items=[
        ('REPLACE', 'Replace', 'Replace Rotation', '', 0),
        ('ADD', 'Add', 'Add Rotation', '', 1),
    ], update=BBN_tree.value_updated
    )

    space_type: bpy.props.EnumProperty(items=[
        ('WORLD', 'World', 'World', '', 0),
        ('PARENT', 'Parent', 'Parent', '', 1),
        ('LOCAL', 'Local', 'Local', '', 2),
    ], update=BBN_tree.value_updated
    )

    _opt_input_sockets = {
        'X': {'type': 'BBN_float_socket'},
        'Y': {'type': 'BBN_float_socket'},
        'Z': {'type': 'BBN_float_socket'},
    }

    input_to_focus = 'Armature'
    focus_mode = {'EDIT'}

    def upgrade_node(self):
        super().upgrade_node()

        if self.current_node_version < 2:
            if 'current_behaviour' in self.keys():
                self['rotation_type'] = self['current_behaviour']
                del self['current_behaviour']

        self.current_node_version = self.node_version

    def draw_buttons(self, context, layout):
        layout, row1, row2 = self.setup_buttons(context, layout)

        row1.prop(self, 'rotation_type', text='')
        row1.prop(self, 'space_type', text='')

    def process(self, context, id, path):
        armature = self.get_input_value("Armature", required=True)
        bone_names = self.get_input_value('Bone', force_list=True)
        order = self.get_input_value('Order')

        for bone_name in bone_names:
            bone = armature.data.edit_bones[bone_name]

            if self.space_type == 'WORLD' or not bone.parent:
                bone_matrix = bone.matrix.copy()
            elif self.space_type == 'PARENT':
                bone_matrix = bone.parent.matrix.inverted() @ bone.matrix.copy()
            elif self.space_type == 'LOCAL':
                bone_matrix = mathutils.Matrix.Identity(4)

            bone_euler = bone_matrix.to_euler(order)

            x = self.get_input_value('X')
            if x is not None:
                if self.rotation_type == 'REPLACE':
                    bone_euler.x = math.radians(x)
                else:
                    bone_euler.x += math.radians(x)
            y = self.get_input_value('Y')
            if y is not None:
                if self.rotation_type == 'REPLACE':
                    bone_euler.y = math.radians(y)
                else:
                    bone_euler.y += math.radians(y)
            z = self.get_input_value('Z')
            if z is not None:
                if self.rotation_type == 'REPLACE':
                    bone_euler.z = math.radians(z)
                else:
                    bone_euler.z += math.radians(z)

            mat_rot = bone_euler.to_matrix().to_4x4()
            mat_loc = mathutils.Matrix.Translation(bone_matrix.to_translation()).to_4x4()
            mat_sca = mathutils.Matrix.Scale(1, 4, bone_matrix.to_scale())

            if self.space_type == 'WORLD' or not bone.parent:
                main_matrix = mat_loc @ mat_rot @ mat_sca
            elif self.space_type == 'PARENT':
                main_matrix = bone.parent.matrix.copy() @ mat_loc @ mat_rot @ mat_sca
            elif self.space_type == 'LOCAL':
                main_matrix = bone.matrix.copy() @ mat_loc @ mat_rot @ mat_sca

            bones = [bone]
            local_matrix_dict = {}

            while bones:
                bones.extend(bones[0].children)
                local_matrix_dict[bones[0]] = bone.matrix.inverted() @ bones[0].matrix.copy()  # get parent space matrix relative to the main bone
                bones.pop(0)

            for bone, local_matrix in local_matrix_dict.items():
                bone.matrix = main_matrix @ local_matrix
                armature.data.BBN_info.add_bone_tracking(bone.name, self, path)

        self.outputs['Armature'].set_value(armature)
        if self.current_behaviour == 'SINGLE':
            self.set_output_value('Bone', bone_names[0])
        else:
            self.set_output_value('Bone', bone_names)
