import bpy
from bpy.types import Node

from ..node_base import BBN_node
from collections import OrderedDict


class BBN_node_add_property(Node, BBN_node):
    bl_idname = 'bbn_add_property'
    bl_label = "Add Custom Prop"
    bl_icon = 'PROPERTIES'

    input_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
        'Bone': {'type': 'BBN_bone_socket'},
        'Name': {'type': 'BBN_string_socket'},

    }

    output_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
    }

    _opt_output_sockets = {
        'Bone': {'type': 'BBN_string_socket'},
        'Datapath': {'type': 'BBN_string_socket'},
        'As Driver': {'type': 'BBN_driver_socket'},
        'As Driver Var': {'type': 'BBN_drivervar_socket'},
    }

    behaviour_enum = {
        ('FLOAT', 'Float', 'Float', 'NONE', 0),
        ('INTEGER', 'Interger', 'Interger', 'NONE', 1),
        ('STRING', 'String', 'String', 'NONE', 2),
    }

    behaviour_sockets = {
        'FLOAT': {
            'INPUTS': OrderedDict(
                Value={'type': 'BBN_float_socket'},
                Min={'type': 'BBN_float_socket'},
                Max={'type': 'BBN_float_socket', 'default_value': 1.0},
            )
        },
        'INTEGER': {
            'INPUTS': OrderedDict(
                Value={'type': 'BBN_int_socket'},
                Min={'type': 'BBN_int_socket'},
                Max={'type': 'BBN_int_socket', 'default_value': 1},
            )
        },
        'STRING': {
            'INPUTS': OrderedDict(
                Value={'type': 'BBN_string_socket'},
            )
        }
    }

    input_to_focus = 'Armature'
    focus_mode = {'OBJECT', 'POSE'}

    def init(self, context):
        super().init(context)

    def draw_label(self):
        return ('Add Property - ' + self.inputs["Name"].get_value())

    def process(self, context, id, path):
        armature = self.get_input_value("Armature", required=True)

        bone_name = self.get_input_value("Bone")
        prop_name = self.get_input_value("Name")
        if not prop_name:
            raise ValueError('Property name is not valid')
        prop_value = self.get_input_value("Value")

        bone = armature.pose.bones.get(bone_name)

        bone[prop_name] = prop_value
        if '_RNA_UI' not in bone:
            bone['_RNA_UI'] = {}

        if self.current_behaviour in {'FLOAT', 'INTEGER'}:
            min = self.get_input_value("Min")
            max = self.get_input_value("Max")
            bone['_RNA_UI'][prop_name] = {'min': min, 'soft_min': min, 'max': max, 'soft_max': max, 'description': '', 'default': prop_value}
        else:
            bone['_RNA_UI'][prop_name] = {'description': '', 'default': prop_value}

        bone.property_overridable_library_set(f'["{prop_name}"]', True)
        data_path = f'pose.bones["{bone_name}"]["{prop_name}"]'

        self.set_output_value('Armature', armature)
        self.set_output_value('Bone', bone_name)
        self.set_output_value('Datapath', data_path)

        if self.has_output('As Driver'):
            ans = {
                'driver_type': 'AVERAGE',
                'expression': '',
                'use_self': False,
                'variables': [
                    {
                        'variable_name': prop_name,
                        'variable_type': 'SINGLE_PROP',

                        'target0_object': armature,
                        'target0_bone': bone_name,
                        'target0_datapath': data_path,
                        'target0_transform_type': 'LOC_X',
                        'target0_rotation_mode': 'AUTO',
                        'target0_transform_space': 'WORLD_SPACE',

                        'target1_object': None,
                        'target1_bone': '',
                        'target1_datapath': '',
                        'target1_transform_type': 'LOC_X',
                        'target1_rotation_mode': 'AUTO',
                        'target1_transform_space': 'WORLD_SPACE',
                    }
                ],
            }
            self.set_output_value('As Driver', ans)
        if self.has_output('As Driver Var'):
            ans = {
                'variable_name': prop_name,
                'variable_type': 'SINGLE_PROP',

                'target0_object': armature,
                'target0_bone': bone_name,
                'target0_datapath': data_path,
                'target0_transform_type': 'LOC_X',
                'target0_rotation_mode': 'AUTO',
                'target0_transform_space': 'WORLD_SPACE',

                'target1_object': None,
                'target1_bone': '',
                'target1_datapath': '',
                'target1_transform_type': 'LOC_X',
                'target1_rotation_mode': 'AUTO',
                'target1_transform_space': 'WORLD_SPACE',
            }
            self.set_output_value('As Driver Var', ans)
        armature.data.BBN_info.add_bone_tracking(bone_name, self, path)
