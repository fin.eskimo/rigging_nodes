import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ..String.node_create_bone_name import create_new_name


class BBN_node_duplicate_chain(Node, BBN_node):
    bl_idname = 'bbn_node_sup_bone_chain'
    bl_label = "Duplicate Bones"
    bl_icon = 'BONE_DATA'

    input_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
    }

    output_sockets = {
        'Armature': {'type': 'BBN_object_socket'},
    }

    _opt_input_sockets = {
        "Connect": {'type': 'BBN_bool_socket'},
        "Deform": {'type': 'BBN_bool_socket'},
        "Inherit Scale": {'type': 'BBN_enum_socket', 'items': [x.identifier for x in bpy.types.EditBone.bl_rna.properties['inherit_scale'].enum_items]},
        "New Parent": {'type': 'BBN_bone_socket'},
        "Override All Parents": {'type': 'BBN_bool_socket'},
        "Scale": {'type': 'BBN_float_socket', 'default_value': 1},
        "Offset": {'type': 'BBN_vector_socket'},
        "Layer": {'type': 'BBN_int_socket'},
        "Delete Originals": {'type': 'BBN_bool_socket'},

        'Type': {'type': 'BBN_enum_socket', 'items': [
            ('DEF', 'Deform', 'Deform', 'NONE', 0),
            ('TRGT', 'Target', 'Target', 'NONE', 1),
            ('MCH', 'Mechanic', 'Mechanic', 'NONE', 2),
            ('CTRL', 'Control', 'Control', 'NONE', 3),
            ('NONE', 'Clear', 'Clear', 'NONE', 4),
        ]},
        'Original Bone': {'type': 'BBN_string_socket'},
        'Base Name': {'type': 'BBN_string_socket'},
        'Prefix': {'type': 'BBN_string_socket'},
        'Suffix': {'type': 'BBN_string_socket'},
        'Edit Prefix': {'type': 'BBN_string_socket'},
        'Edit Suffix': {'type': 'BBN_string_socket'},
        'Side': {'type': 'BBN_enum_socket', 'items': [
            ('MID', 'Mid', 'Mid', 'NONE', 0),
            ('RIGHT', 'Right', 'Right', 'NONE', 1),
            ('LEFT', 'Left', 'Left', 'NONE', 2),
            ('NONE', 'Clear', 'Clear', 'NONE', 3),
        ]},
        'Number': {'type': 'BBN_int_socket'},
    }

    behaviour_enum = [
        ('CHAIN', 'Duplicate Chain', 'Duplicates the bone chain, given a start and end bone'),
        ('COPY', 'Duplicate Bones', 'Duplicates the given bones'),
        ('SINGLE', 'One Bone', 'Duplicate the given bone'),
    ]

    behaviour_sockets = {
        'CHAIN': {
            'INPUTS': {
                'Start': {'type': 'BBN_bone_socket'},
                'End': {'type': 'BBN_bone_socket'},
            },
            'OUTPUTS': {
                'Orig Bones': {'type': 'BBN_string_array_v2_socket'},
                'Dup Bones': {'type': 'BBN_string_array_v2_socket'},
            }
        },
        'COPY': {
            'INPUTS': {
                'Bones': {'type': 'BBN_string_array_v2_socket'},
            },
            'OUTPUTS': {
                'Orig Bones': {'type': 'BBN_string_array_v2_socket'},
                'Dup Bones': {'type': 'BBN_string_array_v2_socket'},
            }
        },
        'SINGLE': {
            'INPUTS': {
                'Bone': {'type': 'BBN_bone_socket'},
            },
            'OUTPUTS': {
                'Orig Bone': {'type': 'BBN_string_socket'},
                'Dup Bone': {'type': 'BBN_string_socket'},
            }
        },
    }

    behaviour_opt_sockets = {
        'CHAIN': {
            'INPUTS': {
                "Reverse Direction": {
                    'type': 'BBN_bool_socket'
                },
            },
        },
    }

    input_to_focus = 'Armature'
    focus_mode = {'EDIT'}

    behaviour_labels = {
        'CHAIN': 'Duplicate Chain',
        'COPY': 'Duplicate Bones',
        'SINGLE': 'Duplicate Bone',
    }

    def process(self, context, id, path):
        armature = self.get_input_value('Armature', required=True)

        orig_bone_map = {}
        dup_bone_map = {}

        # get name related inputs
        input_new_name = self.get_input_value('New Name', default=None)
        input_type = self.get_input_value('Type', default=None)
        input_number = self.get_input_value('Number', default=None)
        input_side = self.get_input_value('Side', default=None)
        input_base_name = self.get_input_value('Base Name', default=None)
        input_prefix = self.get_input_value('Prefix', default=None)
        input_suffix = self.get_input_value('Suffix', default=None)
        input_edit_prefix = self.get_input_value('Edit Prefix', default=None)
        input_edit_suffix = self.get_input_value('Edit Suffix', default=None)

        # get edit bone inputs
        new_parent = self.get_input_value('New Parent', default=None)
        override_all_parents = self.get_input_value('Override All Parents', default=False)
        connect = self.get_input_value('Connect', default=None)
        scale = self.get_input_value('Scale', default=None)
        inherit_scale = self.get_input_value('Inherit Scale', default=None)
        offset = self.get_input_value('Offset', default=mathutils.Vector((0, 0, 0)))
        deform = self.get_input_value('Deform', default=None)
        delete_originals = self.get_input_value('Delete Originals', default=None)
        reverse_direction = self.get_input_value('Reverse Direction', default=None)
        layer = self.get_input_value('Layer', default=None)

        bones = []

        if self.current_behaviour == 'CHAIN':
            end_name = self.get_input_value("End")
            try:
                end_bone = armature.data.edit_bones[end_name]
            except KeyError:
                raise ValueError(f'{end_name} bone does not exist')

            start_name = self.get_input_value("Start")
            try:
                start_bone = armature.data.edit_bones[start_name]
            except KeyError:
                raise ValueError(f'{start_name} bone does not exist')

            list_correct = False

            current_bone = end_bone

            while current_bone:
                bones.append(current_bone.name)
                if current_bone == start_bone:
                    current_bone = None
                    list_correct = True
                else:
                    current_bone = current_bone.parent

            if not reverse_direction:
                bones.reverse()

            if not list_correct:
                raise ValueError('The bones do not form a chain')

        elif self.current_behaviour == 'COPY':
            bones = self.get_input_value('Bones')

        elif self.current_behaviour == 'SINGLE':
            bones = [self.get_input_value('Bone')]

        def setup_bone(new_bone_name):
            new_bone = armature.data.edit_bones[new_bone_name]
            orig_bone = armature.data.edit_bones[dup_bone_map[new_bone_name]]

            new_dup_parent = armature.data.edit_bones[new_parent] if new_parent else None
            if reverse_direction and orig_bone.children.get(0):
                new_bone_parent_name = orig_bone_map.get(orig_bone.children.get(0).name, None)
                if new_bone_parent_name:
                    new_dup_parent = armature.data.edit_bones[new_bone_parent_name]
            elif orig_bone.parent:
                new_bone_parent_name = orig_bone_map.get(orig_bone.parent.name, None)
                if new_bone_parent_name:
                    new_dup_parent = armature.data.edit_bones[new_bone_parent_name]

            if override_all_parents:
                new_dup_parent = new_parent

            if new_parent is None and not new_dup_parent:
                new_dup_parent = orig_bone.parent

            new_bone.use_connect = connect if connect is not None else orig_bone.use_connect

            new_bone.parent = new_dup_parent

            new_bone.use_deform = deform if deform is not None else orig_bone.use_deform
            if scale is not None:
                new_bone.length = new_bone.length * scale

            new_bone.inherit_scale = inherit_scale if inherit_scale is not None else orig_bone.inherit_scale

            new_bone.layers = [x == layer for x in range(0, 32)] if layer is not None else orig_bone.layers

        def dup_bone(bone_name, name):
            edit_bone = armature.data.edit_bones[bone_name]

            new_bone = armature.data.edit_bones.new(name)
            new_bone.matrix = edit_bone.matrix

            if reverse_direction:
                new_bone.head = edit_bone.tail + mathutils.Vector(offset)
                new_bone.tail = edit_bone.head + mathutils.Vector(offset)
            else:
                new_bone.head = edit_bone.head + mathutils.Vector(offset)
                new_bone.tail = edit_bone.tail + mathutils.Vector(offset)

            orig_bone_map[edit_bone.name] = new_bone.name
            dup_bone_map[new_bone.name] = edit_bone.name

            armature.data.BBN_info.add_bone_tracking(new_bone.name, self, path)

            return new_bone

        for bone in bones:
            new_name = input_new_name if input_new_name else create_new_name(
                bone,
                input_type,
                input_number,
                input_side,
                input_base_name,
                input_prefix,
                input_suffix,
                input_edit_suffix,
                input_edit_prefix,
            )

            dup_bone(bone, new_name)

        for bone in dup_bone_map.keys():
            setup_bone(bone)

        if delete_originals:
            for bone in bones:
                armature.data.edit_bones.remove(armature.data.edit_bones.get(bone))
            if self.current_behaviour != 'SINGLE':
                self.outputs['Orig Bones'].set_value([])
            else:
                self.outputs['Orig Bone'].set_value('')
        else:
            if self.current_behaviour != 'SINGLE':
                self.outputs['Orig Bones'].set_value(bones)
            else:
                self.outputs['Orig Bone'].set_value(bones[0])

        if self.current_behaviour != 'SINGLE':
            self.outputs['Dup Bones'].set_value(list(dup_bone_map.keys()))
        else:
            self.outputs['Dup Bone'].set_value(list(dup_bone_map.keys())[0])

        self.outputs['Armature'].set_value(armature)
