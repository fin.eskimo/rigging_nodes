from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node, Operator

from ..node_base import BBN_node
from ...runtime import runtime_info

last_outputs = []

possible_sockets = {
    'String': 'BBN_string_socket',
    'Float': 'BBN_float_socket',
    'Int': 'BBN_int_socket',
    'Object': 'BBN_object_socket',
    'Vector': 'BBN_vector_socket',
    'Bool': 'BBN_bool_socket',
    'StringArray': 'BBN_string_array_v2_socket',
}


class BBN_node_custom_script(Node, BBN_node):
    deprecated = False
    bl_idname = 'bbn_custom_script'
    bl_label = "Custom Script"
    bl_icon = 'SCRIPTPLUGINS'

    text: bpy.props.PointerProperty(type=bpy.types.Text)

    def get_unique_name(self, sockets, basename):
        i = 1
        names = [x.name for x in sockets]
        name = f'{basename}_{i}'
        while name in names:
            i = i + 1
            name = f'{basename}_{i}'
        return name

    @ property
    def opt_output_sockets(self):
        ans = {self.get_unique_name(self.outputs, name): {'type': sock_type, 'editable_name': True} for name, sock_type in possible_sockets.items()}
        return ans

    @ property
    def opt_input_sockets(self):
        ans = {self.get_unique_name(self.inputs, name): {'type': sock_type, 'editable_name': True} for name, sock_type in possible_sockets.items()}
        return ans

    def draw_buttons(self, context, layout):
        layout, row1, row2 = self.setup_buttons(context, layout)
        row1.prop(self, "text", text='')

    def execute_code(self):
        code_globals = {x.name: x.get_real_value() for x in self.inputs[:]}
        code_locals = dict()
        code = compile(self.text.as_string(), self.text.name, 'exec')
        exec(code, code_globals, code_locals)

        # print(code_globals.keys())
        # print(code_locals.keys())

        return code_globals, code_locals

    def process(self, context, id, path):
        if not self.text:
            raise ValueError('No text object assigned')

        code_globals, code_locals = self.execute_code()

        for x in self.outputs:
            if x.name in code_locals:
                x.set_value(code_locals[x.name])
