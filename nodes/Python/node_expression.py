import bpy
import mathutils
from bpy.types import Node, Operator

from ..node_base import BBN_node

possible_sockets = {
    'String': 'BBN_string_socket',
    'Float': 'BBN_float_socket',
    'Int': 'BBN_int_socket',
    'Object': 'BBN_object_socket',
    'Vector': 'BBN_vector_socket',
    'Matrix': 'BBN_matrix_socket',
    'Bool': 'BBN_bool_socket',
    'StringArray': 'BBN_string_array_v2_socket',
}


class BBN_node_expression(Node, BBN_node):
    deprecated = False
    bl_idname = 'bbn_expression'
    bl_label = "Expression"
    bl_icon = 'SCRIPTPLUGINS'

    description = '''Available modules: 
bpy
mathutils'''

    input_sockets = {
        'Expression': {'type': 'BBN_string_socket'},
    }

    def get_unique_name(self, sockets, basename):
        i = 1
        names = [x.name for x in sockets]
        name = f'{basename}_{i}'
        while name in names:
            i = i + 1
            name = f'{basename}_{i}'
        return name

    @ property
    def opt_output_sockets(self):
        if not self.outputs:
            return {name: {'type': sock_type, 'editable_name': True} for name, sock_type in possible_sockets.items()}
        return {}

    @ property
    def opt_input_sockets(self):
        ans = {self.get_unique_name(self.inputs, name): {'type': sock_type, 'editable_name': True} for name, sock_type in possible_sockets.items()}
        return ans

    def execute_code(self):
        orig_globals = {'bpy': bpy, 'mathutils': mathutils}
        code_globals = {x.name: x.get_real_value() for x in self.inputs[:]}
        code_globals.update(orig_globals)
        code_locals = dict()
        code = compile(self.get_input_value('Expression'), '__expression_node__', 'eval')

        return eval(code, code_globals, code_locals)

    def process(self, context, id, path):
        if self.outputs:
            self.outputs[0].set_value(self.execute_code())
