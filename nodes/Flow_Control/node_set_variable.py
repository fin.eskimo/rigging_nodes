import bpy
from ..node_base import BBN_node
from ...runtime import runtime_info

from bpy.types import NodeReroute, Node


class BBN_node_set_variable(Node, BBN_node,):
    bl_idname = "bbn_set_variable"
    bl_label = "Store Value"
    bl_icon = 'IMPORT'

    input_sockets = {
        '...': {'type': 'BBN_add_array_socket'},
    }

    socket_type: bpy.props.StringProperty()
    variable_name: bpy.props.StringProperty()

    def draw_buttons(self, context, layout):
        super().draw_buttons(context, layout)

        row = layout.row(align=True)
        row.prop(self, "variable_name", text="")
        row.operator('bbn.select_referencing_nodes', text='', icon='RESTRICT_SELECT_OFF').node = self.name

    def draw_label(self):
        return 'Set ' + self.variable_name

    def process(self, context, id, path):
        pass

    def update(self):
        if runtime_info['updating']:
            return

        main_input = self.inputs[0]
        connected_socket = main_input.connected_socket
        if connected_socket and self.socket_type != connected_socket.bl_rna.name:

            self.socket_type = connected_socket.bl_rna.name

            input_socket = self.inputs.new(self.socket_type, '.')._init()

            self.id_data.relink_socket(main_input, input_socket)

            self.inputs.remove(self.inputs[0])

            # self.outputs[0].socket_type = self.socket_type

        super().update()
