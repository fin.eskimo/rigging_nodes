from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ...node_tree import runtime_info


class BBN_node_execute(Node, BBN_node):
    bl_idname = 'bbn_execute_node'
    bl_label = "Execute"

    input_sockets = {
        '...': {'type': 'BBN_add_array_socket'},
    }

    def execute_dependants(self, context, id, path):
        for i in range(0, len(self.outputs)):
            connected_socket = self.inputs[i + 1].connected_socket
            if connected_socket:
                x = connected_socket.node
                self.execute_other(context, id, path, x)

    def process(self, context, id, path):
        for i in range(0, len(self.outputs)):
            self.outputs[i].set_value(self.get_input_value(i + 1))

    def update(self):
        if runtime_info['updating']:
            return

        main_input = self.inputs[0]
        connected_socket = main_input.connected_socket

        if connected_socket and connected_socket.links:
            link = main_input.links[0]
            in_socket = self.inputs.new(connected_socket.bl_rna.name, connected_socket.bl_label)._init()
            out_socket = self.outputs.new(connected_socket.bl_rna.name, connected_socket.bl_label)._init()

            self.id_data.relink_socket(main_input, in_socket)

        super().update()
