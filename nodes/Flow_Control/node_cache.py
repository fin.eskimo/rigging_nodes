from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..input_base import BBN_nodebase_input
from ...node_tree import BBN_tree
from ...runtime import runtime_info


class BBN_OP_clear_cache_node(bpy.types.Operator):
    bl_idname = "bbn.clear_cache_node"
    bl_label = "Clear Cache"

    node: bpy.props.StringProperty()

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = context.space_data.edit_tree
        node = tree.nodes[self.node]

        node.clear_cache()
        return {'FINISHED'}


class BBN_node_cache(Node, BBN_nodebase_input):
    bl_idname = 'bbn_node_cache'
    bl_label = "Cache"

    input_sockets = {
        'Object': {'type': 'BBN_object_socket'},
    }

    output_sockets = {
        'Object': {'type': 'BBN_object_socket'},
    }

    behaviour_enum = []
    behaviour_sockets = {}
    _opt_input_sockets = {}

    orig_datablock: bpy.props.PointerProperty(type=bpy.types.Object)
    stored_datablock: bpy.props.PointerProperty(type=bpy.types.Object)
    orig_obj_name: bpy.props.StringProperty()
    orig_data_name: bpy.props.StringProperty()

    dependent_classes = [BBN_OP_clear_cache_node]

    def clear_cache(self):
        old = self.stored_datablock
        if old.type == 'ARMATURE':
            bpy.data.armatures.remove(old.data, do_unlink=True)
        elif old.type == 'MESH':
            bpy.data.meshes.remove(old.data, do_unlink=True)
        elif old.type == 'CURVE':
            bpy.data.curves.remove(old.data, do_unlink=True)

    def draw_buttons(self, context, layout):
        self.setup_buttons(context, layout)
        if self.stored_datablock:
            layout.operator('bbn.clear_cache_node').node = self.name

    def execute_dependants(self, context, id, path):
        if not self.stored_datablock:
            super().execute_dependants(context, id, path)

    def copy(self, node):
        self.stored_datablock = None
        self.created_object = None

    def process(self, context, id, path):

        if not self.stored_datablock:
            obj = self.get_input_value(0)
            if not obj:
                raise ValueError('No object connected to imput')

            if (bpy.ops.object.mode_set.poll()):
                bpy.ops.object.mode_set(mode="OBJECT")

            self.orig_obj_name = obj.name
            self.orig_data_name = obj.data.name

            obj_copy = obj.copy()
            obj_data_copy = obj.data.copy()
            obj_copy.data = obj_data_copy

            obj_copy.name = f'{self.orig_obj_name}_cache'
            obj_data_copy.name = f'{self.orig_data_name}_cache'

            self.orig_datablock = obj
            self.stored_datablock = obj_copy

            self.outputs[0].set_value(obj)
        else:
            obj = self.stored_datablock.copy()
            data_block = self.stored_datablock.data.copy()
            obj.data = data_block

            if self.orig_datablock:
                old_obj = self.orig_datablock
                old_obj.user_remap(obj)

            obj.name = f'{self.orig_obj_name}_cache'
            obj.data.name = f'{self.orig_data_name}_cache'

            self.register_object(context, obj, '', 'NOT_SELECTED', self.create_path_to_self(path))

            self.outputs[0].set_value(obj)
