from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ...runtime import runtime_info


class BBN_OP_add_enum_node(bpy.types.Operator):
    bl_idname = "bbn.add_enum_node"
    bl_label = "Add Enum"
    bl_icon = 'ADD'

    node: bpy.props.StringProperty()

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = context.space_data.edit_tree

        node = tree.nodes.get(self.node)

        a = node.inputs[0].enum_items.add()
        a.value = 'Test'
        return {'FINISHED'}


class BBN_OP_edit_enum_node(bpy.types.Operator):
    bl_idname = "bbn.edit_enum_node"
    bl_label = "Edit Enum"
    bl_icon = 'ADD'

    node: bpy.props.StringProperty()

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = context.space_data.edit_tree

        node = tree.nodes.get(self.node)

        def draw(self, context):
            self.layout.operator('bbn.add_enum_node').node = node.name
            for enum in node.inputs[0].enum_items:
                self.layout.prop(enum, 'value', text='')

        bpy.context.window_manager.popup_menu(draw, title='Focus')

        node.create_enum_sockets()

        return {'FINISHED'}


class BBN_node_select(Node, BBN_node):
    bl_idname = 'bbn_select_node'
    bl_label = "Select"
    bl_icon = 'RIGHTARROW_THIN'

    input_sockets = {
        'Enum': {'type': 'BBN_enum_socket'},
        '...': {'type': 'BBN_add_array_socket'},
    }

    socket_type: bpy.props.StringProperty()

    dependent_classes = [BBN_OP_edit_enum_node, BBN_OP_add_enum_node]

    def draw_buttons(self, context, layout):
        layout, row1, row2 = self.setup_buttons(context, layout)

        row1.operator('bbn.edit_enum_node').node = self.name

    def create_enum_sockets(self):
        if self.socket_type:
            for enum in self.inputs[0].enum_items:
                self.change_socket(self.inputs, enum.value, {'type': self.socket_type, 'display_name': enum.label})

            for i in reversed(range(1, len(self.inputs))):
                x = self.inputs[i]
                if x.name not in [x.value for x in self.inputs[0].enum_items]:
                    self.inputs.remove(x)

    def execute_dependants(self, context, id, path):
        connected_socket = self.inputs[0].connected_socket
        if connected_socket:
            x = connected_socket.node
            self.execute_other(context, id, path, x)

        for x in self.inputs[1:]:
            if x.name == self.get_input_value(0):
                connected_socket = x.connected_socket
                if connected_socket:
                    x = connected_socket.node
                    self.execute_other(context, id, path, x)
                break

    def process(self, context, id, path):
        for x in self.inputs[1:]:
            if x.name == self.get_input_value(0):
                self.outputs[0].set_value(self.get_input_value(x.name))
                break

    def update(self):
        if hasattr(self, 'initializing') and self.initializing:
            return
        if runtime_info['updating']:
            return
        main_input = self.inputs[1]
        connected_socket = main_input.connected_socket

        if main_input.bl_idname == 'BBN_add_array_socket' and connected_socket:
            self.socket_type = connected_socket.bl_rna.name
            self.inputs.remove(main_input)

            self.create_enum_sockets()

            new_socket = self.outputs.new(self.socket_type, 'Out')._init()
            new_socket.init_from_socket(connected_socket.node, connected_socket)

        super().update()
