import bpy
from bpy.types import Node

from ..node_get_property import BBN_node_get_property
from ...runtime import runtime_info

from mathutils import Vector

import logging

log = logging.getLogger(__name__)


class BBN_node_get_propertygroup_prop(Node, BBN_node_get_property):
    bl_idname = 'bbn_get_propertygroup_prop_node'
    bl_label = "Get Pointer Property"

    node_version = 3

    behaviour_sockets = {
        'SINGLE': {
            'INPUTS': {
                'Pointer': {'type': 'BBN_pointer_socket'},
            }
        },
        'MULTIPLE': {
            'INPUTS': {
                'Pointer': {'type': 'BBN_pointer_array_socket'},
            }
        },
        'MULTIPLE_VALUES': {
            'INPUTS': {
                'Pointer': {'type': 'BBN_pointer_array_socket'},
            }
        },
    }

    def upgrade_node(self):
        super().upgrade_node()

        if self.current_node_version == 0:
            outputs = self.outputs[:]
            for x in outputs:
                if x.hide:
                    self.outputs.remove(x)
                else:
                    x.is_deletable = True

        self.current_node_version = self.node_version

    @property
    def props_class(self):
        if self.inputs:
            val = self.inputs[0].get_value()
            if self.current_behaviour == 'SINGLE':
                if val:
                    return val[2]
            else:
                if val and val[0]:

                    return val[0][2]

        return None

    def process(self, context, id, path):
        inputs = self.get_input_value(0, force_list=True)

        objs = [x[0] for x in inputs]
        pointers = [x[0].path_resolve(x[1]) for x in inputs]

        if self.current_behaviour == 'SINGLE':
            self.set_outputs(objs[0], pointers[0])
        else:
            self.set_outputs(objs, pointers)
