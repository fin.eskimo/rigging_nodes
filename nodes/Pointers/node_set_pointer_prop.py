import bpy
from bpy.types import Node

from ..node_set_property import BBN_node_set_property
from ...runtime import runtime_info

from mathutils import Vector

import logging

log = logging.getLogger(__name__)

# to remember the last used pointer type for each node
pointer_type_cache = {}


class BBN_node_set_propertygroup_prop(Node, BBN_node_set_property):
    bl_idname = 'bbn_set_propertygroup_prop_node'
    bl_label = "Set Pointer Property"

    node_version = 2

    behaviour_sockets = {
        'SINGLE': {
            'INPUTS': {
                'Pointer': {'type': 'BBN_pointer_socket'},
            },
            'OUTPUTS': {
                'Pointer': {'type': 'BBN_pointer_socket'},
            },
        },
        'MULTIPLE': {
            'INPUTS': {
                'Pointer': {'type': 'BBN_pointer_array_socket'},
            },
            'OUTPUTS': {
                'Pointer': {'type': 'BBN_pointer_array_socket'},
            },
        },
        'MULTIPLE_VALUES': {
            'INPUTS': {
                'Pointer': {'type': 'BBN_pointer_array_socket'},
            },
            'OUTPUTS': {
                'Pointer': {'type': 'BBN_pointer_array_socket'},
            },
        },
    }

    @property
    def props_class(self):
        if self.inputs:
            val = self.inputs[0].get_value()
            if self.current_behaviour == 'SINGLE':
                if val:
                    pointer_type_cache[self] = val[2]
                    return val[2]
            else:
                if val and val[0]:
                    pointer_type_cache[self] = val[0][2]
                    return val[0][2]
        return pointer_type_cache.get(self, None)

    def process(self, context, id, path):
        inputs = self.get_input_value(0, force_list=True)

        objs = [x[0] for x in inputs]
        pointers = [x[0].path_resolve(x[1]) for x in inputs]

        if self.current_behaviour == 'SINGLE':
            self.set_props(objs[0], pointers[0])
            self.outputs[0].set_value(inputs[0])
        else:
            self.set_props(objs, pointers)
            self.outputs[0].set_value(inputs)
