from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_collection_to_array(Node, BBN_node):
    bl_idname = 'bbn_collection_to_array_node'
    bl_label = "Collection to array"

    input_sockets = {
        'Collection': {'type': 'BBN_collection_socket'},
    }
    output_sockets = {
        'Array': {'type': 'BBN_pointer_array_socket'},
    }

    def process(self, context, id, path):
        source_object, datapath, rna_type = self.get_input_value(0)

        collection = source_object.path_resolve(datapath)

        if collection is None:
            raise ValueError(f'{source_object} -> {datapath} cannot be resolved')

        pointers = [(source_object, datapath + '[' + str(i) + ']', rna_type) for i, x in enumerate(collection)]

        self.outputs['Array'].set_value(pointers)
