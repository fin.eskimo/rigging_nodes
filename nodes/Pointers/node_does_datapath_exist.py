import bpy
from bpy.types import Node

from ..node_base import BBN_node

from mathutils import Vector

# TODO: this may be unnecessary


class BBN_node_does_datapath_exist(Node, BBN_node):
    bl_idname = 'bbn_does_datapath_exist_node'
    bl_label = "Is Datapath Valid"

    input_sockets = {
        'Source Object': {'type': 'BBN_object_ref_socket'},
        'Property Datapath': {'type': 'BBN_string_socket'},
    }

    output_sockets = {
        'Source Object': {'type': 'BBN_bool_socket'},
    }

    def process(self, context, id, path):
        obj = self.get_input_value(0)
        datapath = self.get_input_value(1)

        try:
            prop = obj.path_resolve(datapath)
        except ValueError:
            self.outputs[0].set_value(False)

        self.outputs[0].set_value(True)
