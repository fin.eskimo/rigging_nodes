from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node


from ...runtime import runtime_info
from ..node_base import BBN_node
from ...sockets import array_sockets


class BBN_node_length_of_array(Node, BBN_node):
    bl_idname = 'bbn_length_of_array_node'
    bl_label = "Length"

    input_sockets = {
        'Array': {'type': 'BBN_add_array_socket'},
    }

    output_sockets = {
        'Length': {'type': 'BBN_int_socket'},
    }

    def process(self, context, id, path):
        array = self.get_input_value('Array')

        self.set_output_value('Length', len(array))

    def update(self):
        if runtime_info['updating']:
            return

        input = self.inputs[0]
        connected_socket_0 = input.connected_socket
        if connected_socket_0:
            incoming_type = connected_socket_0.bl_idname
            if incoming_type in array_sockets.keys():
                self.change_socket(self.inputs, 'Array', {'type': incoming_type})

        super().update()
