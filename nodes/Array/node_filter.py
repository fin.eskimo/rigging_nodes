from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ...runtime import runtime_info
from ...sockets import array_sockets


class BBN_node_filter_array(Node, BBN_node):
    bl_idname = 'bbn_node_filter_array'
    bl_label = "Filter"

    input_sockets = {
        'Array': {'type': 'BBN_add_array_socket'},
        'Expression (x)': {'type': 'BBN_string_socket'}
    }

    def process(self, context, id, path):
        array = self.get_input_value(0)
        expression = self.get_input_value(1)

        def exp(x): return eval(expression)

        self.set_output_value('Array', list(filter(exp, array)))

    def update(self):
        if runtime_info['updating']:
            return

        input = self.inputs[0]
        if input.bl_idname == 'BBN_add_array_socket':
            connected_socket_0 = input.connected_socket
            if connected_socket_0:
                incoming_type = connected_socket_0.bl_idname
                if incoming_type in array_sockets.keys():
                    incomming_class = array_sockets[incoming_type]
                    self.change_socket(self.inputs, 'Array', {'type': incoming_type})
                    self.change_socket(self.outputs, 'Array', {'type': incoming_type})

        super().update()
