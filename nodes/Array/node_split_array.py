from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ...runtime import runtime_info
from ...sockets import array_sockets


class BBN_node_split_array(Node, BBN_node):
    bl_idname = 'bbn_node_split_array'
    bl_label = "Split"

    input_sockets = {
        'Array': {'type': 'BBN_add_array_socket'}
    }

    _opt_input_sockets = {
        "Start": {
            'type': 'BBN_int_socket'
        },
        "End": {
            'type': 'BBN_int_socket',
            'default_value': -1
        }
    }

    def draw_label(self):
        if 'Start' in self.inputs and 'End' in self.inputs:
            return f'Split [{self.inputs.get("Start").get_real_value()}:{self.inputs.get("End").get_real_value()}]'
        elif 'Start' in self.inputs:
            return f'Split [:{self.inputs.get("Start").get_real_value()}]'
        elif 'End' in self.inputs:
            return f'Split [{self.inputs.get("End").get_real_value()}:]'
        return 'Split'

    def process(self, context, id, path):
        array = self.get_input_value(0)

        start = self.get_input_value('Start')
        end = self.get_input_value('End')

        if start is not None and end is None:
            self.set_output_value('Array', array[start:])

        elif start is None and end is not None:
            self.set_output_value('Array', array[:end])

        elif start is not None and end is not None:
            self.set_output_value('Array', array[start:end])

        else:
            self.set_output_value('Array', array)

    def update(self):
        if runtime_info['updating']:
            return

        input = self.inputs[0]
        if input.bl_idname == 'BBN_add_array_socket':
            connected_socket_0 = input.connected_socket
            if connected_socket_0:
                incoming_type = connected_socket_0.bl_idname
                if incoming_type in array_sockets.keys():
                    incomming_class = array_sockets[incoming_type]
                    self.change_socket(self.inputs, 'Array', {'type': incoming_type})
                    self.change_socket(self.outputs, 'Array', {'type': incoming_type})

        super().update()
