from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ...runtime import runtime_info
from ...sockets import array_sockets


class BBN_node_get_from_array(Node, BBN_node):
    bl_idname = 'bbn_get_from_array_node'
    bl_label = "Get"

    input_sockets = {
        'Array': {'type': 'BBN_add_array_socket'},
        'Index': {'type': 'BBN_int_socket'}
    }

    def draw_label(self):
        if not self.inputs['Index'].connected_socket:
            val = self.inputs['Index'].get_value()
            if val == -1:
                return 'Get Last'
            elif val == 0:
                return 'Get First'
            return 'Get {}'.format(val)

        return 'Get'

    def process(self, context, id, path):
        array = self.get_input_value('Array', required=True)

        if not array:
            raise ValueError('Array is empty')

        if self.outputs:
            index = self.get_input_value('Index')
            value = array[index]
            self.set_output_value('Item', value)

    def update(self):
        if runtime_info['updating']:
            return

        input = self.inputs[0]
        if input.bl_idname == 'BBN_add_array_socket':
            connected_socket_0 = input.connected_socket
            if connected_socket_0:
                incoming_type = connected_socket_0.bl_idname
                if incoming_type in array_sockets.keys():
                    incomming_class = array_sockets[incoming_type]
                    self.change_socket(self.inputs, 'Array', {'type': incoming_type})
                    self.change_socket(self.outputs, 'Item', {'type': incomming_class.socket_class.bl_idname})

        super().update()
