from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ...runtime import runtime_info
from ...sockets import array_sockets


class BBN_node_insert_array(Node, BBN_node):
    bl_idname = 'bbn_insert_array_node'
    bl_label = "Insert"

    input_sockets = {
        'Array': {'type': 'BBN_add_array_socket'},
        'Index': {'type': 'BBN_int_socket'}
    }

    def draw_label(self):
        if not self.inputs['Index'].connected_socket:
            val = self.inputs['Index'].get_value()
            if val == -1:
                return 'Insert Before Last'
            elif val == 0:
                return 'Insert First'
            return 'Insert at {}'.format(val)

        return 'Insert'

    def process(self, context, id, path):
        array = self.get_input_value('Array', required=True).copy()

        if self.outputs:
            index = self.get_input_value('Index')
            value = self.get_input_value('Item')
            array.insert(index, value)

            self.set_output_value('Array', array)

    def update(self):
        if runtime_info['updating']:
            return

        input = self.inputs[0]
        if input.bl_idname == 'BBN_add_array_socket':
            connected_socket_0 = input.connected_socket
            if connected_socket_0:
                incoming_type = connected_socket_0.bl_idname
                if incoming_type in array_sockets.keys():
                    incomming_class = array_sockets[incoming_type]
                    self.change_socket(self.inputs, 'Array', {'type': incoming_type})
                    self.change_socket(self.inputs, 'Item', {'type': incomming_class.socket_class.bl_idname})
                    self.change_socket(self.outputs, 'Array', {'type': incoming_type})

        super().update()
