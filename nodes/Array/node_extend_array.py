from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ...runtime import runtime_info
from ...sockets import array_sockets


class BBN_node_extend_array(Node, BBN_node):
    bl_idname = 'bbn_extend_array_node'
    bl_label = "Extend"

    input_sockets = {
        'Array': {'type': 'BBN_add_array_socket'},
    }

    @property
    def predefined_socket(self):
        if self.inputs[0].bl_idname != 'BBN_add_array_socket':
            return {'type': self.inputs[0].bl_idname}
        return {}

    def process(self, context, id, path):
        array = self.get_input_value(0).copy()

        for i in range(1, len(self.inputs)):
            array2 = self.get_input_value(i)
            array.extend(array2)

        self.set_output_value('Array', array)

    def update(self):
        if runtime_info['updating']:
            return

        input = self.inputs[0]
        if input.bl_idname == 'BBN_add_array_socket':
            connected_socket_0 = input.connected_socket
            if connected_socket_0:
                incoming_type = connected_socket_0.bl_idname
                if incoming_type in array_sockets.keys():
                    self.change_socket(self.inputs, 'Array', {'type': incoming_type})
                    self.change_socket(self.outputs, 'Array', {'type': incoming_type})

        super().update()
