from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ...runtime import runtime_info
from ...sockets import array_sockets


class BBN_node_append_to_array(Node, BBN_node):
    bl_idname = 'bbn_append_to_array_node'
    bl_label = "Append"

    input_sockets = {
        'Array': {'type': 'BBN_add_array_socket'}
    }

    array_type: bpy.props.StringProperty()

    error_message: bpy.props.StringProperty()

    @property
    def predefined_socket(self):

        if self.inputs[0].bl_idname != 'BBN_add_array_socket':
            return {'type': self.inputs[1].bl_idname}
        return {}

    def process(self, context, id, path):
        array = self.get_input_value(0, required=True).copy()

        for i in range(1, len(self.inputs)):
            array.append(self.get_input_value(i))

        self.set_output_value('Array', array)

    def update(self):
        if runtime_info['updating']:
            return

        input = self.inputs[0]
        if input.bl_idname == 'BBN_add_array_socket':
            connected_socket_0 = input.connected_socket
            if connected_socket_0:
                incoming_type = connected_socket_0.bl_idname
                if incoming_type in array_sockets.keys():
                    incomming_class = array_sockets[incoming_type]
                    self.change_socket(self.inputs, 'Array', {'type': incoming_type})
                    self.change_socket(self.outputs, 'Array', {'type': incoming_type})
                    new_socket = self.inputs.new(incomming_class.socket_class.bl_idname, 'Item')._init()

        super().update()
