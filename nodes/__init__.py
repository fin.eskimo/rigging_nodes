import nodeitems_utils
from nodeitems_utils import NodeCategory, NodeItem
import os
import importlib
from bpy.types import Node, NodeCustomGroup, NodeGroupInput, NodeReroute
import inspect
import bpy


import logging
log = logging.getLogger(__name__)
log.debug('NODES')

nodes_dict = {}

node_classes = set()

categories = [x for x in os.listdir(os.path.dirname(__file__)) if os.path.isdir(os.path.join(os.path.dirname(__file__), x))]
for category in categories:

    files = [x[:-3] for x in os.listdir(os.path.join(os.path.dirname(__file__), category)) if x.endswith('.py') and not x.startswith('_')]

    if not files:
        continue

    nodes_dict[category] = list()

    for i in files:
        importlib.import_module('.' + category + '.' + i, package=__package__)

    __globals = globals().copy()

    module = __globals[category]

    for x in [x for x in dir(module) if x.startswith('node_')]:
        sub_module = getattr(module, x)
        for y in [item for item in dir(sub_module)]:
            if not y.startswith('BBN_node_'):
                continue
            node = getattr(sub_module, y)
            if node != NodeReroute and node != Node and node != NodeCustomGroup and node != NodeGroupInput and inspect.isclass(node) and issubclass(node, Node) and node not in node_classes:
                # globals()[y] = node
                node_classes.add(node)
                if not node.deprecated:
                    nodes_dict[category].append(node.bl_idname)

# SETUP CATEGORIES
nodes_dict['Group'].extend(['NodeGroupInput', 'NodeGroupOutput'])
nodes_dict['Loop'].extend(['NodeGroupInput', 'NodeGroupOutput'])
nodes_dict['Flow_Control'].extend(['NodeFrame'])
for category, classes in list(nodes_dict.items()):
    classes.sort(reverse=False)
    if not classes:
        del nodes_dict[category]


class BBN_node_category(NodeCategory):
    @ classmethod
    def poll(cls, context):
        return context.space_data.tree_type == 'bbn_tree'


node_categories = [BBN_node_category(category, category, items=[NodeItem(node) for node in node_classes]) for category, node_classes in nodes_dict.items()]


def register():
    from bpy.utils import register_class

    for cls in node_classes:
        cls.register_dependants()

    for cls in node_classes:
        #print(f'registering {cls.bl_idname} ...')
        #print(f'bases: {cls.__bases__} ...')
        register_class(cls)

    nodeitems_utils.register_node_categories('Rigging Nodes', node_categories)


def unregister():
    nodeitems_utils.unregister_node_categories('Rigging Nodes')

    from bpy.utils import unregister_class

    for cls in node_classes:
        #print(f'unregistering {cls.bl_idname} ...')
        unregister_class(cls)

    for cls in node_classes:
        cls.unregister_dependants()
