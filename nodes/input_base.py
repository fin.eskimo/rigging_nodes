from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from .node_base import BBN_node
from ..node_tree import BBN_tree

# TODO:enable inputs in subgroups by storing the node path with the registered object


class BBN_nodebase_input(BBN_node):
    subtype = 'INPUT'

    _opt_input_sockets = {
        'Name': {'type': 'BBN_string_socket'},
        'Collection': {'type': 'BBN_string_socket'},
    }

    output_sockets = {
        'Object': {'type': 'BBN_object_socket'},
    }

    executable = False

    created_object: bpy.props.PointerProperty(type=bpy.types.Object)

    def copy(self, node):
        self.created_object = None

    def register_object(self, context, obj, collection, mode, path, register_data=True):
        old = self.created_object
        self.created_object = obj

        context.space_data.node_tree.register_object(context, self.created_object, collection, mode, old, register_data, path)

    def unlink_object(self):
        self.created_object = None
