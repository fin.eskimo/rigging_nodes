import bpy
from ..node_base import BBN_node
from ...runtime import runtime_info
from bpy.types import Node


class BBN_node_group_out(Node, BBN_node):
    deprecated = True

    bl_idname = "bbn_group_out_node"
    bl_label = "Group Output"
    bl_icon = 'GROUP'

    node_version = 1

    input_sockets = {
        '...': {'type': 'BBN_add_array_socket'},
    }

    def upgrade_node(self):
        super().upgrade_node()
        for x in self.inputs[1:]:
            x.editable_name = True
            x.is_moveable = True
            x.is_deletable = True

        self.current_node_version = self.node_version

    def update(self):
        if runtime_info['updating']:
            return
        if len(self.inputs) < 1:
            return

        main_input = self.inputs[0]
        connected_socket = main_input.connected_socket

        if connected_socket:
            new_socket = self.inputs.new(connected_socket.bl_rna.name, connected_socket.bl_label)._init()
            new_socket.editable_name = True
            new_socket.is_moveable = True
            new_socket.is_deletable = True

            if len(bpy.context.space_data.path) > 1:
                for node in bpy.context.space_data.path[-2].node_tree.nodes:
                    if node.bl_rna.identifier == 'bbn_group_node':
                        if node.node_tree == self.id_data:
                            node.outputs.new(connected_socket.bl_rna.name, connected_socket.bl_label)._init()

            self.id_data.relink_socket(main_input, new_socket)

        super().update()
