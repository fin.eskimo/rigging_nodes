import bpy
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_cross(Node, BBN_node):
    deprecated = True

    bl_idname = 'bbn_cross_node'
    bl_label = "Cross"
    bl_icon = 'PANEL_CLOSE'

    input_sockets = {
        'Vector 1': {'type': 'BBN_vector_socket'},
        'Vector 2': {'type': 'BBN_vector_socket'},
    }
    output_sockets = {
        'Cross': {'type': 'BBN_vector_socket'},
    }

    def process(self, context, id, path):
        raise ValueError('This node is deprecated, replace it with the math node')
