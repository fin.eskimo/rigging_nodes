import bpy
from ..node_base import BBN_node

from bpy.types import NodeCustomGroup, Node, Operator
from mathutils import Vector

import uuid

import logging

log = logging.getLogger(__name__)


class BBN_node_group(Node, BBN_node):
    deprecated = True
    bl_idname = "bbn_group_node"
    bl_label = "Node Group"
    bl_icon = 'GROUP'

    def nested_tree_filter(self, context):
        """Define which tree we would like to use as nested trees."""
        if context.bl_idname != 'bbn_tree_group':  # It should be our dedicated to this class
            return False
        else:
            # to avoid circular dependencies
            for path_tree in bpy.context.space_data.path:
                if path_tree.node_tree.name == context.name:
                    return False
            return True

    def update_group_tree(self, context):
        self.update_sockets_from_node_tree()

    # attribute for available sub tree
    node_tree: bpy.props.PointerProperty(type=bpy.types.NodeTree, poll=nested_tree_filter, update=update_group_tree)

    input_sockets = {
        '...': {'type': 'BBN_add_array_socket'},
    }

    def draw_buttons(self, context, layout):
        super().draw_buttons(context, layout)

        row = layout.row(align=True)
        row.prop(self, "node_tree", text="")
        if self.node_tree:
            layout.prop(self.node_tree, 'name', text='Group Name')

    def process(self, context, id, path):
        raise ValueError('This node is deprecated, run the "Create correct groups" operator in the side panel to upgrade the node tree')
