from nodeitems_utils import NodeCategory, NodeItem
import nodeitems_utils
import bpy
import mathutils
from bpy.types import Node

from ..node_base import BBN_node
from ...runtime import runtime_info
import logging

log = logging.getLogger(__name__)


class BBN_node_multiply(Node, BBN_node):
    deprecated = True

    bl_idname = 'bbn_multiply_node'
    bl_label = "Multiply (*)"
    bl_icon = 'PANEL_CLOSE'

    input_sockets = {
        '...': {'type': 'BBN_add_array_socket'},
    }

    socket_type: bpy.props.StringProperty()

    def process(self, context, id, path):
        raise ValueError('This node is deprecated, replace it with the math node')
