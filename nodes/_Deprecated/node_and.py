import bpy
from bpy.types import Node

from ..node_base import BBN_node


class BBN_node_and(Node, BBN_node):
    deprecated = True
    bl_idname = 'bbn_and_node'
    bl_label = "And"

    input_sockets = {
        'Val1': {'type': 'BBN_bool_socket'},
        'Val2': {'type': 'BBN_bool_socket'},
    }

    output_sockets = {
        'Result': {'type': 'BBN_bool_socket'},
    }

    def process(self, context, id, path):
        raise ValueError('Replace with the "logic" node')
