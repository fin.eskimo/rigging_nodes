import bpy
from bpy.types import Node
from ..node_base import BBN_node


drivervar_types = [(x.identifier, x.name, x.description, x.icon, i) for i, x in enumerate(bpy.types.DriverVariable.bl_rna.properties['type'].enum_items[:])]
transform_type_enum = [(x.identifier, x.name, x.description, x.icon, i) for i, x in enumerate(bpy.types.DriverTarget.bl_rna.properties['transform_type'].enum_items[:])]
rotation_mode_enum = [(x.identifier, x.name, x.description, x.icon, i) for i, x in enumerate(bpy.types.DriverTarget.bl_rna.properties['rotation_mode'].enum_items[:])]
transform_space_enum = [(x.identifier, x.name, x.description, x.icon, i) for i, x in enumerate(bpy.types.DriverTarget.bl_rna.properties['transform_space'].enum_items[:])]


class BBN_node_add_driver_variable(Node, BBN_node):
    bl_idname = 'bbn_add_driver_variable'
    bl_label = "Setup Driver Variable"
    bl_icon = 'DRIVER'

    node_version = 1

    behaviour_enum = drivervar_types

    behaviour_sockets = {
        'SINGLE_PROP': {
            'INPUTS': {
                '0 Object': {'type': 'BBN_object_ref_socket'},
                '0 Datapath': {'type': 'BBN_string_socket'},
            }
        },
        'TRANSFORMS': {
            'INPUTS': {
                '0 Object': {'type': 'BBN_object_ref_socket'},
                '0 Bone': {'type': 'BBN_string_socket'},
                '0 Transform Type': {'type': 'BBN_enum_socket', 'items': transform_type_enum},
                '0 Rotation Mode': {'type': 'BBN_enum_socket', 'items': rotation_mode_enum},
                '0 Transform Space': {'type': 'BBN_enum_socket', 'items': transform_space_enum},
            }
        },
        'ROTATION_DIFF': {
            'INPUTS': {
                '0 Object': {'type': 'BBN_object_ref_socket'},
                '0 Bone': {'type': 'BBN_string_socket'},
                '1 Object': {'type': 'BBN_object_ref_socket'},
                '1 Bone': {'type': 'BBN_string_socket'},
            }
        },
        'LOC_DIFF': {
            'INPUTS': {
                '0 Object': {'type': 'BBN_object_ref_socket'},
                '0 Bone': {'type': 'BBN_string_socket'},
                '0 Transform Space': {'type': 'BBN_enum_socket', 'items': transform_space_enum},
                '1 Object': {'type': 'BBN_object_ref_socket'},
                '1 Bone': {'type': 'BBN_string_socket'},
                '1 Transform Space': {'type': 'BBN_enum_socket', 'items': transform_space_enum},
            }
        },
    }

    input_sockets = {
        'Name': {'type': 'BBN_string_socket'},
    }

    output_sockets = {
        'Driver Variable': {'type': 'BBN_drivervar_socket'},
    }

    executable = False

    def upgrade_node(self):
        super().upgrade_node()

        # remap old properties
        if 'drivervar_type' in self.keys():
            self['current_behaviour'] = self['drivervar_type']
            del self['drivervar_type']

        self.current_node_version = self.node_version

    def process(self, context, id, path):

        ans = {
            'variable_name': self.get_input_value('Name'),
            'variable_type': self.current_behaviour,

            'target0_object': self.get_input_value('0 Object', default=None),
            'target0_bone': self.get_input_value('0 Bone', default=''),
            'target0_datapath': self.get_input_value('0 Datapath', default=''),
            'target0_transform_type': self.get_input_value('0 Transform Type', default='LOC_X'),
            'target0_rotation_mode': self.get_input_value('0 Rotation Mode', default='AUTO'),
            'target0_transform_space': self.get_input_value('0 Transform Space', default='WORLD_SPACE'),

            'target1_object': self.get_input_value('1 Object', default=None),
            'target1_bone': self.get_input_value('1 Bone', default=''),
            'target1_datapath': self.get_input_value('1 Datapath', default=''),
            'target1_transform_type': self.get_input_value('1 Transform Type', default='LOC_X'),
            'target1_rotation_mode': self.get_input_value('1 Rotation Mode', default='AUTO'),
            'target1_transform_space': self.get_input_value('1 Transform Space', default='WORLD_SPACE'),

        }
        self.outputs[0].set_value(ans)
