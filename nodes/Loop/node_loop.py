from ...runtime import cache_loop_index, cache_loop_inputs, cache_node_group_outputs, runtime_info
import bpy
from ..node_base import BBN_node

from bpy.types import NodeCustomGroup, Node, Operator
from mathutils import Vector

import uuid

import logging

log = logging.getLogger(__name__)


class BBN_OP_create_loop(Operator):
    bl_idname = "bbn.create_loop"
    bl_label = "Create Loop"

    node: bpy.props.StringProperty()

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        bone_tree = context.space_data.edit_tree
        orig_node = bone_tree.nodes[self.node]

        new_node_tree = bpy.data.node_groups.new("LOOP_new_loop", "bbn_tree_loop")
        orig_node.node_tree_selection = new_node_tree
        loop_info = new_node_tree.nodes.new("bbn_loop_info")
        loop_input = new_node_tree.nodes.new("NodeGroupInput")
        loop_output = new_node_tree.nodes.new("NodeGroupOutput")

        new_node_tree.inputs.new('BBN_int_socket', 'Start Index')
        new_node_tree.inputs.new('BBN_int_socket', 'End Index')

        orig_node.node_tree_selection = new_node_tree
        return {'FINISHED'}


class BBN_OP_setup_loop(Operator):
    '''Creates "Start Index" and "End Index" sockets in the node group if they don't exist'''

    bl_idname = "bbn.setup_loop"
    bl_label = "Setup Loop"

    node_group: bpy.props.StringProperty()

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = bpy.data.node_groups[self.node_group]

        if 'Start Index' not in tree.inputs:
            tree.inputs.new('BBN_int_socket', 'Start Index')
        if 'End Index' not in tree.inputs:
            tree.inputs.new('BBN_int_socket', 'End Index')

        return {'FINISHED'}


class BBN_node_loop(NodeCustomGroup, BBN_node):
    '''Converts old loop trees into the new types'''
    bl_idname = "bbn_node_loop"
    bl_label = "Loop"
    bl_icon = 'FILE_REFRESH'

    log_time = False

    dependent_classes = [BBN_OP_create_loop, BBN_OP_setup_loop]

    def loop_tree_filter(self, context):
        """Define which tree we would like to use as loop trees."""
        if context:
            if context.bl_idname != 'bbn_tree_loop':  # It should be our dedicated to this class
                return False
            else:
                # to avoid circular dependencies
                for path_tree in bpy.context.space_data.path:
                    if path_tree.node_tree.name == context.name:
                        return False
                return True
        return False

    def update_loop_tree(self, context):
        self.node_tree = self.node_tree_selection

    node_tree_selection: bpy.props.PointerProperty(type=bpy.types.NodeTree, poll=loop_tree_filter, update=update_loop_tree)

    def draw_buttons(self, context, layout):
        super().draw_buttons(context, layout)

        row = layout.row(align=True)
        row.prop(self, "node_tree_selection", text="")
        if self.node_tree:
            layout.prop(self.node_tree, 'name', text='Loop Name')
            row.operator('bbn.setup_loop', text='', icon='MODIFIER_ON').node_group = self.node_tree.name
        else:
            row.operator('bbn.create_loop', text='', icon='ADD').node = self.name

    def process_group(self, context, id, path):
        start_index = self.get_input_value('Start Index', required=True, throw=True)
        end_index = self.get_input_value('End Index', required=True, throw=True)

        if end_index - start_index <= 0:
            raise ValueError('The loop will not be executed')

        # Cache all the output nodes
        if self.node_tree not in cache_node_group_outputs:
            cache_node_group_outputs[self.node_tree] = []
            for x in self.node_tree.nodes:
                if x.bl_rna.identifier == 'NodeGroupOutput':
                    cache_node_group_outputs[self.node_tree].append(x)

        path = path + [self.name]

        cache_loop_inputs[self.node_tree] = {}

        for i in range(start_index, end_index):
            cache_loop_index[self.node_tree] = i
            execute_id = str(uuid.uuid4())
            outputs = set()

            # first execute everything
            for x in cache_node_group_outputs[self.node_tree]:
                self.execute_other(context, execute_id, path, x)

            # after everything is executed gather the results for the next loop
            for x in cache_node_group_outputs[self.node_tree]:
                # if we are on the last loop, set the values of the output sockets
                if i == end_index - 1:
                    for socket in x.inputs:
                        if socket.identifier not in outputs:
                            try:
                                output = next(y for y in self.outputs if y.identifier == socket.identifier)
                            except StopIteration:
                                continue

                            output.set_value(socket.get_value())

                            if socket.links:
                                outputs.add(socket.identifier)
                        elif socket.links:
                            raise ValueError(f'Socket {x}:{socket.name} has already been set by another Group Output Node')
                        else:
                            pass
                else:
                    for socket in x.inputs:
                        if socket.bl_idname != 'NodeSocketVirtual':
                            if socket.name not in outputs:
                                cache_loop_inputs.setdefault(self.node_tree, {})[socket.name] = socket.get_value()

                                if socket.links:
                                    outputs.add(socket.name)
                            elif socket.links:
                                raise ValueError(f'Socket {x}:{socket.name} has already been set by another Group Output Node')
                            else:
                                pass
                    pass
