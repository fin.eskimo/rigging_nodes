import bpy
from bpy.types import Node

from ..node_base import BBN_node

from ...node_tree import BBN_tree

from mathutils import Vector


class BBN_node_apply_action(Node, BBN_node):
    bl_idname = 'bbn_node_apply_action'
    bl_label = "Apply Action"

    input_sockets = {
        'Object': {'type': 'BBN_object_socket'},
        'Action': {'type': 'BBN_action_socket'},
    }

    output_sockets = {
        'Object': {'type': 'BBN_object_socket'},
    }

    input_to_focus = 'Object'
    focus_mode = {'POSE', 'OBJECT'}

    def process(self, context, id, path):
        armature = self.get_input_value(0)
        action = self.get_input_value(1)

        if not armature.animation_data:
            armature.animation_data_create()
        armature.animation_data.action = action

        self.outputs[0].set_value(armature)
