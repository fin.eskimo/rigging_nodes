import bpy
from bpy.types import Node

from ..node_get_property import BBN_node_get_property
from mathutils import Vector

import logging

log = logging.getLogger(__name__)


class BBN_node_get_object_property(Node, BBN_node_get_property):
    bl_idname = 'bbn_get_object_property_node'
    bl_label = "Get Object Property"

    node_version = 1

    behaviour_sockets = {
        'SINGLE': {
            'INPUTS': {
                'Object': {'type': 'BBN_object_ref_socket'},
            }
        }
    }

    def updated_enum(self, context):
        self.update_behaviour_sockets()

    object_type: bpy.props.EnumProperty(
        items=[
            ('OBJECT', 'Object', 'Object', 'OBJECT_DATAMODE', 0),
            ('CURVE', 'Curve', 'Curve', 'OUTLINER_OB_CURVE', 1),
            ('ARMATURE', 'Armature', 'Armature', 'OUTLINER_OB_ARMATURE', 2),
            ('GPENCIL', 'GPencil', 'Grease Pencil', 'OUTLINER_OB_GREASEPENCIL', 3),
        ],
        update=updated_enum)

    def draw_buttons(self, context, layout):

        layout, row1, row2 = self.setup_buttons(context, layout)
        row1.prop(self, 'object_type', text='')

    def upgrade_node(self):
        super().upgrade_node()

        if self.current_node_version == 0:
            inputs = self.inputs[1:]
            for x in inputs:
                if x.hide:
                    self.inputs.remove(x)
                else:
                    x.is_deletable = True

        self.current_node_version = self.node_version

    @ property
    def props_class(self):
        if self.object_type == 'OBJECT':
            return bpy.types.Object
        elif self.object_type == 'CURVE':
            return bpy.types.Curve
        elif self.object_type == 'ARMATURE':
            return bpy.types.Armature
        elif self.object_type == 'MESH':
            return bpy.types.Mesh
        elif self.object_type == 'GPENCIL':
            return bpy.types.GreasePencil

    def process(self, context, id, path):
        obj = self.get_input_value("Object")

        data = obj if self.object_type == 'OBJECT' else obj.data

        self.set_outputs(obj, data)
