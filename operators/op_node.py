from bpy.types import Operator
import math
import bpy
from collections import OrderedDict

import bpy
import mathutils
from mathutils import Vector
import uuid

from ..sockets.socket import draw_times, get_value_times
from ..runtime import cache_socket_variables, runtime_info


class BBN_OP_group_nodes(Operator):
    bl_idname = "bbn.group_nodes"
    bl_label = "Group nodes"

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        runtime_info['updating'] = True
        try:
            # Get space, path, current nodetree, selected nodes and a newly created group
            space = context.space_data
            path = space.path
            node_tree = space.path[-1].node_tree
            node_group = bpy.data.node_groups.new("GRP_new_group", "bbn_tree_group")
            selected_nodes = [i for i in node_tree.nodes if i.select]
            nodes_len = len(selected_nodes)

            # Store all links (internal/external) for the selected nodes to be created as group inputs/outputs
            links_external_in = []
            links_external_out = []
            for n in selected_nodes:
                for i in n.inputs:
                    if (i.links):
                        l = i.links[0]
                        if (not l.from_node in selected_nodes):
                            if (not l in links_external_in):
                                links_external_in.append(l)
                for o in n.outputs:
                    if (o.links):
                        for l in o.links:
                            if (not l.to_node in selected_nodes):
                                if (not l in links_external_out):
                                    links_external_out.append(l)

            def sort_in_link(link):
                return link.from_node.location[1]

            def sort_out_link(link):
                return link.to_node.location[1]

            links_external_in.sort(reverse=True, key=sort_in_link)
            links_external_out.sort(reverse=False, key=sort_out_link)

            # Calculate the required locations for placement of grouped node and input/output nodes

            def get_world_location(node):
                location = node.location.copy()
                while node.parent:
                    node = node.parent
                    location += node.location
                return location

            loc_x_in = math.inf
            loc_x_out = -math.inf
            loc_avg = Vector((0, 0))
            for n in selected_nodes:
                loc_avg += get_world_location(n) / nodes_len
                if (get_world_location(n)[0] < loc_x_in):
                    loc_x_in = get_world_location(n)[0]
                if (get_world_location(n)[0] > loc_x_out):
                    loc_x_out = get_world_location(n)[0] + n.width

            # Create and relocate group input & output nodes in the newly created group
            group_input = node_group.nodes.new("NodeGroupInput")
            group_output = node_group.nodes.new("NodeGroupOutput")
            group_input.location = Vector((loc_x_in - 280, loc_avg[1]))
            group_output.location = Vector((loc_x_out + 200, loc_avg[1]))

            # Copy the selected nodes from current nodetree
            # TODO: no idea why this sometimes crashes blender
            # it seems to happen when sockets change when in the copy() or update() functions, makes sense
            if (nodes_len > 0):
                bpy.ops.node.clipboard_copy()

            # Create a grouped node with correct location and assign newly created group
            group_node = node_tree.nodes.new("bbn_node_group_custom")
            group_node.location = loc_avg
            group_node.node_tree_selection = node_group

            # Add overlay to node editor for the newly created group
            path.append(node_group, node=group_node)

            if (nodes_len > 0):
                #! if in the copy() node function sockets are changed or deleted this crashes blender, make a check for runtime_info['updating'] before deleting sockets in def copy()
                bpy.ops.node.clipboard_paste()

                for node in node_group.nodes:
                    if not node.parent:
                        node.location = node.location - loc_avg

            for link in links_external_in:
                node = node_group.nodes[link.to_node.name]
                connect_socket = next(x for x in node.inputs if x.identifier == link.to_socket.identifier)
                node_group.links.new(connect_socket, group_input.outputs[-1])

            for link in links_external_out:

                node = node_group.nodes[link.from_node.name]
                connect_socket = next(x for x in node.outputs if x.identifier == link.from_socket.identifier)

                node_group.links.new(connect_socket, group_output.inputs[-1])

            # Add new links to grouped node from original external links
            for i in range(0, len(links_external_in)):
                link = links_external_in[i]

                node = node_tree.nodes[link.from_node.name]
                connect_socket = next(x for x in node.outputs if x.identifier == link.from_socket.identifier)

                node_tree.links.new(connect_socket, group_node.inputs[i])

            for i in range(0, len(links_external_out)):
                link = links_external_out[i]

                node = node_tree.nodes[link.to_node.name]
                connect_socket = next(x for x in node.inputs if x.identifier == link.to_socket.identifier)

                node_tree.links.new(group_node.outputs[i], connect_socket)

            for n in selected_nodes:
                node_tree.nodes.remove(n)

        finally:
            runtime_info['updating'] = False

        return {"FINISHED"}


class BBN_OP_edit_group(Operator):
    """Edit the group referenced by the active node (or exit the current node-group)"""
    bl_idname = "bbn.edit_group"
    bl_label = "Edit Group"

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        space = context.space_data
        path = space.path
        node = path[-1].node_tree.nodes.active

        if hasattr(node, "node_tree") and node.select:
            if (node.node_tree):
                path.append(node.node_tree, node=node)
                draw_times.clear()
                get_value_times.clear()
                return {"FINISHED"}
        elif len(path) > 1:
            path.pop()
            draw_times.clear()
            get_value_times.clear()
        return {"CANCELLED"}


class BBN_OP_generate_rig(Operator):
    bl_idname = "bbn.generate_rig"
    bl_label = "Execute"
    bl_options = {"REGISTER", "UNDO"}

    node_name: bpy.props.StringProperty()

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def invoke(self, context, event):
        bone_tree = context.space_data.edit_tree
        if self.node_name:
            bone_tree.set_preview(self.node_name)
        bone_tree.execute(context)
        return {'FINISHED'}


class BBN_OP_update_sockets(Operator):
    '''Updates node sockets (useful when updating the addon)\nBACKUP YOUR WORK FIRST!'''

    bl_idname = "bbn.update_nodes"
    bl_label = "Update Nodes"

    @classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        correct_types = {
            'BBN_float_socket': 'BBN_float_array_socket',
            'BBN_string_socket': 'BBN_string_array_v2_socket',
            'BBN_bool_socket': 'BBN_bool_array_socket',
            'BBN_vector_socket': 'BBN_vector_array_socket',
        }

        def get_array_socket_type(socket, default='BBN_string_socket'):
            if not socket.array_socket_type:
                if socket.value:
                    array_socket_type = socket.value[0].array_type
                else:
                    array_socket_type = default
            else:
                array_socket_type = socket.array_socket_type

            return array_socket_type

        def fix_arrays(node, sockets):
            socket_list = [x for x in sockets]
            for i, socket in enumerate(socket_list):
                if socket.bl_idname == 'BBN_string_array_socket':
                    array_socket_type = get_array_socket_type(socket)
                    required_length = socket.required_length
                    def_value = []
                    if required_length:
                        def_value = [x.get_value() for x in socket.value]

                    correct_type = correct_types[array_socket_type]
                    if hasattr(node, 'change_socket'):
                        socket = node.change_socket(sockets, i, {'type': correct_type, 'required_length': required_length, 'default_value': def_value})

        runtime_info['updating'] = True
        try:
            groups = {}
            for node_group in bpy.data.node_groups:
                for node in node_group.nodes[:]:
                    if node.bl_idname == 'bbn_node_group_custom':
                        groups.setdefault(node.node_tree, []).append(node)

            for tree, node_groups in groups.items():
                if not tree:
                    continue
                for i, socket in enumerate(tree.inputs):
                    if socket.bl_rna.identifier == 'BBN_socket_array_interface_old':
                        array_type = None
                        for node in [x for x in tree.nodes if x.bl_idname == 'NodeGroupInput']:
                            if node.outputs[i].links:
                                array_type = get_array_socket_type(node.outputs[i])
                                break

                        tree.inputs.new(correct_types[array_type], socket.name)
                        new_socket = node.outputs[-2]

                        for node in [x for x in tree.nodes if x.bl_idname == 'NodeGroupInput']:
                            if node.outputs[i].links:
                                to_sockets = [x.to_socket for x in node.outputs[i].links]
                                for to_socket in to_sockets:
                                    tree.links.new(new_socket, to_socket)

                        for node_group in node_groups:
                            node_group_tree = node_group.id_data
                            new_socket = node_group.inputs[-1]

                            if node_group.inputs[i].links:
                                node_group_tree.links.new(node_group.inputs[i].links[0].from_socket, new_socket)

                for i, socket in enumerate(tree.outputs):
                    if socket.bl_rna.identifier == 'BBN_socket_array_interface_old':
                        array_type = None
                        for node in [x for x in tree.nodes if x.bl_idname == 'NodeGroupOutput']:
                            if node.inputs[i].links:
                                array_type = get_array_socket_type(node.inputs[i])
                                break
                        tree.outputs.new(correct_types[array_type], socket.name)
                        new_socket = node.inputs[-2]

                        for node in [x for x in tree.nodes if x.bl_idname == 'NodeGroupOutput']:
                            if node.inputs[i].links:
                                from_sockets = [x.from_socket for x in node.inputs[i].links]
                                for from_socket in from_sockets:
                                    tree.links.new(new_socket, from_socket)

                        for node_group in node_groups:
                            node_group_tree = node_group.id_data
                            new_socket = node_group.outputs[-1]

                            if node_group.outputs[i].links:
                                to_sockets = [x.to_socket for x in node_group.outputs[i].links]
                                for to_socket in to_sockets:
                                    node_group_tree.links.new(new_socket, to_socket)

                for socket in reversed(tree.inputs):
                    if socket.bl_rna.identifier == 'BBN_socket_array_interface_old':
                        tree.inputs.remove(socket)

                for socket in reversed(tree.outputs):
                    if socket.bl_rna.identifier == 'BBN_socket_array_interface_old':
                        tree.outputs.remove(socket)

            cache_socket_variables.clear()

            for bone_tree in bpy.data.node_groups:
                if bone_tree.rna_type.identifier in {'bbn_tree', 'bbn_tree_group', 'bbn_tree_loop'}:
                    for node in bone_tree.nodes:
                        if hasattr(node, 'deprecated') and node.deprecated:
                            print(f'{bone_tree}:{node.bl_idname} is deprecated')

                            context.space_data.path.append(bone_tree)
                            bone_tree.nodes.active = node

                            bpy.ops.node.select_all(action='DESELECT')
                            node.select = True

                            def draw(self, context):
                                self.layout.operator('bbn.focus_node').node = node.name

                            bpy.context.window_manager.popup_menu(draw, title='Focus')

                            return {'FINISHED'}

                        # new_node.location = node.location

                        if hasattr(node, 'current_node_version'):
                            if node.current_node_version != node.node_version:
                                node.upgrade_node()

            for bone_tree in bpy.data.node_groups:
                if bone_tree.rna_type.identifier in {'bbn_tree', 'bbn_tree_group', 'bbn_tree_loop'}:
                    for node in bone_tree.nodes:
                        fix_arrays(node, node.inputs)
                        fix_arrays(node, node.outputs)
        finally:
            runtime_info['updating'] = False
        return {'FINISHED'}


class BBN_OP_move_socket(Operator):
    bl_idname = "bbn.move_socket"
    bl_label = "Move Socket"

    node: bpy.props.StringProperty()
    current_index: bpy.props.IntProperty()
    new_index: bpy.props.IntProperty()
    is_output: bpy.props.BoolProperty()

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        bone_tree = context.space_data.edit_tree

        node = bone_tree.nodes[self.node]
        sockets = node.outputs if self.is_output else node.inputs
        current_socket = sockets[self.current_index]
        new_socket = sockets[self.new_index]

        if current_socket.is_moveable and new_socket.is_moveable:
            sockets.move(self.new_index, self.current_index)
        return {'FINISHED'}


class BBN_OP_delete_socket(Operator):
    bl_idname = "bbn.delete_socket"
    bl_label = "Delete Socket"

    node: bpy.props.StringProperty()
    is_output: bpy.props.BoolProperty()
    current_index: bpy.props.IntProperty()

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        bone_tree = context.space_data.edit_tree

        node = bone_tree.nodes[self.node]
        sockets = node.outputs if self.is_output else node.inputs
        current_socket = sockets[self.current_index]

        sockets.remove(current_socket)

        return {'FINISHED'}


propertygryoup_types = []


def get_propertygroup_types_enum(self, context):
    global propertygryoup_types
    propertygryoup_types = [(x.bl_rna.name, x.bl_rna.name, x.bl_rna.name, '', index) for index, x in enumerate(bpy.types.PropertyGroup.__subclasses__())]
    return propertygryoup_types


class BBN_OP_set_propertygroup_type(Operator):
    bl_idname = "bbn.set_propertygroup_type"
    bl_label = "Set propertygroup type"

    node: bpy.props.StringProperty()

    propertygroup_type: bpy.props.EnumProperty(items=get_propertygroup_types_enum)

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = context.space_data.edit_tree

        node = tree.nodes.active

        if hasattr(node, 'propertygroup_type'):
            node.propertygroup_type = self.propertygroup_type
        return {'FINISHED'}


class BBN_OP_new_datablock(Operator):
    bl_idname = "bbn.new_datablock"
    bl_label = "New Datablock"

    node: bpy.props.StringProperty()

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = context.space_data.edit_tree
        node = tree.nodes.get(self.node)
        node.create_datablock(context)

        return {'FINISHED'}


class BBN_OP_edit_datablock(Operator):
    bl_idname = "bbn.edit_datablock"
    bl_label = "Edit Datablock"

    node: bpy.props.StringProperty()

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def invoke(self, context, event):
        tree = context.space_data.edit_tree
        node = tree.nodes.get(self.node)
        clear_other_objects = not event.ctrl
        node.edit_datablock(context, clear_other_objects=clear_other_objects)

        return {'FINISHED'}


class BBN_OP_show_shocket_value(Operator):
    bl_idname = "bbn.show_shocket_value"
    bl_label = ""

    value: bpy.props.StringProperty()

    @ classmethod
    def description(self, context, properties):
        return properties.value

    def execute(self, context):
        return {'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def draw(self, context):
        strings = self.value.split('\n')
        layout = self.layout

        col = layout.column()
        for x in strings:
            col.label(text=x)


class BBN_OP_show_full_error_message(Operator):
    bl_idname = "bbn.show_full_error_message"
    bl_label = ""

    node: bpy.props.StringProperty()

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    @ classmethod
    def description(self, context, properties):
        tree = context.space_data.edit_tree
        node = tree.nodes.get(properties.node)
        return node.error_message_full

    def execute(self, context):
        return {'FINISHED'}


class BBN_OP_focus_node(Operator):
    bl_idname = "bbn.focus_node"
    bl_label = "Focus Node"

    node: bpy.props.StringProperty()

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = context.space_data.edit_tree
        node = tree.nodes.get(self.node)

        bpy.ops.node.select_all(action='DESELECT')
        tree.nodes.active = node
        node.select = True
        bpy.ops.node.view_selected()

        return {'FINISHED'}


class BBN_OP_select_referencing_nodes(Operator):
    bl_idname = "bbn.select_referencing_nodes"
    bl_label = "Select Referencing Nodes"

    node: bpy.props.StringProperty()

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        tree = context.space_data.edit_tree
        node = tree.nodes.get(self.node)

        nodes = [x for x in tree.nodes if x.bl_idname == 'bbn_get_variable' and x.variable_name == node.variable_name]

        if nodes:
            bpy.ops.node.select_all(action='DESELECT')
            tree.nodes.active = nodes[0]
            for x in nodes:
                x.select = True
            bpy.ops.node.view_selected()

        return {'FINISHED'}


class BBN_OP_create_correct_tree_types(Operator):
    bl_idname = "bbn.create_correct_tree_types"
    bl_label = "Create Correct tree types"

    def copy_nodes_to_new_tree(self, context, node_group, new_tree_type):
        node_group_name = node_group.name
        node_group.name = node_group_name + '__TEMP'

        new_node_group = bpy.data.node_groups.new(node_group_name, new_tree_type)

        space = context.space_data
        path = space.path
        # Add overlay to node editor for the newly created group
        path.append(node_group)
        for x in node_group.nodes:
            x.select = True

        bpy.ops.node.clipboard_copy()
        path.pop()

        path.append(new_node_group)
        bpy.ops.node.clipboard_paste()
        path.pop()

        node_group.user_remap(new_node_group)
        bpy.data.node_groups.remove(node_group)
        pass

    def execute(self, context):
        node_group_nodes = ['bbn_group_in_node', 'bbn_group_out_node']
        loop_group_nodes = ['bbn_loop_start', 'bbn_loop_end']

        groups = set()
        loops = set()
        for node_group in bpy.data.node_groups:
            if node_group.bl_idname == 'bbn_tree':
                for node in node_group.nodes:
                    if node.bl_idname in node_group_nodes:
                        groups.add(node_group)
                        break
                    elif node.bl_idname in loop_group_nodes:
                        loops.add(node_group)
                        break
        for node_group in groups:
            self.copy_nodes_to_new_tree(context, node_group, 'bbn_tree_group')

        for node_group in loops:
            self.copy_nodes_to_new_tree(context, node_group, 'bbn_tree_loop')

        return {'FINISHED'}


class BBN_OP_focus_path_node(Operator):
    bl_idname = "bbn.focus_path_node"
    bl_label = "Focus Node"

    path: bpy.props.StringProperty()

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):
        context.space_data.path.clear()

        path = self.path.split('////')

        current_tree = bpy.data.node_groups.get(path[0])
        assert current_tree
        context.space_data.node_tree = current_tree
        node = current_tree.nodes.get(path[1])
        assert node
        bpy.ops.node.select_all(action='DESELECT')
        node.select = True
        current_tree.nodes.active = node

        for x in path[2:]:
            current_tree = node.node_tree
            node = current_tree.nodes.get(x)
            current_tree.nodes.active = node
            context.space_data.path.append(current_tree)

            bpy.ops.node.select_all(action='DESELECT')
            node.select = True

        def draw(self, context):
            self.layout.operator('bbn.focus_node').node = node.name

        bpy.context.window_manager.popup_menu(draw, title='Focus')

        return {'FINISHED'}


class BBN_OP_focus_error_node(Operator):
    bl_idname = "bbn.focus_error_node"
    bl_label = "Focus Node"

    path: bpy.props.StringProperty()
    error_msg: bpy.props.StringProperty()

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    @ classmethod
    def description(self, context, properties):
        return properties.error_msg

    def execute(self, context):
        bpy.ops.bbn.focus_path_node(path=self.path)

        return {'FINISHED'}


last_node = None
last_possible_sockets = []
last_possible_sockets_32 = []
last_possible_sockets_64 = []
last_possible_sockets_96 = []
last_possible_sockets_128 = []


def sort_sockets(elem):
    return elem[1].get("display_name", elem[0])


def get_possible_sockets(node, var, sockets, search):
    global last_node
    global last_possible_sockets
    opt_sockets = getattr(node, var)
    last_node = node
    last_possible_sockets = [(x, opt_sockets[x]) for x in opt_sockets if x not in [y.name for y in sockets] and search in x]
    last_possible_sockets.sort(key=sort_sockets)
    return last_possible_sockets


def sockets_to_enum(sockets, range1, range2):
    ans = [(
        sockets[i][0],
        f'{sockets[i][1].get("display_name", sockets[i][0])}',
        f'{sockets[i][1].get("description", sockets[i][0])}\n{sockets[i][0]}',
    ) for i in range(range1, min(len(sockets), range2))]
    return ans


var_to_check = {
    'opt_input_sockets': 'inputs',
    'opt_output_sockets': 'outputs'
}


def clear_cached_optional_sockets():
    global last_possible_sockets_32
    global last_possible_sockets_64
    global last_possible_sockets_96
    global last_possible_sockets_128

    last_possible_sockets_32 = []
    last_possible_sockets_64 = []
    last_possible_sockets_96 = []
    last_possible_sockets_128 = []


class BBN_OP_add_socket(Operator):
    '''Add optional socket'''
    bl_idname = "bbn.add_socket"
    bl_label = "Add Socket"
    bl_options = {'UNDO', 'INTERNAL'}

    socket_var: bpy.props.EnumProperty(items=[
        ('opt_input_sockets', 'Inputs', 'Inputs'),
        ('opt_output_sockets', 'Outputs', 'Outputs'),
    ])

    def search_updated(self, context):
        clear_cached_optional_sockets()

    search: bpy.props.StringProperty(options={'TEXTEDIT_UPDATE', 'SKIP_SAVE'}, update=search_updated)

    node: bpy.props.StringProperty()

    def get_sockets_32(self, context):
        global last_possible_sockets_32
        if last_possible_sockets_32:
            return last_possible_sockets_32
        node = context.space_data.edit_tree.nodes.get(self.node)
        sockets = get_possible_sockets(node, self.socket_var, getattr(node, var_to_check[self.socket_var]), self.search)
        last_possible_sockets_32 = sockets_to_enum(sockets, 0, 32)
        return last_possible_sockets_32

    def get_sockets_64(self, context):
        global last_possible_sockets_64
        if last_possible_sockets_64:
            return last_possible_sockets_64
        node = context.space_data.edit_tree.nodes.get(self.node)
        sockets = get_possible_sockets(node, self.socket_var, getattr(node, var_to_check[self.socket_var]), self.search)
        last_possible_sockets_64 = sockets_to_enum(sockets, 32, 64)
        return last_possible_sockets_64

    def get_sockets_96(self, context):
        global last_possible_sockets_96
        if last_possible_sockets_96:
            return last_possible_sockets_96
        node = context.space_data.edit_tree.nodes.get(self.node)
        sockets = get_possible_sockets(node, self.socket_var, getattr(node, var_to_check[self.socket_var]), self.search)
        last_possible_sockets_96 = sockets_to_enum(sockets, 64, 96)
        return last_possible_sockets_96

    def get_sockets_128(self, context):
        global last_possible_sockets_128
        if last_possible_sockets_128:
            return last_possible_sockets_128
        node = context.space_data.edit_tree.nodes.get(self.node)
        sockets = get_possible_sockets(node, self.socket_var, getattr(node, var_to_check[self.socket_var]), self.search)
        last_possible_sockets_128 = sockets_to_enum(sockets, 96, 128)
        return last_possible_sockets_128

    socket_32: bpy.props.EnumProperty(items=get_sockets_32, options={'ENUM_FLAG'})
    socket_64: bpy.props.EnumProperty(items=get_sockets_64, options={'ENUM_FLAG'})
    socket_96: bpy.props.EnumProperty(items=get_sockets_96, options={'ENUM_FLAG'})
    socket_128: bpy.props.EnumProperty(items=get_sockets_128, options={'ENUM_FLAG'})

    @ classmethod
    def poll(cls, context):
        return context.space_data.type == "NODE_EDITOR" and context.space_data.tree_type == 'bbn_tree'

    def execute(self, context):

        global last_node
        node_tree = context.space_data.edit_tree
        node = node_tree.nodes.get(self.node)

        for socket in self.socket_32 | self.socket_64 | self.socket_96 | self.socket_128:
            socket_info = getattr(node, self.socket_var)[socket].copy()
            del socket_info['type']
            socket_info['is_deletable'] = True

            getattr(node, var_to_check[self.socket_var]).new(getattr(node, self.socket_var)[socket]['type'], socket)._init(**socket_info)

        self.socket_32, self.socket_64, self.socket_96, self.socket_128 = set(), set(), set(), set()
        last_node = None
        return {'FINISHED'}

    def invoke(self, context, event):
        global last_possible_sockets_32
        global last_possible_sockets_64
        global last_possible_sockets_96
        global last_possible_sockets_128

        last_possible_sockets_32 = []
        last_possible_sockets_64 = []
        last_possible_sockets_96 = []
        last_possible_sockets_128 = []

        wm = context.window_manager
        node_tree = context.space_data.edit_tree
        node = node_tree.nodes.get(self.node)
        sockets = get_possible_sockets(node, self.socket_var, getattr(node, var_to_check[self.socket_var]), self.search)
        width = (1 + int(len(sockets) / 32)) * 150
        return wm.invoke_props_dialog(self, width=width)

    def draw(self, context):
        layout = self.layout
        layout.prop(self, 'search')
        row = layout.row()
        col = row.column()
        col.prop(self, 'socket_32')
        col = row.column()
        col.prop(self, 'socket_64')
        col = row.column()
        col.prop(self, 'socket_96')
        col = row.column()
        col.prop(self, 'socket_128')


class BBN_OP_create_correct_groups(Operator):
    bl_idname = "bbn.create_correct_groups"
    bl_label = "Create Correct Groups"

    def execute(self, context):
        runtime_info['updating'] = True
        try:
            for node_group in bpy.data.node_groups:
                for node in node_group.nodes[:]:
                    if node.bl_idname == 'bbn_group_in_node':
                        new_input_node = node_group.nodes.new('NodeGroupInput')
                        new_input_node.location = node.location
                        for socket in node.outputs:
                            if socket.links:

                                # node_group.links.new(socket.links[0].to_socket, new_input_node.outputs[-1])
                                new_socket = node_group.inputs.new(socket.bl_idname, socket.name)
                                new_socket = new_input_node.outputs[-2]

                                for link in socket.links:
                                    node_group.links.new(new_socket, link.to_socket)
                        node_group.nodes.remove(node)

                    elif node.bl_idname == 'bbn_group_out_node':
                        new_output_node = node_group.nodes.new('NodeGroupOutput')
                        new_output_node.location = node.location
                        for socket in node.inputs:
                            if socket.links:

                                # node_group.links.new(socket.links[0].to_socket, new_output_node.inputs[-1])
                                new_socket = node_group.outputs.new(socket.bl_idname, socket.name)
                                new_socket = new_output_node.inputs[-2]

                                for link in socket.links:
                                    node_group.links.new(link.from_socket, new_socket)
                        node_group.nodes.remove(node)

            for node_group in bpy.data.node_groups:
                for node in node_group.nodes[:]:
                    if node.bl_idname == 'bbn_group_node':
                        new_group_node = node_group.nodes.new('bbn_node_group_custom')
                        new_group_node.location = node.location
                        new_group_node.node_tree_selection = node.node_tree
                        print(f'{node_group} {node}')

                        for i, socket in enumerate((x for x in node.inputs[1:] if not x.hide)):
                            print(socket.name)
                            if socket.links:
                                for link in socket.links:
                                    node_group.links.new(link.from_socket, new_group_node.inputs[i])
                            else:
                                if hasattr(socket, 'default_value'):
                                    new_group_node.inputs[i].do_not_update = True
                                    new_group_node.inputs[i].default_value = socket.default_value
                                    new_group_node.inputs[i].do_not_update = False

                        for i, socket in enumerate(node.outputs[:]):
                            if socket.links:
                                for link in socket.links:
                                    node_group.links.new(new_group_node.outputs[i], link.to_socket)

                        node_group.nodes.remove(node)
        finally:
            runtime_info['updating'] = False
        return {'FINISHED'}


class BBN_OP_create_correct_loops(Operator):
    bl_idname = "bbn.create_correct_loops"
    bl_label = "Create Correct Loops"

    def execute(self, context):
        output_maps = {}
        runtime_info['updating'] = True
        try:
            for node_group in bpy.data.node_groups:
                output_maps[node_group] = OrderedDict()
                node_input = node_group.nodes.get('Loop Start')
                node_output = node_group.nodes.get('Loop End')

                if node_input and node_output:
                    node_group.inputs.new('BBN_int_socket', 'Start Index')
                    node_group.inputs.new('BBN_int_socket', 'End Index')

                    for i, socket in enumerate(node_input.outputs):
                        socket.name = f'{i}_{socket.name}'

                    new_input_node = node_group.nodes.new('NodeGroupInput')
                    index_node = node_group.nodes.new('bbn_loop_info')
                    new_input_node.location = node_input.location
                    index_node.location = node_input.location
                    for i, socket in enumerate(node_input.outputs):
                        if i == 0:
                            for link in socket.links:
                                node_group.links.new(index_node.outputs[0], link.to_socket)
                        else:
                            new_socket = node_group.inputs.new(socket.bl_idname, socket.name)
                            new_socket = new_input_node.outputs[-2]

                            for link in socket.links:
                                node_group.links.new(new_socket, link.to_socket)

                    new_output_node = node_group.nodes.new('NodeGroupOutput')
                    new_output_node.location = node_output.location
                    last_index = 0
                    for i, socket in enumerate(node_output.inputs):
                        if socket.links:

                            output_maps[node_group][i] = last_index
                            node_group.outputs.new(socket.bl_idname, node_input.outputs[i + 1].name)
                            new_socket = new_output_node.inputs[-2]
                            last_index += 1

                            for link in socket.links:
                                node_group.links.new(link.from_socket, new_socket)

                    node_group.nodes.remove(node_output)
                    node_group.nodes.remove(node_input)

            for node_group in bpy.data.node_groups:
                for node in node_group.nodes[:]:
                    if node.bl_idname == 'bbn_loop':
                        new_group_node = node_group.nodes.new('bbn_node_loop')
                        new_group_node.location = node.location
                        new_group_node.node_tree_selection = node.node_tree
                        print(f'{node_group} {node}')

                        for i, socket in enumerate((x for j, x in enumerate(node.inputs) if not j == 2)):
                            print(f'{i} {socket.name}')
                            if socket.links:
                                for link in socket.links:
                                    node_group.links.new(link.from_socket, new_group_node.inputs[i])
                            else:
                                if hasattr(socket, 'default_value'):
                                    new_group_node.inputs[i].do_not_update = True
                                    new_group_node.inputs[i].default_value = socket.default_value
                                    new_group_node.inputs[i].do_not_update = False

                        for i, socket in enumerate(node.outputs[:]):
                            real_index = output_maps[node.node_tree].get(i, None)
                            if real_index is not None:
                                if socket.links:
                                    for link in socket.links:
                                        node_group.links.new(new_group_node.outputs[real_index], link.to_socket)

                        node_group.nodes.remove(node)
        finally:
            runtime_info['updating'] = False
        return {'FINISHED'}


class BBN_OP_show_node_description(Operator):
    bl_idname = "bbn.show_node_description"
    bl_label = ""

    value: bpy.props.StringProperty()

    @ classmethod
    def description(self, context, properties):
        return properties.value

    def execute(self, context):
        return {'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def draw(self, context):
        strings = self.value.split('\n')
        layout = self.layout

        col = layout.column()
        for x in strings:
            col.label(text=x)


classes = [
    BBN_OP_generate_rig,
    BBN_OP_update_sockets,
    BBN_OP_edit_group,
    BBN_OP_group_nodes,
    BBN_OP_set_propertygroup_type,
    BBN_OP_move_socket,
    BBN_OP_new_datablock,
    BBN_OP_edit_datablock,
    BBN_OP_delete_socket,
    BBN_OP_show_shocket_value,
    BBN_OP_show_full_error_message,
    BBN_OP_focus_node,
    BBN_OP_focus_path_node,
    BBN_OP_create_correct_tree_types,
    BBN_OP_select_referencing_nodes,
    BBN_OP_focus_error_node,
    BBN_OP_add_socket,
    BBN_OP_create_correct_groups,
    BBN_OP_create_correct_loops,
    BBN_OP_show_node_description,

]


def register():
    from bpy.utils import register_class

    for cls in classes:
        register_class(cls)


def unregister():
    from bpy.utils import unregister_class

    for cls in reversed(classes):
        unregister_class(cls)
